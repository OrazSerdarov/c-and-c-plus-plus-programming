#include <stdio.h>

#define MAX 5


inline void swap(int* x,int* y) {

	int temp = *x;
	*x = *y;
	*y = temp;

}


inline int compare(int x, int y) {
	
	if (x > y)
		return -1;
	else if (y > x)
		return 1;
	else
		return 0;
}

void bubbleSort(int* , size_t );

int main(void) {

	//int x = 10;
	int array[MAX];
	//int *zeiger =&x;  //0xDFF193
	//int a;

	printf("Bitte geben Sie 5 Ganzzahlen ein: \n");

		scanf_s("%d %d %d %d %d", &array[0], &array[1], &array[2], &array[3], &array[4]);

	bubbleSort(array, MAX);

	for (int i = 0; i < MAX; i++)
		printf("%d ", array[i]);

	printf("\nMedian ist:%d  \n", array[2]);

	system("pause");
	return 0;
}
void bubbleSort(int*  array, size_t s) {

	for (unsigned int i = 0; i < s ;i++)
		for (int j = 0; j < s - 1 - i; j++)
			if ((compare(*(array + j), *(array + j + 1)))==-1)
				swap(array + j, array + j + 1);


	return 0;

}
