#include <stdlib.h>
#include <stdio.h>

void printBin(int number) {
	
	for (int i = sizeof(number) * 8 - 1; i >= 0; i--) {
		printf("%d", (number >> i) & 0x1);
		if (i % 4 == 0 || i % 8 == 0)
			if (i % 8 != 0)
				printf(".");
			else if (i % 8 == 0)
				printf(" ");
	}

	printf("\n");
}

int* selectMask(int* a, int size, int maske ,int *m ) {

	int *r = NULL;
	int counter=0;

	for (int i = 0; i < size; i++)
		if ((a[i] & maske) == maske)
			counter++;

	*m = counter;

	r = (int*)calloc(counter, sizeof(int));

	for (int i = 0, j = 0; i<size; i++)
		if ((a[i] & maske) == maske) {
			r[j] = a[i] & ~maske;
			j++;
		}

	return r;
}
int main(void) {

	int n = 20, m;
	int *ergebniss, *feld;


	feld = (int*)calloc(n, sizeof(int));
	for (int i = 0; i < n; i++)
		feld[i] = i + 1;

	printf("Initial Array:\n");
	for (int i = 0; i < n; i++) {
		printf("%d  :", i);
		printBin(feld[i]);

	}
	ergebniss= selectMask(feld, n,16 ,&m );
	printf("Die Maske ist %d \n", 16);
	

	printf("Mask Array:\n");
	for (int i = 0; i < m; i++) {
		printf("%d  :", i);
		printBin(ergebniss[i]);
	}


	free(ergebniss);
	free(feld);
	system("pause");
	return 0;
}


