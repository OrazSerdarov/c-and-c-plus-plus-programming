#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main(void) {


	srand(time(0));
	rand();
	int zahl = (int)(10.0*rand() /(RAND_MAX +1));
	int eingegebene_Zahl =0;



	printf("Das Spiel beginnt... \n");
	printf("Raten Sie eine Zahl von 0 bis 9. \nSie haben 3 Versuche \n");

	for (int i = 1; i < 4; i++) {
		printf("%d Versuch: \n" , i);
		scanf_s("%d", &eingegebene_Zahl);

		if (zahl == eingegebene_Zahl) {
			printf("Sie haben gewonnen \n");
			break;
		}

		if (eingegebene_Zahl < zahl)
			printf("Die gesuchte Zahl ist groesser! \n");
		else
			printf("Die gesuchte Zahl ist kleiner! \n");
	}

	printf("Die richtige Zahl ist %d. \n"  , zahl);

	system("pause");
	return 0;
}