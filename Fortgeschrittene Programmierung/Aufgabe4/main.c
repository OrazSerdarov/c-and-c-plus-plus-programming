#include <stdio.h>

int main(void)
{

	char c = 'x';

	printf("char-Konst.: %d Bytes  , char-Var: %d  , int: %d Bytes  \n",sizeof('x'), sizeof(c), sizeof(10));		// 4 , 1 , 4

	printf("Inhalt der char-Var.: %c,  int-Konstante: %i\n", c, 10);							//x   , 10


	system("pause");
	return 0;

}