


const float *a;								//a ist ein Zeiger auf einen konstanten float - Wert.

float const *b;								//b ist ein konstanter Zeiger auf eine float - Variable.

float const *p; float*c =&p;				//c ist ein Zeiger auf einen konstanten Zeiger auf eine float - Variable.

float d[3]; 								//d ist ein Vektor mit 3 Zeigern auf float - Variablen.

								//e ist ein Zeiger auf einen Vektor mit 3 Zeigern auf float - Variablen.