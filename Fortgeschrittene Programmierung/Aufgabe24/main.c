#include <stdio.h>

int strlaenge(const char);
int anzahlDerZeichen(const char*, char);

int main(void) {

	char eingabe[80] ,c;
	printf("Geben Sie einen Text \n");
	fgets(eingabe, sizeof(eingabe), stdin);
	printf("Geben Sie ein Zeichen: \n");
	scanf_s("%c", &c);

	printf("Es wurde in dem Text '%s'  %d  Zeichen %c gefunden! \n", eingabe, anzahlDerZeichen(eingabe, c), c);



	system("pause");
	return 0;
}

int strlaenge(const char* str) {

	int i = 0;
	while (str[i] != '\0')
		i++;
	return i;
}

int anzahlDerZeichen(const char *eingabe, char c) {

	int counter = 0;
	int laenge = strlaenge(eingabe);
	c = toupper(c);
	for (int i = 0; i < laenge; i++)
		if (toupper(eingabe[i]) == c)
			counter++;


	return counter;

}