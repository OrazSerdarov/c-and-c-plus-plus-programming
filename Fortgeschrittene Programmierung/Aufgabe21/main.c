#include <stdio.h>

int substr(const char* str1, const char* str2);
int strlaenge(const char* str);
int main(void) {

	char * str1 = "Halloechen";
	char *str2 = "chen";

	printf("%d \n", substr(str1, str2));
	
	system("pause");
	return 0;
}

int substr(const char* str1, const char* str2) {
	
	int laenge1 = strlaenge(str1);
	int laenge2 = strlaenge(str2);

	if (str1 == NULL || str2 == NULL || laenge1<laenge2)
		return -1;

	for (int i = 0, j = 0; i < laenge1 - laenge2+1; i++)
		if (str2[j] == str1[i])
			for (int z = i; z < laenge1-1; z++, j++){
				if (str2[j] != str1[z]){
					j = 0;
					break;
				}
				if (str2[z+1]=='\0')
					return i;
			}

	return -1 ;

}
int strlaenge(const char* str) {
	
	int i = 0;
	while (str[i]!='\0')
			i++;

	return i;


}