#include <stdio.h>
#include <io.h>

#define STR_LEN 80



int main(void){
char str[STR_LEN];
FILE * fp; 
const char * const filename = "bsp.txt"; /* Datei oeffnen, eine Zeile anhaengen: */ 

if ((fp = fopen(filename, "a")) == NULL) {
	/* Fehlerbehandlung: */
	fprintf(stderr,
		"Datei '%s' konnte nicht zum Anhaengen"
		" geoeffnet werden!\n", filename);
	return 1;
}
fprintf(fp, "Noch eine Zeile...\n");
fclose(fp);


system("pause");
return 0;

}