#include <stdio.h>



void printPoint(const struct Point);

struct Point {

	double x;
	double y;

};

int main(void) {
	double c;
	double s;
	struct Point P1 = { -1.1, 3.5 };
	struct Point  P2 = { 2.4, -0.7 };

	s = (P2.x - P1.x) / (P2.y - P2.y);
	c=  P1.y- s *P1.x;
	printPoint(P1);
	printPoint(P2);


	printf("m= %.3lf   und   c=%.3lf \n", s, c);

	system("pause");
	return 0;
}


void printPoint(struct Point p) {

	printf("(Point:%f,%f ) \n", p.x, p.y);
}