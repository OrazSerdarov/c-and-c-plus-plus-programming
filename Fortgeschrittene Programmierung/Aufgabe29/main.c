#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#include "header.h"

int main(int argc, char *argv[])
{
	struct List list;
	init(&list);
	insert(&list, 1, "18.11.11\n");
	insert(&list, 1, "19.11.11\n");
	insert(&list, 5, "20.11.11\n");
	insert(&list, 5, "21.11.11\n");
	insert(&list, 0, "22.11.11\n");
	insert(&list, 10, "23.11.11\n");
	insert(&list, 10, "24.11.11\n");
	
	print(&list);
	removeElements(&list, 1);
	print(&list);
	
	/*removeElements(&list, 0);
	print(&list);
	removeElements(&list, 5);
	*/
	removeMaximum(&list);
	print(&list);

	clear(&list);

	_getch();
	return 0;
}
