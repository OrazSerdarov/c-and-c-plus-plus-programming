#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include "header.h"



 

void init(struct List *list) {
	
	list->first= NULL;
	list->last=NULL;
	}

void insert(struct List *list, int val, char* dat) {

	struct Element * newElement=NULL;

	if (list ==NULL)
		return;

	newElement = ( struct Element *) malloc(sizeof(struct Element));
	
	if (!newElement)
		return;

	 newElement->tagesproduktion = val;
	 strcpy_s(newElement->datum, DATLENGTH, dat );

	 newElement->next = list->first;
	 list->first = newElement;

	 if (!list->last)
		 list->last = newElement;




}
int removeElements(struct List* list, int val) {

	int deletedElements = 0;
	
	struct Element *removable, *cur, *prev;

	cur = list->first;
	prev = NULL, removable = NULL;

	while (cur) {

		if (cur->tagesproduktion == val)
		{
			if (cur== list->first)
			{
				list->first = cur->next;
			}
			else
			{
				prev->next = cur->next;
			}

			if (list->last == cur)
				list->last = prev;

			removable = cur;
			cur = cur->next;
			free(removable);
			deletedElements++;
		}
		else
		{
			/* Aktualisiere prev nur dann, wenn nichts gel�scht wird */
			prev = cur;
			cur = cur->next;
		}
	
	}

	return deletedElements;
}
int clear(struct List *list) {

	struct Element *cur = list->first;
	int result = 0;
	while (cur) {

		list->first = cur->next;
		free(cur);
		cur = list->first;

		result = 1;

	}

	init(list);
	return result;


}
int forall(struct List *list, ItemFunction exec) {

	int counter = 0;
	struct Element *cur;

	for (cur = list->first; cur != NULL; cur = cur->next)
		counter += exec(cur);

	return counter;


}

int printItem(struct Element* element) {
	
	if (element == NULL)
		return 0;

	printf("%s : %d \n", element->datum, element->tagesproduktion);

	return 1;

}

void print(struct List *list) {

	if (list == NULL)
		return;

	ItemFunction e = printItem;
	int counter;
	counter= forall(list, e);

	printf("The list contains:\n");
	printf("Total of %d item%s.\n", counter, (counter == 1) ? "" : "s");


}


//Aufgabe 31

int empty(struct List *list) {


	if (list->first == NULL)
		return 1;

	return 0;
}
int compare(struct Element *elem1, struct Element *elem2) {


	if (elem1->tagesproduktion < elem2->tagesproduktion)
		return -1;

	else if (elem1->tagesproduktion > elem2->tagesproduktion)
		return 1;
	else
		return 0;

}
int removeMaximum(struct List *list) {

	struct Element * current , *max;

	if (list->first == NULL)
		return -1;

	current = list->first;
	
	if (!current->next){
	
		removeElements(list, current->tagesproduktion);
		return current->tagesproduktion;

	}
	
	
	max = current->tagesproduktion > current->next->tagesproduktion ? current : current->next;
	current = current->next->next;
	while (current ) {

		if (current->tagesproduktion > max->tagesproduktion)
			max = current;

		current = current->next;


	}

	removeElements(list, max->tagesproduktion);


	return max->tagesproduktion;

}

