/* Programmieren III
   HS Coburg
   Woche 8, Aufgabe 44
*/
#include <string>
#include <vector>

#ifndef _QUEUE2_H_
#define _QUEUE2_H_


class CQueue2
{
private:
    std::vector<std::string>  m_asQueue;  // Array der Queue-Elemente
    int      m_iMax;     // Gr��e des Array
    int      m_iCount;   // Anzahl der genutzten Elemente im Array
    int      m_iFirst;   // Index des ersten eingef�gten Elements,
                         // dieses wird als erstes wieder ausgegeben
    int      m_iLast;    // Index des zuletzt eingef�gten Elements
    
public:
    CQueue2() : m_iMax(0), m_iCount(0), 
        m_iFirst(0), m_iLast(0) {}
    CQueue2(int iMax);
	CQueue2(const CQueue2& q);    // Kopierkonstruktor
	CQueue2(CQueue2&& q);         // Movekonstruktor
	~CQueue2() = default;
    bool isEmpty() { return m_iCount==0; } // Queue leer ? 
    bool isFull() { return m_iCount==m_iMax; } // Queue voll ? 
    const std::string& get();
    bool put(const std::string& name);
    void contents(); // Auflisten der Queue entspr. der Reihenfolge

	static std::string dummy;
}; 


#endif