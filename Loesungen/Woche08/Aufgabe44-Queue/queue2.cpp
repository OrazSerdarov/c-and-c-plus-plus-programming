/* Programmieren III
   HS Coburg
   Woche 8, Aufgabe 44
*/
#include <iostream>
#include <string>
#include <list>
#include "queue2.h"

using namespace std;

string CQueue2::dummy = "";

CQueue2::CQueue2(int iMax) : m_asQueue(iMax), m_iMax(iMax), m_iCount(0), 
	m_iFirst(0), m_iLast(iMax)
{}

const string& CQueue2::get()
{
	if (isEmpty())
		return dummy;

	m_iCount--;
	m_iLast %= m_iMax;
	return m_asQueue[m_iLast++];
}

bool CQueue2::put(const string& name)
{
	if (isFull() || m_asQueue.empty())
		return false;

	m_iCount++;
	m_asQueue[m_iFirst++] = name;
	m_iFirst %= m_iMax;
	return true;
}

void CQueue2::contents()
{
	int i = m_iLast;
	int n = 0;

	while(n < m_iCount)
	{
		i %= m_iMax;
		cout << n+1 << ".: " << m_asQueue[i] << endl;
		i++; n++;
	}

	cout << "Array: " << endl;
	for(i=0; i<m_iMax; i++)
		cout << i << ": " << m_asQueue[i] << ", ";
	cout << endl;
}

// Kopierkonstruktor
CQueue2::CQueue2(const CQueue2& q): m_asQueue(q.m_asQueue), 
	m_iMax(q.m_iMax), m_iCount(q.m_iCount),
	m_iFirst(q.m_iFirst), m_iLast(q.m_iLast) {}

// Movekonstruktor
CQueue2::CQueue2(CQueue2&& q)
{
	// Den anderen Speicher übernehmen
	m_iMax = q.m_iMax;
	m_iCount = q.m_iCount;
	m_iFirst = q.m_iFirst;
	m_iLast = q.m_iLast;
	m_asQueue = std::move(q.m_asQueue);
	
	// Verantwortung abgeben
	q.m_iMax = 0;
	q.m_iCount = 0;
	q.m_iFirst = q.m_iLast = 0;
}


