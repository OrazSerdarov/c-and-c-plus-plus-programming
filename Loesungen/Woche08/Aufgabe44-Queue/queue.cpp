/* Programmieren III
   HS Coburg
   Woche 8, Aufgabe 44
*/
#include <iostream>
#include <string>
#include <list>
#include "queue2.h"

using namespace std;

class CQueue
{
private:
    string*  m_asQueue;  // Array der Queue-Elemente
    int      m_iMax;     // Gr��e des Array
    int      m_iCount;   // Anzahl der genutzten Elemente im Array
    int      m_iFirst;   // Index des ersten eingef�gten Elements,
                         // dieses wird als erstes wieder ausgegeben
    int      m_iLast;    // Index des zuletzt eingef�gten Elements
    
public:
    CQueue() : m_asQueue(nullptr), m_iMax(0), m_iCount(0), 
        m_iFirst(0), m_iLast(0) {}
    CQueue(int iMax);
	CQueue(const CQueue& q);    // Kopierkonstruktor
	CQueue(CQueue&& q);         // Movekonstruktor
	~CQueue();
    bool isEmpty() { return m_iCount==0; } // Queue leer ? 
    bool isFull() { return m_iCount==m_iMax; } // Queue voll ? 
    const string& get();
    bool put(const string& name);
    void contents(); // Auflisten der Queue entspr. der Reihenfolge

	static string dummy;
}; 

string CQueue::dummy = ""; 

CQueue::CQueue(int iMax) : m_iMax(iMax), m_iCount(0), 
	m_iFirst(0), m_iLast(iMax)
{
	if (m_iMax <= 0) {
		m_iMax = 0;
		m_asQueue = nullptr;
		return;
	}

	m_asQueue = new string[m_iMax];
}

const string& CQueue::get()
{
	if (isEmpty() || !m_asQueue)
		return dummy;

	m_iCount--;
	m_iLast %= m_iMax;
	return m_asQueue[m_iLast++];
}

bool CQueue::put(const string& name)
{
	if (isFull() || !m_asQueue)
		return false;

	m_iCount++;
	m_asQueue[m_iFirst++] = name;
	m_iFirst %= m_iMax;
	return true;
}

void CQueue::contents()
{
	int i = m_iLast;
	int n = 0;

	while(n < m_iCount)
	{
		i %= m_iMax;
		cout << n+1 << ".: " << m_asQueue[i] << endl;
		i++; n++;
	}

	cout << "Array: " << endl;
	for(i=0; i<m_iMax; i++)
		cout << i << ": " << m_asQueue[i] << ", ";
	cout << endl;
}

// Kopierkonstruktor
CQueue::CQueue(const CQueue& q): m_iMax(q.m_iMax), m_iCount(q.m_iCount),
	m_iFirst(q.m_iFirst), m_iLast(q.m_iLast)
{
	if (!q.m_asQueue)
	{
		m_asQueue = nullptr;
		return;
	}

	m_asQueue = new string[m_iMax];

	for(int i = 0; i < m_iMax; i++)
		m_asQueue[i] = q.m_asQueue[i];
}

// Movekonstruktor

// Erl�utern Sie kurz den Unterschied zwischen einem Kopierkonstruktor und einem Movekonstruktor. 
// Welche Vorteile hat ein Movekonstruktor?

// Kopierkonstruktor: erzeugt Kopie, legt eigenen Speicher an. 
// Movekonstr.: �bernimmt Verantwortung f�r bestehenden Speicher, setzt Ursprungsobjekt auf 0. 

// Welche Schritte sollte ein Movekonstruktor stets durchlaufen?

// i.	Initialisierung der Attribute
// ii.	Kopie des Zeigers
// iii.	Setzen des Zeigers im Ursprungsobjekt auf 0

CQueue::CQueue(CQueue&& q)
{
	// Den anderen Speicher �bernehmen
	m_iMax = q.m_iMax;
	m_iCount = q.m_iCount;
	m_iFirst = q.m_iFirst;
	m_iLast = q.m_iLast;
	m_asQueue = q.m_asQueue;
	
	// Verantwortung abgeben
	q.m_iMax = 0;
	q.m_iCount = 0;
	q.m_iFirst = q.m_iLast = 0;
	q.m_asQueue = nullptr;
}

//Destruktor
CQueue::~CQueue()
{
	if (m_asQueue)
		delete[] m_asQueue;
}

int main()
{
	CQueue	q(6);
	CQueue2 p(6);
	int		i;

	for(i=0; i<3; i++)
	{
		string eingabe;
		cout << "Bitte " << i+1 << ". Element eingeben: ";
		cin >> eingabe;
		if (!q.put(eingabe))
			cerr << "Einf�gen von " << eingabe << " fehlgeschlagen!" << endl;
		p.put(eingabe);
	}

	cout << "Inhalt der Queue: " << endl;
	q.contents();
	cout << "-------------" << endl;
	p.contents();

	cout << "Erstes entferntes Element: " << q.get() << endl;
	cout << "Zweites entferntes Element: " << q.get() << endl;

	for(i=0; i<6; i++)
	{
		string eingabe;
		cout << "Bitte " << i+2 << ". Element eingeben: ";
		cin >> eingabe;
		if (!q.put(eingabe))
			cerr << "Einf�gen von " << eingabe << " fehlgeschlagen!" << endl;
		p.put(eingabe);
	}

	cout << "Inhalt der Queue: " << endl;
	q.contents();
	cout << "-------------" << endl;
	p.contents();

	cout << "Erstes entferntes Element: " << q.get() << endl;
	cout << "Zweites entferntes Element: " << q.get() << endl;

	list<CQueue>  l;
	l.push_back(std::move(CQueue(q)));

	l.front().contents();

	system("pause");
	return 0;
}