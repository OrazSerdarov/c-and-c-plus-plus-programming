/* Programmieren III
   HS Coburg
   Woche 8, Aufgabe 43
*/

#include <iostream>
#include <fstream>
#include <windows.h>  // f�r SleepEx

#pragma comment(lib, "winmm.lib") // f�r PlaySound

using namespace std;

class Soundcard
{
private:
	static Soundcard* card;
	Soundcard() = default;
	Soundcard(const Soundcard&) = default;

public:
	static Soundcard* getInstance() {
		if (card == nullptr)
			card = new Soundcard();

		return card;
	}

	static void cleanup()
	{
		if (card) delete card;
		card = nullptr;
	}
	void loadSoundFile(const char* filename);
	void play();
};

void Soundcard::loadSoundFile(const char* filename)
{
	cout << filename << " loaded!" << endl;
}

void Soundcard::play()
{
	cout << "Now playing..." << endl;
	cout << PlaySound(L"alle.mid", 0, SND_SYNC | SND_FILENAME)<< endl;
	//SleepEx(3000, FALSE);
	cout << "Finished." << endl;
}

Soundcard* Soundcard::card = nullptr;

int main()
{

	//Soundcard* sc2 = new Soundcard();   // nicht m�glich!!
	Soundcard* sc = Soundcard::getInstance();

    //*sc2 = *sc;   // zurzeit m�glich
	sc->loadSoundFile("alle.mid");
	sc->play();

	sc->cleanup();
	cin.peek();
	return 0;
}
