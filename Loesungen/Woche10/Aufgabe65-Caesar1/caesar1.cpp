/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 65
*/

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

char shiftchar(char inp, int shift)
{
    if (inp >= 'A' && inp <= 'Z')
    {
        if (inp + shift >= 'A')
        {
            if (inp + shift <= 'Z')
                return (inp + shift);
            else
                return ('A' - 1 + shift - 'Z' + inp);
        }
        else
            return ('Z' + 1 + shift - 'A' + inp);
    }
    
    if (inp >= 'a' && inp <= 'z')
    {
        if (inp + shift >= 'a')
        {
            if (inp + shift <= 'z')
                return (inp + shift);
            else
                return ('a' - 1 + shift - 'z' + inp);
        }
            return ('z' + 1 + shift - 'a' + inp);
    }
    
    return inp;
}
    
string caesar_encode(const string& src, unsigned int dist)
{
    int i;
	ostringstream o;
    
	for (i=0; i < src.size(); i++)
        o << shiftchar(src[i], (int)dist);

    o << ends;
	return o.str();
}        

string caesar_decode(const string& src, unsigned int dist)
{
    int i;
	ostringstream o;
    
    for (i=0; i < src.size(); i++)
        o << shiftchar(src[i], -(int)dist);

    o << ends;
	return o.str();
}        

int main()
{
/*
    string text = "Gallien in seiner Gesamtheit ist in drei Teile geteilt, "
                  "von denen den einen die Belgier bewohnen, den anderen die "
                  "Aquitanier und den dritten die, welche in ihrer eignen Sprache"
                  " Kelten, in unserer Gallier heissen.";*/
    string text = "Franz jagt im komplett verwahrlosten Taxi quer durch Bayern.";
                  
    string verschl, entschl;
    
	cout << "Nachricht: " << endl << text << endl << endl; 
    verschl = caesar_encode(text, 15);
    
    cout << "Verschluesselte Nachricht: " << endl << verschl << endl << endl;
    entschl = caesar_decode(verschl, 15);

    cout << "Entschluesselte Nachricht:" << endl << entschl << endl << endl << endl;
    
    cout << "Ihre Nachricht: ";
	char text2[80];
	cin.getline(text2,80);
	string ihrtext(text2);
    
    verschl = caesar_encode(ihrtext, 20);
    
    cout << endl << "Verschluesselte Nachricht:" << endl << verschl << endl << endl;
    entschl = caesar_decode(verschl, 20);

    cout << "Entschluesselte Nachricht: " << endl <<  entschl << endl;
    
	system("pause");
    return 0;
}
    
