/* Programmieren III
   HS Coburg
   Woche 10, Aufgabe 53
*/

#include <iostream>

using namespace std;

template<typename T> T f(T* t, int i)
{  
    cout << "Hier Template-Funktion 1" << endl;
    return *t; 
}
    
template<typename T> T f(T s, T t)
{ 
    cout << "Hier Template-Funktion 2" << endl;
    return s; 
}

template<>
char f(char* s, int i)
{ 
    cout << "Hier char-Funktion" << endl;
    return *s; 
}

template<>
double f(double x, double y) 
{ 
    cout << "Hier double-Funktion" << endl;
    return x; 
}


int main()
{

    int i = 0;
    unsigned int ui = 1;
    char cFeld[20];
    int iFeld[20];

    cout << "Aufruf 1: ";
    f(cFeld,20);
    
    cout << "Aufruf 2: ";
    f(iFeld,20);
    
    cout << "Aufruf 3: ";
    f(iFeld[0], i);
    
    cout << "Aufruf 4: ";
	f(i, (int)ui);
    
    cout << "Aufruf 5: ";
    f(iFeld, ui); 
    
    cout << "Aufruf 6: ";
    f(&i, i);
    
	cin.peek();
    return 0;
}
