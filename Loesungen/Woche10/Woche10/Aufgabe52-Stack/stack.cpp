/* Programmieren III
   HS Coburg
   Woche 10, Aufgabe 52
*/
#include <cstdlib>

template<typename T, int n>
class Stapel
{
public:
	Stapel() {}
	T pop() {return T();}
	void push(T) {}
	bool istLeer() {return true;}

private:
	int hoehe;
	T speicher[n];

};

int main()
{
	Stapel<int,50> S1, S2;
	Stapel<bool,30> S3;
	Stapel<int,100> S4;

	S1.push(20.0);

	S2.push(S1.pop());

	S1.push(S2.istLeer());

	// S4 = S1 + S2;  f

	// S4.push(S1.hoehe); f

	std::system("pause");

	return 0;
}
