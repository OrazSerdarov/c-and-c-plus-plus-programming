/* Programmieren III
   HS Coburg
   Woche 10, Aufgabe 54
*/

#include <iostream>
#include "liste.h"

using namespace std;

struct Produktion
{
	int wert;
	string datum;

	Produktion(int val, const string& dat = "01.01.15") :
		wert(val), datum(dat) {}
};

bool operator==(const Produktion& p1, const Produktion& p2)
{
	if (p1.wert == p2.wert && p1.datum == p2.datum)
		return true;
	return false;
}

ostream& operator<<(ostream& o, const Produktion& p)
{
	o << p.datum << ": " << p.wert << endl;
	return o;
}


int main(int argc, char *argv[])
{
    AllgemeineListe<Produktion>  list;

	list.insert(Produktion(1, "18.10.14")); 
    list.insert(Produktion(0, "19.10.14")); 
    list.insert(Produktion(5, "20.10.14")); 
    list.insert(Produktion(1, "21.10.14")); 
    list.insert(Produktion(5, "22.10.14")); 
    list.print(); 
    
    {
        AllgemeineListe<Produktion> l2(list);
        cout << endl << "2. Liste: ";
        l2.print();
        cout << endl;
    }
    
    list.remove(Produktion(5,"20.10.14")); 
    list.print(); 
    list.clear();
    
    if (list.empty())
        cout << "List is now empty." << endl;
    else
        cout << "List not empty." << endl; 
  
    system("PAUSE");	
    return 0;
}
