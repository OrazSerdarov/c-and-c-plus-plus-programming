/* Programmieren III
   HS Coburg
   Woche 10, Aufgabe 54
*/

#ifndef _LISTE_H_
#define _LISTE_H_

#include <string>

// Definition der Listen-Klasse
template <typename T>
class AllgemeineListe {
private:
	// Definition der Listen-Strukturen 
	class tElement {
	public:
		T datum;   // Singular von "Daten"
		tElement* next;

		tElement() : datum(0), next(nullptr) {}
		tElement(const T& d, tElement* n = nullptr) : datum(d), next(n) {}
	};

	tElement*	first;
	tElement*	last;

public:
    AllgemeineListe() { 
		std::cout<<"Konstruktor"<<std::endl;
        init(); 
	}
    AllgemeineListe(const T& dat) {
        init(); 
		insert(dat); }
    AllgemeineListe(const AllgemeineListe<T>& _list) {
		std::cout << "Hier ist der Kopierkonstruktor!" << endl;
		init();
    
		if (_list.empty()) return;
        
		for(auto current = _list.first; current != nullptr; current = current->next)
			append(current->datum);
	}

    ~AllgemeineListe() { 
        std::cout << "Hier ist der Destruktor!" << std::endl;
        std::cin.peek();
        clear(); 
    }
    
    void init() {
		first = last = nullptr; }
    void insert(const T& dat);
    void append(const T& dat);
    int remove(const T& dat);
    int clear();

	using ItemFunction = int(AllgemeineListe<T>::*)(typename tElement*);

    int forall(ItemFunction);
	int printItem(tElement* element);
    void print();
    bool empty() const {
		return (first == nullptr)? true : false; }
};

/*  ------------------------------------------
    Name      : insert
    Zweck     : Einf�gen eines Elements an den Anfang der Liste
    Parameter : Wert des neuen Elements
    R�ckgabe  : keine
    ------------------------------------------*/    
template<typename T>
void AllgemeineListe<T>::insert(const T& dat)
{
    tElement* newelem = new tElement(dat, first);
    
	first = newelem;
    
    /* Erstes Element der Liste */
    if (!last)
        last = newelem;
}

/*  ------------------------------------------
    Name      : append
    Zweck     : Einf�gen eines Elements ans Ende der Liste
    Parameter : Wert des neuen Elements
    R�ckgabe  : keine
    ------------------------------------------*/    
template<typename T>
void AllgemeineListe<T>::append(const T& dat)
{
    tElement* newelem = new tElement(dat);
    
    if (first)
    {
        /* Einfuegen an letzter Stelle, falls schon ein anderes vorhanden */
        last->next = newelem;
        last = newelem;
    }
    else
    {
        /* Erstes Element der Liste */
        first = newelem;
        last = newelem;
    }
}

/*  ------------------------------------------
    Name      : remove
    Zweck     : L�scht alle Eintr�ge der Liste mit gegebenem Wert
    Parameter : -
                Gesuchter Wert f�r Tagesproduktion
    R�ckgabe  : Anzahl der gel�schten Elemente
    ------------------------------------------*/    
template<typename T>
int AllgemeineListe<T>::remove(const T& dat)
{
    tElement* current, *prev;
    unsigned int no_del_elem = 0;
    
    /* Aus leerer Liste kann nichts gel�scht werden */
    if (empty())
        return 0;
        
    /* Durchlaufe die Liste */    
    current = first;
    prev = first;

    while(current)
    {
        if (current->datum == dat)
        {
            if (current == first)
            {
                first = current->next;
                if (last == current)
                    last = 0;
                
            }
            else
            {
                prev->next = current->next;
                if (last == current)
                    last = prev;
            }
            
            prev = current;
            current = current->next;
            delete prev;
            no_del_elem++;
        }                
        else
        {
            prev = current;
            current = current->next;
        }
    }
    
    return no_del_elem;
}

/*  ------------------------------------------
    Name      : clear
    Zweck     : L�scht alle Eintr�ge der Liste 
    Parameter : -
    R�ckgabe  : 1 bei Erfolg, 0 sonst
    ------------------------------------------*/    
template<typename T>
int AllgemeineListe<T>::clear()
{
    tElement* current;

    /* Aus leerer Liste kann nichts gel�scht werden */
    if (empty())
        return 1;
        
    /* Durchlaufe die Liste */    
    current = first;

    while(current)
    {
        first = current->next;
        delete current;
        current = first;
    }
    
    init();
    return 1;
}

/*  ------------------------------------------
    Name      : forall
    Zweck     : Ruft die �bergebene Funktion f�r alle Elemente der Liste auf 
    Parameter : Zeiger auf Methode der Klasse AllgemeineListe<T>
    R�ckgabe  : Anzahl der erfolgreichen Aufrufe dieser Funktion
    ------------------------------------------*/    
template<typename T>
int AllgemeineListe<T>::forall(ItemFunction exec)
{
    tElement* current;
    int no_of_execs = 0;

    /* Bei leerer Liste kann nichts aufgerufen werden */
    if (empty())
        return 0;
        
    /* Durchlaufe die Liste */    
    for(current = first; current != 0; current = current->next)
        no_of_execs += (this->*exec)(current);
    
    return no_of_execs;
}

/*  ------------------------------------------
    Name      : printItem
    Zweck     : Druckt ein Listenelement aus
    Parameter : Zeiger auf ein Element
    R�ckgabe  : Anzahl der erfolgreichen Aufrufe dieser Funktion
    ------------------------------------------*/    
template <typename T>
int AllgemeineListe<T>::printItem(tElement* element)
{
    std::cout << element->datum << std::endl;
    return 1;
}

/*  ------------------------------------------
    Name      : print
    Zweck     : Druckt die ganze Liste aus
    Parameter : -
    R�ckgabe  : keine
    ------------------------------------------*/    
template<typename T>
void AllgemeineListe<T>::print() 
{
    int n;
    
    std::cout << "The list contains:" << std::endl;
    n = forall(&AllgemeineListe<T>::printItem);
    std::cout << "Total of " << n << " item" << ((n==1)? "" : "s") 
         << "." << std::endl; 
}

#endif

