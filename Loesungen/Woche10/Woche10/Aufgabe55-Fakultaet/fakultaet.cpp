/* Programmieren III
   HS Coburg
   Woche 10, Aufgabe 55
*/

#include <iostream>

using namespace std;

template <int n>
class Fakultaet
{
public:
	enum{ value = n * Fakultaet<n-1>::value};
};

template<>
class Fakultaet<0>
{
public:
       enum{ value = 1};
};             

int main()
{
    const int x = Fakultaet<12>::value;
    cout << x << endl;
    cin.peek();
    return 0;
}
