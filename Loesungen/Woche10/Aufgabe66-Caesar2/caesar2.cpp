/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 66
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>

using namespace std;

char shiftchar(char inp, int shift)
{
    if (inp >= 'A' && inp <= 'Z')
    {
        if (inp + shift >= 'A')
        {
            if (inp + shift <= 'Z')
                return (inp + shift);
            else
                return ('A' - 1 + shift - 'Z' + inp);
        }
        else
            return ('Z' + 1 + shift - 'A' + inp);
    }
    
    if (inp >= 'a' && inp <= 'z')
    {
        if (inp + shift >= 'a')
        {
            if (inp + shift <= 'z')
                return (inp + shift);
            else
                return ('a' - 1 + shift - 'z' + inp);
        }
            return ('z' + 1 + shift - 'a' + inp);
    }
    
    return inp;
}

int main(int argc, char** argv)
{    
    if (argc < 3)
    {
        cerr << "Aufruf: caesarcode [-d] <quelldatei> <verschiebungsfaktor>" << endl;
        cin.peek();
        return -1;
    }    
    
    bool decode = false;
    if (argc > 3)
    {
        if (strcmp(argv[1], "-d"))
        {
            cerr << "Ung�ltige Option " << argv[1] << endl;
            cin.peek();
            return -4;
        }
        
        argv++;
        decode = true;
    }
    
    int shiftfactor = atoi(argv[2]);
    if (shiftfactor == 0)
    {
        cerr << "Bitte eine Zahl ungleich 0 statt " << argv[2] << " eingeben!" << endl;
        cin.peek();
        return -3;
    }
    
    if (decode)
        shiftfactor *= -1;
    
    ifstream inpfile(argv[1]);
    if (!inpfile)
    {
        cerr << "Quelldatei " << argv[1] << " kann nicht geoeffnet werden!" << endl;
        cin.peek();
        return -2;
    }
    
	ostringstream outfilename;
    if (decode)
		outfilename << argv[1] << ".txt" << ends;
    else
		outfilename << argv[1] << ".enc" << ends;
      
    ofstream outpfile(outfilename.str().c_str());
    if (!outpfile)
    {
        cerr << "Zieldatei " << outfilename.str() << " kann nicht geoeffnet werden!" << endl;
        cin.peek();
        return -3;
    }
    
	cout << "Schreibe " << argv[1] << " mit Verschiebungsfaktor " << shiftfactor 
		 << " nach " << outfilename.str() << endl;
	char inpchar;
    while (inpfile.get(inpchar))
        outpfile.put(shiftchar(inpchar, shiftfactor));

	cout << "Datei " << outfilename.str() << " erfolgreich geschrieben!" << endl;
	cin.peek();
    return 0;
}
        

