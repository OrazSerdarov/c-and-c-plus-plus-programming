/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 67
*/

#include <iostream>
#include <cstdio>

using namespace std;

//----------------------------------------------------
class VektorFehler {
protected:
	char meldung[30];

public:
	VektorFehler() throw() {
		strcpy(meldung, "Einfacher Vektor-Fehler");
	}
    virtual const char* what() const throw() {
        return meldung;
    }
};

//----------------------------------------------------
class VektorUnterlauf : public VektorFehler 
{
public:
	VektorUnterlauf(int index = 0) throw() {
		sprintf(meldung, "Vektor-Index %d zu klein", index); 
    }        
};

//----------------------------------------------------
class VektorUeberlauf: public VektorFehler
{
public:
	VektorUeberlauf(int index = 0) throw() {
		sprintf(meldung, "Vektor-Index %d zu gross", index); 
    }        
};

//-----------------------------------------------

template <typename T> class Vektor
{
private:
  int size;
  T* v;

public:
  Vektor() : size(0), v(0) {}
  Vektor(int sz);
  Vektor(const Vektor& vek);
  ~Vektor() { if (v) delete[] v; }
  int getSize() const { return size; }
  T& at(int i);
};

//-----------------------------------------------
// Methoden der Klasse
//-----------------------------------------------
template <typename T>
Vektor<T>::Vektor(int sz) :         
  size(sz)
{ 
  v = new T[size]; 
}

// Kopierkonstruktor
template <typename T>
Vektor<T>::Vektor(const Vektor<T>& vek) :
  v(0), size(0)
{
    // 1. Reservieren des Speichers
    v = new T[vek.size];
    if (v == 0) // kein Speicher!
    {
      return;
    }
    size = vek.size;

    // 2. Kopieren des Inhalts
    for(unsigned i=0; i<size; i++)
      v[i] = vek.v[i];
}

// Schreibzugriff
template <typename T>
T& Vektor<T>::at(int i) 
{
	if (i < 0)
		throw VektorUnterlauf(i);
	if (i >= size)
		throw VektorUeberlauf(i);

	return v[i];
}

//-----------------------------------------------
int main()
{
  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

  try
  {
	  cout << "v[-1] = " << v.at(-1) << endl;
  }
  catch (VektorFehler& vf)
  {
	  cerr << "Fehler: " << vf.what() << endl;
  }

  try
  {
	  cout << "v[5] = " << v.at(5) << endl;
  }
  catch (VektorFehler& vf)
  {
	  cerr << "Fehler: " << vf.what() << endl;
  }

  try
  {
	  cout << "v[0] = " << v.at(0) << endl;
  }
  catch (VektorFehler& vf)
  {
	  cerr << "Fehler: " << vf.what() << endl;
  }

  cin.peek();
  return 0;
}
