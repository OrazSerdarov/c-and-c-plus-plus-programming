/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 64
*/

#include <iostream>
#include <fstream>

using namespace std;

int filterHTML(const char* quelle, const char* ziel);

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        cerr << "Aufruf: filterhtml <quelldatei> <zieldatei>" << endl;
        return -1;
    }    
    
    return filterHTML(argv[1], argv[2]);
}


int filterHTML(const char* quelle, const char* ziel)
{
    ifstream  inpfile(quelle);
    if (!inpfile)
    {
        cerr << "Quelldatei " << quelle << " kann nicht geoeffnet werden!" << endl;
        return -2;
    }
    
	ofstream  outpfile(ziel);
    if (!outpfile)
    {
        cerr << "Zieldatei " << ziel << " kann nicht geoeffnet werden!" << endl;
        return -3;
    }
    
	char inpchar;
    bool tocopy = true;

    while (inpfile.get(inpchar))
    {
        switch(inpchar)
        {
            case '<':   tocopy = false;
                        break;
            case '>':   tocopy = true;
                        break;
            default:    if (tocopy)
                            outpfile.put(inpchar);
        }    
    }
    
	cout << "Umwandlung erfolgreich!" << endl;
	cin.peek();
    return 0;
}

