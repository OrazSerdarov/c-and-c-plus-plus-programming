/* Programmieren III
   HS Coburg
   Woche 7, Aufgabe 39
*/

#include <iostream>

using namespace std;

class Rational
{  
private:
  long zaehler,nenner;
  long ggT (long a,long b);

public:
  Rational()  
  { cout << "Standardkonstruktor" << endl;
    set(0, 1); 
  }
  
  Rational(long gz) // f�r Ganzzahlen
  { cout <<"Konstruktor " << gz << endl;
    set(gz, 1); 
  }

  Rational(long z, long n) 
  { cout <<"Konstruktor (" << z << "/" << n << ")" << endl;
    set(z, n); 
  }
  
  Rational(const Rational& r)  // Kopierkonstruktor
  { cout << "Kopierkonstruktor ";
    r.ausgabe();
    cout << endl; 
    set(r.zaehler, r.nenner); 
  }

  Rational(Rational&& r)  // Movekonstruktor
  {
	  cout << "Movekonstruktor ";
	  r.ausgabe();
	  cout << endl;
	  set(r.zaehler, r.nenner);
  }

  Rational& operator=(const Rational& r)
  {
	  //if (&r == this)
		 // return *this;
	  //
	  cout << "Zuweisungsoperator ";
	  r.ausgabe();
	  cout << endl;
    
	  set(r.zaehler, r.nenner);
	  return *this;
  }

  void set(long z, long n);

  operator double()
  { return (double)zaehler/nenner; }

//  friend Rational operator+(const Rational& a, const Rational& b);
  friend Rational add(const Rational& a, const Rational& b);
  friend Rational sub(const Rational& a, const Rational& b);
  friend Rational mult(const Rational& a, const Rational& b);
  friend Rational div(const Rational& a, const Rational& b);

  void kehrwert()
    { long t=zaehler; zaehler=nenner; nenner=t; }

  void kuerzen();
  void ausgabe(ostream& o = cout) const
    { o << "(" << zaehler << "/"<< nenner << ")"; }

  friend ostream& operator<<(ostream& o, const Rational& r);
};

ostream& operator<<(ostream& o, const Rational& r)
{
	r.ausgabe(o);
	return o;
}

//-----------------------------------------------------------------------------------
inline void Rational::set(long z,long n)
{
  if (n)
  {  
    zaehler=z; 
    nenner=n; 
  }
  else
  {  
    zaehler=0; 
    nenner=1; 
  }

  kuerzen();
}

//-----------------------------------------------------------------------------------
long Rational::ggT(long x, long y)
{
  while(y) 
  { 
    long r = x % y; 
    x = y; 
    y = r; 
  }
  return (x);
}

//-----------------------------------------------------------------------------------
void Rational::kuerzen()
{
  int sign=1;

  if (zaehler <0) 
  { 
    sign *= (-1); 
    zaehler *= (-1); 
  }
  
  if (nenner  <0) 
  { 
    sign *= (-1); 
    nenner  *= (-1); 
  }

  long t=ggT(zaehler,nenner);

  if (t)
  { 
    zaehler = sign * zaehler / t;
    nenner  = nenner / t; 
  }
}

//-----------------------------------------------------------------------------------
//Rational operator+(const Rational& a, const Rational& b)
Rational add(const Rational& a, const Rational& b)
{
    Rational temp;
    temp.set(a.zaehler * b.nenner + b.zaehler * a.nenner, a.nenner * b.nenner); 
    return std::move(temp);
}

//-----------------------------------------------------------------------------------
Rational sub(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.nenner - b.zaehler * a.nenner, a.nenner * b.nenner); 
    return temp;
}

//-----------------------------------------------------------------------------------
Rational mult(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.zaehler, a.nenner * b.nenner); 
    return temp;
}

//-----------------------------------------------------------------------------------
Rational div(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.nenner, a.nenner * b.zaehler); 
    return temp;
}

//-----------------------------------------------------------------------------------
int main(void)
{
   Rational  a(3,4),b;

   b.set(2,5);
   cout << "b: " << static_cast<double>(b) << endl;

//   Rational c = a + b;
   Rational c = add(a, b);
   a.ausgabe(); cout << " + "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   c = sub(3 ,b);
   cout << "3 - " << b << " = " << c; 
   cout << endl;

   c = mult(a,b);
   a.ausgabe(); cout << " * "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   c = div(a,b);
   a.ausgabe(); cout << " / "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   system("pause");
   return 0;
}




