/* Programmieren III
   HS Coburg
   Woche 7, Aufgabe 38
*/

#include <iostream>

using namespace std;

class Rational
{  
private:
  long zaehler,nenner;
  long ggT (long a,long b);

public:
  Rational()  { set(0, 1); }
  Rational(long gz) // f�r Ganzzahlen
  { cout <<"Konstruktor " << gz << endl;
    set(gz, 1); 
  }

  Rational(long z, long n) 
  { cout <<"Konstruktor (" << z << "/" << n << ")" << endl;
    set(z, n); 
  }
  
  Rational(const Rational& r)  // Kopierkonstruktor
  { 
	cout << "Kopierkonstruktor ";
    r.ausgabe();
    cout << endl; 
    set(r.zaehler, r.nenner); 
  }
  
  void set(long z, long n);

  void add(const Rational& a, const Rational& b)
    { set(a.zaehler * b.nenner + b.zaehler * a.nenner, a.nenner * b.nenner); }

  void sub(const Rational& a, const Rational& b)
    { set(a.zaehler * b.nenner - b.zaehler * a.nenner, a.nenner * b.nenner); }

  void mult(const Rational& a, const Rational& b)
    { set(a.zaehler * b.zaehler, a.nenner * b.nenner); }

  void div(const Rational& a, const Rational& b)
    { set(a.zaehler * b.nenner, a.nenner * b.zaehler); }

  void kehrwert()
    { long t=zaehler; zaehler=nenner; nenner=t; }

  void kuerzen();
  void ausgabe() const
    { cout << "(" << zaehler << "/"<< nenner << ")"; }
};

//-----------------------------------------------------------------------------------
inline void Rational::set(long z,long n)
{
  if (n)
  {  
    zaehler=z; 
    nenner=n; 
  }
  else
  {  
    zaehler=0; 
    nenner=1; 
  }

  kuerzen();
}

//-----------------------------------------------------------------------------------
long Rational::ggT(long x, long y)
{
  while(y) 
  { 
    long r = x % y; 
    x = y; 
    y = r; 
  }
  return (x);
}

//-----------------------------------------------------------------------------------
void Rational::kuerzen()
{
  int sign=1;

  if (zaehler <0) 
  { 
    sign *= (-1); 
    zaehler *= (-1); 
  }
  
  if (nenner  <0) 
  { 
    sign *= (-1); 
    nenner  *= (-1); 
  }

  long t=ggT(zaehler,nenner);

  if (t)
  { 
    zaehler = sign * zaehler / t;
    nenner  = nenner / t; 
  }
}

//-----------------------------------------------------------------------------------
int main(void)
{
   Rational  a(3,4),b, c;

   b.set(2,5);

   c.add(a,b);
   a.ausgabe(); cout << " + "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   c.sub(3 ,b);
   cout << "3 - "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   c.mult(a,b);
   a.ausgabe(); cout << " * "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   c.div(a,b);
   a.ausgabe(); cout << " / "; b.ausgabe(); cout << " = "; c.ausgabe();
   cout << endl;

   system("pause");
   return 0;
}




