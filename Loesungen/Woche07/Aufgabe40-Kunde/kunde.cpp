/* Programmieren III
   HS Coburg
   Woche 7, Aufgabe 40
*/

#include<iostream>
#include<string>

using namespace std;

class Kunde 
{
	int m_nummer; 
	string m_name;
	
	static int anzahl;

public:
	Kunde(int nummer = 0, const string& name = "") :
	  m_nummer(nummer), m_name(name) 
	{ 
		  cout << "Hier Konstruktor f�r " << name << endl;
		  anzahl++; 
	}

	~Kunde() { anzahl--; }
	
	int getNummer() const { return m_nummer; }
	const string& getName() const { return m_name; } 
	void ausgeben()
	{
		std::cout << m_nummer << ": " << m_name << std::endl; 
	}
	
	static int gibAnzahl() { return anzahl; }
};

int Kunde::anzahl = 0;

int main()
{
	cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	Kunde k1(1, "Knut Wuchtig");
	Kunde k2(2, "Hasso Zorn");

	cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	{
		Kunde k3(3, "Sven R�hrig");
		cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	}
	
	cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	cin.peek();

	return 0;
}

