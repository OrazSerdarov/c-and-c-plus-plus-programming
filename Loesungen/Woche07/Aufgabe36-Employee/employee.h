/* Programmieren III
   HS Coburg
   Woche 7, Aufgabe 36 + 37
*/

#ifndef  __EMPLOYEE_H__  // 1. es muss #ifndef hei�en
#define __EMPLOYEE_H__

#include <iostream>

class Employee { // 2. Vor class kein public
  int salary;  // 3. salary darf erst in C++11 initialisiert werden
  char* name;
	
public: // 4. Hinter public muss ein Doppelpunkt
  Employee(int newSalary = 0) { // 5. ein Konstruktor darf kein void haben
	  std::cout << "Hier Konstruktor mit salary: " << newSalary << std::endl;
    salary = newSalary;
    name = new char[80];
  }

  Employee(const Employee& e);
  ~Employee();

  Employee& operator=(const Employee& e);

  int getSalary() {return salary;}
  const char* getName() {return name;} // 6. getName muss char* liefern
  // 7. Tippfehler: "nane" ist kein bekannter Bezeichner
};  // 8. Hinter die } muss ein Semikolon

#endif // 9. Das endif darf keine Parameter haben


/* Aufgabe 37:

a )In welchen F�llen ben�tigt eine Klasse einen Kopierkonstruktor? 
   Warum geh�rt die Klasse Employee aus Aufgabe 36 dazu? 
   
   Eine Klasse ben�tigt einen Kopierkonstruktor, wenn sie �ber selbst verwalteten
   Speicher verf�gt. Das ist bei Employee mit name der Fall.

b) In welchen F�llen ben�tigt eine Klasse einen Destruktor? 
   Warum geh�rt die Klasse Employee aus Aufgabe 36 dazu? 

   Eine Klasse ben�tigt einen Destruktor, wenn sie �ber selbst verwalteten
   Speicher verf�gt. Das ist bei Employee mit name der Fall.

*/
