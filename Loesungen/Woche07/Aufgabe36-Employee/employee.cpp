/* Programmieren III
   HS Coburg
   Woche 7, Aufgabe 36
*/


#include <iostream>
#include <cstring>
#include "employee.h"
using namespace std;


Employee::Employee(const Employee& e)
{
	cout << "Hier Kopierkonstruktor" << endl;
	name = new char[80];
	strncpy(name, e.name, 80);
	salary = e.salary + 1;
}

Employee& Employee::operator=(const Employee& e)
{
	cout << "Hier Zuweisungsoperator" << endl;
	char* tmp = new char[80];
	if (!tmp)
		return *this;

	strncpy(tmp, e.name, 80);
	delete[] name;
	name = tmp;
	salary = e.salary + 2;
	return *this;
}

Employee::~Employee()
{
	cout << "Hier Destruktor (sal="<< salary << ")" << endl;
	if (name)
		delete[] name;
	system("pause");
}

//--------------------------------------------
Employee f( Employee& e ) {
  return e;
}

int main() {
  Employee empl1(25000); 
  Employee empl2(empl1);
  Employee empl3 = empl1; 
  //empl1 = f(empl3);

  return 0; 
}

