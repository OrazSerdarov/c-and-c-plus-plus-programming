/* Programmieren III
   HS Coburg
   Woche 5, Aufgabe 30
*/

#include <iostream>
#include <algorithm>

using namespace std;

int n = 0;
bool vorz(int v) {
	cout << "v: "<< v << endl; 
     if (v < 0) { n++; return false; }
	    return true;
}	     

int f1()
{
	int a[10] = { 3, 5, -8, 13, 21, 34, -55, 89, 144, 233 };

	if (all_of(a, a + 10, vorz))
		cout << n << ", +" << endl;
	else
		cout << n << ", -" << endl;

	return 0;
}
int f2(void)
{
	int a[10]={3, 5, -8, 13, 21, 34, -55, 89, 144, 233}; 
	int n2 = 0;
	
	if (all_of(a, a+10, [&n2] (int v) -> bool {
		cout << "v: "<< v << endl; 
		if (v < 0) { 
			n2++; return false; }
		return true;
	}))
		cout << n2 << ", +" << endl;
	else
		cout << n2 << ", -" << endl;

	return 0;
}

int main()
{
	f1();

	cout << endl;
	f2();
	system("pause");
	return 0;
}
