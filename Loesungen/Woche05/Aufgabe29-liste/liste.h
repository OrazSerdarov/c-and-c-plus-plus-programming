/* Programmieren III
   Hochschule Coburg
   Woche 5, Aufgabe 29
*/


/* Definition der Listen-Strukturen */
#define DATLENGTH  11

struct  Element {
	char datum[DATLENGTH]; 
	unsigned tagesproduktion;
	struct Element* next;
};	

struct List {
	struct Element*	first;
	struct Element*	last;
};

/* Definition des Funktionszeigers */
typedef int (*ItemFunction) (struct Element*);

/* Header der Funktionen */
void init(struct List *list);
void insert(struct List *list, int val, char* dat);
int removeElements(struct List *list, int val);
int clear(struct List *list);
int forall(struct List *list, ItemFunction exec);
int printItem(struct Element *element);
void print(struct List *list);

