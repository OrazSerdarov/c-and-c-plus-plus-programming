/* Programmieren III
   Hochschule Coburg
   Woche 5, Aufgabe 29
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "liste.h"

/* Implementierung der Listen-Funktionen */

/*  ------------------------------------------
    Name      : init
    Zweck     : Initialisierung der Liste
    Parameter : Zeiger auf Struktur f�r Liste
    R�ckgabe  : keine
    ------------------------------------------*/    
void init(struct List *list)
{
    list->first = NULL;
    list->last = NULL;
}

/*  ------------------------------------------
    Name      : insert
    Zweck     : Einf�gen eines Elements an den Anfang der Liste
    Parameter : Zeiger auf Struktur f�r Liste
                Wert des neuen Elements
                Datum des neuen Elements
    R�ckgabe  : keine
    ------------------------------------------*/    
void insert(struct List *list, int val, char* dat)
{
	struct Element* newelem = NULL;

	if (list == NULL) 
		return;
	
	newelem = (struct Element*)malloc(sizeof(struct Element));
	if (!newelem)
		return;

    newelem->tagesproduktion = val;
    strncpy(newelem->datum, dat, DATLENGTH);

    newelem->next = list->first;
    list->first = newelem;
    
    if (!list->last)
    {
        /* Erstes Element der Liste */
        list->last = newelem;
    }
}
    
/*  ------------------------------------------
    Name      : removeElements
    Zweck     : L�scht alle Eintr�ge der Liste mit gegebenem Wert
    Parameter : Zeiger auf Struktur f�r Liste
                Gesuchter Wert f�r Tagesproduktion
    R�ckgabe  : Anzahl der gel�schten Elemente
    ------------------------------------------*/    
int removeElements(struct List *list, int val)
{
	struct Element* current, *prev, *removable;
    unsigned int no_del_elem = 0;
    
    /* Aus leerer Liste kann nichts gel�scht werden */
    if (list == NULL)
		return 0;
	
    /* Durchlaufe die Liste */    
    current = list->first;
    prev = NULL;
	removable = NULL;

    while(current)
    {
        if (current->tagesproduktion == val)
        {
            if (current == list->first)
            {
                list->first = current->next;                
            }
			else
			{
				prev->next = current->next;
			}
            
			if (list->last == current)
                list->last = prev;
            
            removable = current;
            current = current->next;
            free(removable);
            no_del_elem++;
        }                
        else
        {
			/* Aktualisiere prev nur dann, wenn nichts gel�scht wird */
            prev = current;
            current = current->next;
        }
    }
    
    return no_del_elem;
}
            
    
/*  ------------------------------------------
    Name      : clear
    Zweck     : L�scht alle Eintr�ge der Liste 
    Parameter : Zeiger auf Struktur f�r Liste
    R�ckgabe  : 1 bei Erfolg, 0 sonst
    ------------------------------------------*/    
int clear(struct List *list)
{
    struct Element* current;

    /* Aus leerer Liste kann nichts gel�scht werden */
    if (list == NULL)
		return 0;

    /* Durchlaufe die Liste */    
    current = list->first;

    while(current)
    {
        list->first = current->next;
        free(current);
        current = list->first;
    }
    
    init(list);
    return 1;
}
        
/*  ------------------------------------------
    Name      : forall
    Zweck     : Ruft die �bergebene Funktion f�r alle Elemente der Liste auf 
    Parameter : Zeiger auf Struktur f�r Liste
                Zeiger auf Funktion
    R�ckgabe  : Anzahl der erfolgreichen Aufrufe dieser Funktion
    ------------------------------------------*/    
int forall(struct List *list, ItemFunction exec)
{
    struct Element* current;
    int no_of_execs = 0;

    /* Bei leerer Liste kann nichts aufgerufen werden */
    if (list == NULL)
		return 0;

    /* Durchlaufe die Liste */    
    for(current = list->first; current != NULL; current = current->next)
        no_of_execs += exec(current);
    
    return no_of_execs;
}

/*  ------------------------------------------
    Name      : printItem
    Zweck     : Druckt ein Listenelement aus
    Parameter : Zeiger auf ein Element
    R�ckgabe  : Anzahl der erfolgreichen Aufrufe dieser Funktion
    ------------------------------------------*/    
int printItem(struct Element *element)
{
	if (element == NULL)
		return 0;

    printf("%s: %d\n", element->datum, element->tagesproduktion);
    return 1;
}

/*  ------------------------------------------
    Name      : print
    Zweck     : Druckt die ganze Liste aus
    Parameter : Zeiger auf Struktur f�r Liste
    R�ckgabe  : keine
    ------------------------------------------*/    
void print(struct List *list)
{
    int n;
    if (list == NULL)
		return;
   
    printf("The list contains:\n");
    n = forall(list, printItem);
    printf("Total of %d item%s.\n", n, (n==1)? "" : "s");
}

