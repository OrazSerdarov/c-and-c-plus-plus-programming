/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 63
*/

#include <iostream>

using namespace std;

class Rational
{  
private:
  long zaehler,nenner;
  long ggT (long a,long b);

public:
  Rational()  
  { cout << "Standardkonstruktor" << endl;
    set(0, 1); 
  }
  
  Rational(long gz) // f�r Ganzzahlen
  { cout <<"Konstruktor " << gz << endl;
    set(gz, 1); 
  }

  Rational(long z, long n) 
  { cout <<"Konstruktor (" << z << "/" << n << ")" << endl;
    set(z, n); 
  }
  
  Rational(const Rational& r)  // Kopierkonstruktor
  { cout << "Kopierkonstruktor " << r << endl;
    set(r.zaehler, r.nenner); 
  }
  
  void set(long z, long n);
  operator double()
  { return (double)zaehler/nenner; }

  // POSTFIX-Operator
  const Rational operator++(int) { 
       Rational tmp = *this;    
       zaehler += nenner;
       return tmp;  // Kopie des nicht ver�nderten Objektes zur�ckgeben
  }

  friend Rational operator+(const Rational& a, const Rational& b);
  friend Rational operator-(const Rational& a, const Rational& b);
  friend Rational operator*(const Rational& a, const Rational& b);
  friend Rational operator/(const Rational& a, const Rational& b);
  
  friend bool operator==(const Rational& a, const Rational& b);
  friend bool operator!=(const Rational& a, const Rational& b);

  friend ostream& operator<<(ostream& o, const Rational& a);

  void kehrwert()
    { long t=zaehler; zaehler=nenner; nenner=t; }

  void kuerzen();
};

//-----------------------------------------------------------------------------------
inline void Rational::set(long z,long n)
{
  if (n)
  {  
    zaehler=z; 
    nenner=n; 
  }
  else
  {  
    zaehler=0; 
    nenner=1; 
  }

  kuerzen();
}

//-----------------------------------------------------------------------------------
long Rational::ggT(long x, long y)
{
  while(y) 
  { 
    long r = x % y; 
    x = y; 
    y = r; 
  }
  return (x);
}

//-----------------------------------------------------------------------------------
void Rational::kuerzen()
{
  int sign=1;

  if (zaehler <0) 
  { 
    sign *= (-1); 
    zaehler *= (-1); 
  }
  
  if (nenner  <0) 
  { 
    sign *= (-1); 
    nenner  *= (-1); 
  }

  long t=ggT(zaehler,nenner);

  if (t)
  { 
    zaehler = sign * zaehler / t;
    nenner  = nenner / t; 
  }
}

//-----------------------------------------------------------------------------------
Rational operator+(const Rational& a, const Rational& b)
{
    Rational temp;
    temp.set(a.zaehler * b.nenner + b.zaehler * a.nenner, a.nenner * b.nenner); 
    return temp;
}

//-----------------------------------------------------------------------------------
Rational operator-(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.nenner - b.zaehler * a.nenner, a.nenner * b.nenner); 
    return temp;
}

//-----------------------------------------------------------------------------------
Rational operator*(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.zaehler, a.nenner * b.nenner); 
    return temp;
}

//-----------------------------------------------------------------------------------
Rational operator/(const Rational& a, const Rational& b)
{ 
    Rational temp;
    temp.set(a.zaehler * b.nenner, a.nenner * b.zaehler); 
    return temp;
}

//-----------------------------------------------------------------------------------
bool operator==(const Rational& a, const Rational& b)
{ 
	Rational temp_a = a, temp_b = b;
	temp_a.kuerzen();
	temp_b.kuerzen();

	return (temp_a.zaehler == temp_b.zaehler && temp_a.nenner == temp_b.nenner);
}

//-----------------------------------------------------------------------------------
bool operator!=(const Rational& a, const Rational& b)
{ 
	return !(a==b);
}

//-----------------------------------------------------------------------------------
ostream& operator<<(ostream& o, const Rational& a)
{
   o << "(" << a.zaehler << "/"<< a.nenner << ")"; 
   return o;
}


//-----------------------------------------------------------------------------------
int main(void)
{
   Rational  a(3,4),b;

   b.set(2,5);
   cout << "b: " << (double)b << endl;

   Rational c = a + b;
   cout << a << " + " << b << " = " << c << endl;

   c = Rational(3) - b;
   cout << "3 - " << b << " = " << c << endl;

   c = a * b;
   cout << a << " * " << b << " = " << c << endl;

   c = a / b;
   cout << a << " / " << b << " = " << c++ << endl;
   cout << c;

   system("pause");
   return 0;
}




