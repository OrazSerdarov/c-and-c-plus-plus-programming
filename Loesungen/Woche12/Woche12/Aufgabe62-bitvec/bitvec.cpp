/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 62
*/

#include <iostream>
#include <algorithm>

using namespace std;

class BitVec 
{
private:
  unsigned char* vec;	                           // Vektor von 8*bytes bits
  unsigned short bytes;                            // Groesse des Vektors

public:
  BitVec (unsigned short dim);                     // Konstruktor mit gewuenschter Bit-Anzahl
  BitVec (const BitVec& v);                        // Kopierkonstruktor
  ~BitVec  ()                                      // Destruktor
    { if (vec!=NULL) delete[] vec; }

  bool operator[] (unsigned short idx);
  void set        (unsigned short idx);            // Setzen auf 1
  void reset      (unsigned short idx);            // Zuruecksetzen auf 0

  BitVec&         operator =   (const BitVec& v);  // Zuweisungsoperator
  BitVec&         operator &=  (const BitVec& v);  // UND-Verknuepfung mit Zuweisung
  BitVec&         operator |=  (const BitVec& v);  // ODER-Verknuepfung mit Zuw.

  void print();

  // Operatoren sind selbstaendig und nur mit der Klasse befreundet
  friend  BitVec  operator &   (const BitVec& v, const BitVec& w);
  friend  BitVec  operator |   (const BitVec& v, const BitVec& w);
  friend  BitVec  operator ~   (const BitVec& v);
  friend  bool	  operator ==  (const BitVec& v, const BitVec& w);
  friend  bool	  operator !=  (const BitVec& v, const BitVec& w);
};

//-----------------------------------------------------------
// Konstruktor mit gewuenschter Bit-Anzahl
//-----------------------------------------------------------
BitVec::BitVec (unsigned short dim)
{
  dim = dim <= 0 ? 8 : dim;

  // Bestimme Groesse des Arrays in Bytes (Bits/8)
  bytes = dim / 8 + (dim % 8 == 0 ? 0 : 1);

  // Reserviere Speicher
  vec = (unsigned char*) new char[bytes];

  // Setze alle Elemente auf 0
  for (int i = 0; i < bytes; i++)
    vec[i] = 0;	
} 

//-----------------------------------------------------------
// Kopierkonstruktor
//-----------------------------------------------------------
BitVec::BitVec (const BitVec& v) :
  bytes(v.bytes)
{
  // Reserviere Speicher
  vec = new unsigned char[bytes];

  // Kopiere alle Bytes
  for (int i = 0; i < bytes; i++)
    vec[i] = v.vec[i];
} 

//-----------------------------------------------------------
// Auslesen eines Eintrags (das Bit mit Nr. idx)
//-----------------------------------------------------------
inline bool BitVec::operator[] (unsigned short idx) 
{
  return vec[idx/8] & (1 << idx%8) ? true : false;
}

//-----------------------------------------------------------
// Setzen eines Eintrags (das Bit mit Nr. idx) auf 1
//-----------------------------------------------------------
inline void BitVec::set (unsigned short idx) 
{
  vec[idx/8] |= (1 << idx%8);
}

//-----------------------------------------------------------
// Setzen eines Eintrags (das Bit mit Nr. idx) auf 0
//-----------------------------------------------------------
inline void BitVec::reset (unsigned short idx) 
{
  vec[idx/8] &= ~(1 << idx%8);
} 


//-----------------------------------------------------------
// Zuweisungsoperator
//-----------------------------------------------------------
BitVec& BitVec::operator = (const BitVec& v)	
{
  // Test auf Selbstzuweisung
  if (this == &v)
    return *this;

  // Bei Groessenaenderung: neues Array
  if (bytes != v.bytes)
  {
    // Gib bisherigen Speicher frei
    if (vec)
      delete[] vec;

    // Kopiere Groesse des Arrays in Bytes 
    bytes = v.bytes;

    // Reserviere Speicher
    vec = new unsigned char[bytes];
  }
  
  // Kopiere alle Bytes
  for (int i = 0; i < v.bytes; ++i)
    vec[i] = v.vec[i];        	

  return *this;
} 

//-----------------------------------------------------------
// Zuweisung mit UND-Verknuepfung
//-----------------------------------------------------------
inline BitVec& BitVec::operator &= (const BitVec& v)	
{ 
  return (*this) = (*this) & v; 
}

//-----------------------------------------------------------
// Zuweisung mit ODER-Verknuepfung
//-----------------------------------------------------------
inline BitVec& BitVec::operator |= (const BitVec& v)	
{ 
  return (*this) = (*this) | v; 
}

//-----------------------------------------------------------
// Ausgabe 
//-----------------------------------------------------------
void BitVec::print ()
{
  for (int i = 0; i < 8*bytes; ++i)
    if ((*this)[i])
      cout << "1";
    else
      cout << "0";

  cout << endl;
}

//-----------------------------------------------------------
// Bitweise UND-Verknuepfung zweier Bit-Vektoren
//-----------------------------------------------------------
BitVec operator& (const BitVec& v, const BitVec& w)	
{
  // Definiere lokalen Ergebnisvektor
  BitVec r(8 * max(v.bytes, w.bytes));

  // Verknuepfe alle Bytes bis zur Groesse des kleineren
  for (int i = 0; i < min(v.bytes,w.bytes); ++i)
    r.vec[i] = v.vec[i] & w.vec[i];

  return r;
}

//-----------------------------------------------------------
// Bitweise ODER-Verknuepfung zweier Bit-Vektoren
//-----------------------------------------------------------
BitVec operator| (const BitVec& v, const BitVec& w)
{
  // Definiere lokalen Ergebnisvektor
  BitVec r(8 * max(v.bytes, w.bytes));

  // Verknuepfe alle Bytes bis zur Groesse des kleineren
  for (int i = 0; i < min(v.bytes,w.bytes); ++i)
    r.vec[i] = v.vec[i] | w.vec[i];

  return r;
}

//-----------------------------------------------------------
// Bitweises Komplement eines Vektors
//-----------------------------------------------------------
BitVec operator~ (const BitVec& v)		
{
  // Definiere lokalen Ergebnisvektor
  BitVec r(v.bytes * 8);

  // Aendere alle Bytes
  for (int i = 0; i < v.bytes; ++i)
       r.vec[i] = ~v.vec[i];

  return r;
} 

//-----------------------------------------------------------
// Test auf Gleichheit zweier Vektoren
//-----------------------------------------------------------
bool operator ==  (const BitVec& v, const BitVec& w)
{
  // Bestimme Groesse des kleinern
  int i, smaller = min(v.bytes, w.bytes);

  // Vergleiche die Bytes
  for (i = 0; i < smaller; ++i)	
    if (v.vec[i] != w.vec[i])
      return false;

  // Zusaetzliche Bytes in v duerfen 0 sein
  for (i = smaller; i < v.bytes; ++i)	
    if (v.vec[i] != 0)
      return false;

  // Zusaetzliche Bytes in w duerfen 0 sein
  for (i = smaller; i < w.bytes; ++i)	
    if (w.vec[i] != 0)
      return false;

  // Wenn wir bis hierhin kamen, sind beide gleich
  return true;
} 

//-----------------------------------------------------------
// Test auf Ungleichheit zweier Vektoren
//-----------------------------------------------------------
inline bool operator !=  (const BitVec& v, const BitVec& w)	
{ 
  return !(v == w);
}

//-----------------------------------------------------------
// Beispielprogramm
//-----------------------------------------------------------
int main ()
{
    BitVec s(16);  BitVec t(16);  BitVec u(16);
    s.set(3); s.set(10); s.set(12);
    t.set(8); t.set(10); t.set(11);

    cout << "s =      "; s.print();  
    cout << "t =      "; t.print();

    u = s | t;  
    cout << "s | t =  "; u.print();

    u = s & t;  
    cout << "s & t =  "; u.print();

    u = ~s;     
    cout << "~s =     "; u.print();

    u &= t;     
    cout << "~s & t = "; u.print();
    
    system("pause");
    return 0;
}
