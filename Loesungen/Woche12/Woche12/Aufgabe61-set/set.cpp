/* Programmieren III
   HS Coburg
   Woche 12, Aufgabe 61
*/

#include <iostream>

using namespace std;


//  Klassendefiniton von Set
class Set
{
private:
  int  maxCard;
  int* elems;
  int  card;

public:
  enum ErrCode {OK, ADDERR, RMVERR};

  Set(int size = 50) : maxCard(size), card(0) 
  { elems = new int[size]; }                      // Default-Konstruktor

  Set(const Set& anotherSet) : elems(0)           // Kopierkonstruktor
  { *this = anotherSet; }
  
  ~Set()
  { if (elems != 0) delete[] elems; }             // Destruktor

  Set& operator=(const Set& anotherSet);          // Zuweisungsoperator
  
  ErrCode         AddElem(int);                   // Methode zum Hinzufuegen
  ErrCode         RmvElem(int);                   // Methode zum Entfernen eines Elements

  // Operatoren sind selbstaendig und nur mit der Klasse befreundet
  friend bool     operator& ( int, const Set&);
  friend bool     operator==(const Set&, const Set&);
  friend bool     operator!=(const Set&, const Set&);
  friend Set      operator* (const Set&, const Set&);
  friend Set      operator+ (const Set&, const Set&);
  friend Set      operator- (const Set&, const Set&);
  friend bool     operator<=(const Set&, const Set&);
  friend ostream& operator<<(ostream&, const Set&);
  friend istream& operator>>(istream&, Set&);
};

//-----------------------------------------------------------
// Zuweisungsoperator
//-----------------------------------------------------------
Set& Set::operator=(const Set& anotherSet)
{
    if (this != &anotherSet)
    {
        maxCard = anotherSet.maxCard;
        card = anotherSet.card;
        
        if (elems != 0)
            delete[] elems;
            
        elems = new int[maxCard];
        
        for(int i = 0; i < maxCard; i++)
            elems[i] = anotherSet.elems[i];
    }
    
    return *this;
}
    
//-----------------------------------------------------------
// Test ob ein Element enthalten ist
//-----------------------------------------------------------
bool operator&(int z, const Set& s)
{
  int i;
  for(i=0; (i < s.card) && (z != s.elems[i]); i++);

  if (i == s.card)  // Falls die Schleife am Ende der Menge ist
    return false;

  return true;
}

//-----------------------------------------------------------
// Test ob eine Menge eine Teilmenge einer anderen ist
//-----------------------------------------------------------
bool operator<=(const Set& s, const Set& t)
{
  int i;
  // Jedes Element der einen muss in der zweiten enthalten sein
  for(i=0; (i < s.card) && (s.elems[i] & t); i++);

  if (i == s.card) // Falls die Schleife am Ende der Menge ist
    return true;

  return false;
}

//-----------------------------------------------------------
// Test ob zwei Mengen gleich sind
//-----------------------------------------------------------
bool operator==(const Set& s, const Set& t)
{
  if ((s.card == t.card) &&   // Gleiche Kardinalitaet
      (s <= t))               // und s in t enthalten
      return true;

  return false;
}

//-----------------------------------------------------------
// Test ob zwei Mengen verschieden sind
//-----------------------------------------------------------
bool operator!=(const Set& s, const Set& t)
{
  return !(s==t);
}

//-----------------------------------------------------------
// Schnittmenge zweier Mengen
//-----------------------------------------------------------
Set operator*(const Set& s, const Set& t)
{
  Set u;  // lokales Objekt

  for(int i=0;i < s.card; i++)
    if(s.elems[i] & t)   // Wenn Element von s auch in t
    {
      // Fuege es hinzu und erhoehe den Elementzaehler
      u.elems[u.card++] = s.elems[i];  
    }

  return u;
}

//-----------------------------------------------------------
// Reduktion einer Menge um eine andere
//-----------------------------------------------------------
Set operator-(const Set& s, const Set& t)
{
  Set u;  // lokales Objekt

  for(int i=0;i<s.card;i++)
    if(!(s.elems[i] & t))  // Wenn Element von s nicht in t
    {
      // Fuege es hinzu und erhoehe den Elementzaehler
      u.elems[u.card++]=s.elems[i];
    }

  return u;
}

//-----------------------------------------------------------
// Vereinigung zweier Mengen
//-----------------------------------------------------------
Set operator+(const Set& s, const Set& t)
{
  Set u = s; // lokale Kopie von s

  for(int j=0; j < t.card; j++)
  {
    // Fuege alle Elemente von t der Menge u hinzu
    // Doppelte Elemente werden beim Einfuegen mit AddElem
    // vermieden
     if (u.AddElem(t.elems[j]) == Set::ADDERR)
        cerr << "Overflow in Union-Operator" << endl;
  }

  return u;
}


//  Ein- und Ausgabe

//-----------------------------------------------------------
// Ausgabeoperator
//-----------------------------------------------------------
ostream& operator<< (ostream& os, const Set& s)
{
  if(!s.card)   // bei leerer Menge
    os << " {}" << endl;
  else
  {
    os << " {"; // drucke bis zum vorletzten mit Komma 

    for(int i=0;i<s.card-1;i++)
      os << s.elems[i] << ",";

    os << s.elems[s.card-1];
    os << "}" << endl;
  }

  return os;
}

//-----------------------------------------------------------
// Eingabeoperator
//-----------------------------------------------------------
istream& operator>> (istream& in, Set& s)
{
  int z;
  in >> z;               // lies eine Ganzzahl ein

  if(s.AddElem(z))       // Fuege diese der Menge hinzu
     cerr << "Overflow!"<<endl;

  return in;
}

//  Elemente anfuegen/entfernen

//-----------------------------------------------------------
// Hinzufuegen eines neuen Elements
//-----------------------------------------------------------
Set::ErrCode Set::AddElem(int z)
{
  // Ist die maximale Anzahl von Elemente erreicht? => Fehler
  if((card==maxCard)) 
    return ADDERR;

  // Ist Element bereits in der Menge? => nichts zu tun					
  if (z&(*this))																//?????
    return OK;

  // Fuege es hinzu und erhoehe den Elementzaehler
  elems[card++]=z;

  return OK;
}

//-----------------------------------------------------------
// Entfernen eines Elements
//-----------------------------------------------------------
Set::ErrCode Set::RmvElem(int z)
{
  // Ist die Menge schon leer? => Fehler
  if(!card) 
    return RMVERR; 

  // Kopiere die Menge um
  for (int i=0; i < card; i++)
     if (elems[i] == z) {
        for(; i < card; i++)
           elems[i] = elems[i+1];

        card--;      // Vermindere den Elementzaehler
	    break;
     }

  return OK;
}

//   Hauptprogramm

int main()
{
  Set a,b,c;

  a.AddElem(1);a.AddElem(3);a.AddElem(5);a.AddElem(7);
  b.AddElem(3);b.AddElem(2);b.AddElem(1);
  c = a;

  cout << "a   =" << a; cout << "b   =" << b;  cout << "c   =" << c;

  c = a + b;   cout << "a+b =" << c;
  c = a - b;   cout << "a-b =" << c;
  c = a * b;   cout << "a*b =" << c;
  b.RmvElem(3);

  cout << "a   =" << a; cout << "b \\ {3}  =" << b;  cout << "c = a*b =" << c;

  if(c==c) cout << " c = c"<< endl;
  if(a!=b) cout << " a <> b"<< endl;
  if(c<=a) cout << " c <=a"<< endl;
  if(5&a)  cout << " 5 in a"<< endl;

  cout  << " Zusaetzliches Element fuer a : ";
  cin >> a;  cout << "a   =" << a;

  system("pause");
  return 0;
}

