/* Programmieren III
   HS Coburg
   Woche 11, Aufgabe 60
*/

#include <iostream>
#include <algorithm>

using namespace std;

int f(void)
{
	int a[10]={3, 5, -8, 13, 21, 34, -55, 89, 144, 233}; 
	int n = 0;
	
	if (all_of(a, a+10, [&] (int v) -> bool {
		cout << "v: "<< v << endl; 
		if (v < 0) { n++; return false; }
		    return true;
	}))
		cout << n << ", +" << endl;
	else
		cout << n << ", -" << endl;



	return 0;
}

int n = 0;
bool vorz(int v) {
	cout << "v: "<< v << endl; 
     if (v < 0) { n++; return false; }
	    return true;
}	     

int f1()
{
	int a[10]={3, 5, -8, 13, 21, 34, -55, 89, 144, 233}; 
	
	if (all_of(a, a+10, vorz))
		cout << n << ", +" << endl;
	else
		cout << n << ", -" << endl;

	return 0;
}

class Funktor
{
private:
	int* _n;  // Indirektion, damit auch im kopierten Objekt noch erhalten

public:
	Funktor(int* z = nullptr) : _n(z) {}

	bool operator()(int v) {
		cout << "v: " << v << endl;
		if (v < 0) { (*_n)++; /*cout << *_n;*/  return false; }
		return true;
	}

	int getN() { return *_n; }
};

int f2()
{
	int a[10] = { 3, 5, -8, 13, 21, 34, -55, 89, 144, 233 };
	int n = 0;
	Funktor fk(&n);

	if (all_of(a, a + 10, fk))  // Funktor wird immer by-Value �bergeben, also kopiert
		cout << fk.getN() << ", +" << endl;
	else
		cout << fk.getN() << ", -" << endl;

	return 0;
}

int main()
{
	f();

	cout << endl;
	f1();

	cout << endl;
	f2();

	system("pause");
	return 0;
}
