/* Programmieren III
   HS Coburg
   Woche 11, Aufgabe 57
*/

#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
    map<string,string>  airportlist;
    
    airportlist["JFK"] = "New York (J.F.Kennedy)";
    airportlist["MUC"] = "Muenchen";
    airportlist["FRA"] = "Frankfurt Rhein/Main";
    airportlist["TXL"] = "Berlin Tegel";
    airportlist["DUS"] = "Duesseldorf";
    
    cout << "Alle Eintraege: " << endl;
    
    for(auto iter = airportlist.begin(); iter != airportlist.end(); iter++)
        cout << iter->second << " (" << iter->first << ")" << endl;
        
    system("pause");
    return 0;
}    


