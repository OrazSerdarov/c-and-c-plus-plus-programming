/* Programmieren III
   HS Coburg
   Woche 11, Aufgabe 56
*/

#include <iostream>
#include <iterator>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

int main()
{
	set<string>   begriffe;
    string        eingabe;
    
    while(true)
    {
        cout << "Begriff: ";
        char buffer[80];
		cin.getline(buffer, 80);
		eingabe = buffer;
        
        if (eingabe == "x")
            break;
            
        if (begriffe.insert(eingabe).second == false)
            cerr << "Ausdruck bereits vorhanden!!!" << endl;
        else
            cout << "Ok" << endl;
    }
    
    cout << endl << "Alle eingegebenen Begriffe: " << endl;
    
	//for(auto i = begriffe.begin(); i != begriffe.end(); i++)
	//	cout << *i << endl;
	//for(auto i : begriffe) cout << i << endl;

	copy (begriffe.begin(), begriffe.end(), ostream_iterator<string> (cout, "\n"));

	system("pause");
    return 0;
}    
        


