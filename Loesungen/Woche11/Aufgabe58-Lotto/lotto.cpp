/* Programmieren III
   HS Coburg
   Woche 11, Aufgabe 58
*/

#include <iostream>
#include <string>
#include <set>
#include <iterator> // fuer ostream_iterator
#include <cstdlib>  // fuer random() 
#include <iomanip>  // fuer setw()
#include <ctime>    // fuer time()

using namespace std;

typedef unsigned int UINT;

UINT neue_zufallszahl(set<UINT>& zahlenmenge)
{
  UINT z;
  // gibt es die neue Zahl schon?
  bool exists;

  do
  {
    exists = false;

    // Zufallszahl zwischen 1 und 49
    z = 1 + (UINT) (49.0*rand()/(RAND_MAX+1.0));

    // Fuege ein und pruefe ob bereits vorhanden
    if (zahlenmenge.insert(z).second == false)
    	exists = true;
  } while (exists);

  return z;
}

UINT zusatzzahl(set<UINT>& zahlenmenge)
{
  UINT z;
  // gibt es die neue Zahl schon?
  bool exists;

  do
  {
    exists = false;

    // Zufallszahl zwischen 1 und 49
    z = 1 + (UINT) (49.0*rand()/(RAND_MAX+1.0));

    // Fuege ein und pruefe ob bereits vorhanden
    if (zahlenmenge.find(z) != zahlenmenge.end())
    	exists = true;
  } while (exists);

  return z;
}

int main()
{
  const UINT anzahl = 6;
  set<UINT> zahlen;
  UINT zusatzz;

  // Initialisiere Zufallszahlengenerator
  srand((UINT)time(0));
  
  // �berspringe erste Zufallszahl
  rand();   

  // Bestimme 6 Zufallszahlen
  for(UINT i=0; i<anzahl; i++)
    neue_zufallszahl(zahlen);
    
  // und die Zusatzzahl
  zusatzz = zusatzzahl(zahlen);

  // Gib das Ergebnis aus
  cout << "Die Lottozahlen: ";

  copy (zahlen.begin(), zahlen.end(), ostream_iterator<int>(cout," "));

  cout << "(" << zusatzz << ")" << endl;
  cout << "Diese Angabe ist wie immer ohne Gew�hr." << endl;

  system("pause");
  return 0;
}
