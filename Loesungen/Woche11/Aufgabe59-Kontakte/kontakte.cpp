/* Programmieren III
   HS Coburg
   Woche 11, Aufgabe 59
*/

#include <iostream>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

class Person {
private:
	string name;
public:
	Person(const string& nm) : name(nm) {}
	const string& getName() const {
		return name; }
};

string g_name;
bool findname(Person* p)
{
	return (g_name == p->getName());
}

class PersonFinder
{
private:
	string name;

public:
	PersonFinder(const string& nm) : name(nm) {};

	bool operator()(Person* p) {
		return (name == p->getName());
	}
};

class Kontakte { 
private:
	list<Person*> personen; 

public:
	void append(Person* p){
		personen.push_back(p);
	}

	void printAll() { 
		for (auto p : personen) {
			if (p)
				cout << p->getName() << endl;
		}

		//for (auto iter = personen.begin(); 
		//	iter != personen.end(); iter++) { 
		//	cout << (*iter)->getName() << endl; 
		//}
	}

	Person* contains(const string& aName)
	{

		// 1. Möglichkeit: Funktorobjekt
		auto iter = find_if(personen.begin(), personen.end(), PersonFinder(aName));
		
		// 2. Möglichkeit: Prädikatsfunktion mit globaler Variable
		// g_name = aName;
		// auto iter = find_if(personen.begin(), personen.end(), findname);

		// 3. Möglichkeit: Lambda-Ausdruck
		//auto iter = find_if(personen.begin(), personen.end(), 
		//	[&] (Person* p) -> bool {
		//		if (aName == p->getName())
		//			return true;
		//		return false;
		//});
		if (iter == personen.end())
			return nullptr;
		else
			return *iter;
	}
	
};

int main() 
{
	Person* p1 = new Person("Knut Wuchtig");
	Person* p2 = new Person("Hasso Zorn");
	Kontakte k;

	k.append(p1);
	k.append(p2);

	cout << "Personen in den Kontakten:" << endl;
	k.printAll();

	Person* res = k.contains("Knut Wuchtig");
	if (res)
		cout << res->getName() << " gefunden!" << endl;
	else
		cout << "Nicht enthalten!" << endl;

	res = k.contains("Xaver Rosig");
	if (res)
		cout << res->getName() << " gefunden!" << endl;
	else
		cout << "Nicht enthalten!" << endl;

	cin.peek();
	return 0;
}



