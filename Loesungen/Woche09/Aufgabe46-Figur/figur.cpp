/* Programmieren III, WS 2010/11
   HS Coburg
   Woche 9, Aufgabe 46
*/

#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

using namespace std;

#define M_PI 3.14

// ----------------------------------------------------
class Figur
{
public:
    virtual double flaeche() const = 0;
    virtual double umfang() const = 0;
};

// ----------------------------------------------------
class Kreis : public Figur
{
private:
    double radius;
    
public:
    Kreis(double _r) : radius(_r) {}
    
    double flaeche() const {
        return radius * radius * M_PI; }
        
    double umfang() const {
        return 2.0 * radius * M_PI; }
};

// ----------------------------------------------------
class Rechteck : public Figur
{
private:
    double b, h;
    
public:
    Rechteck(double _breite, double _hoehe) : 
        b(_breite), h(_hoehe) {}
        
    double flaeche() const {
        return b * h; }
        
    double umfang() const {
        return 2 * (b + h); }
};
        
// ----------------------------------------------------
class Dreieck : public Figur
{
private:
    double a, b, c;
    
public:
    Dreieck(double _a, double _b, double _c) : 
        a(_a), b(_b), c(_c) {}
        
    double flaeche() const {
        double s = umfang() / 2.0;
        return sqrt(s * (s - a) * (s - b) * (s - c)); }
        
    double umfang() const {
        return (a + b + c); }
};
        
// ----------------------------------------------------
void figurDaten(Figur* f)
{
    cout << "Flaeche: " << f->flaeche() << endl;
    cout << "Umfang:  " << f->umfang()  << endl;
}

// ----------------------------------------------------
int main()
{
    Kreis     k(5.0);
    Rechteck  r(3.0, 4.0);
    Dreieck   d(5.0, 5.0, 6.0);
    
    cout << "Daten zum Kreis: " << endl;
    figurDaten(&k);
    
    cout << endl << "Daten zum Rechteck: " << endl;
    figurDaten(&r);
    
    cout << endl << "Daten zum Dreieck: " << endl;
    figurDaten(&d);
    
	//Figur* f = new Kreis[10];
	//figurDaten(f);

    system("pause");
    return 0;
}
    
        

