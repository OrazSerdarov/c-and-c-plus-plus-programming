/* Programmieren III
   HS Coburg
   Woche 9, Aufgabe 48
*/

#ifndef _SORTIERSTRATEGIE_H
#define _SORTIERSTRATEGIE_H

class Sortierstrategie
{
public:
       Sortierstrategie() {}
       virtual ~Sortierstrategie() {}  // oder: = default;
	          
       virtual void sortiere(int* arr, int size) = 0;
};

#endif
