/* Programmieren III
   HS Coburg
   Woche 9, Aufgabe 48
*/

#include <iostream>
#include "sortieralgorithmen.h"

using namespace std;

// ------------------------------------------------------
void BubbleSort::sortiere(int* arr, int size)
{
     cout << "Hier BubbleSort-Verfahren!" << endl;
     for(int i = 0; i < size; i++)
         for(int j = 0; j < size - 1; j++)
             if(arr[j] > arr[j + 1])
             {
                 int temp = arr[j];
                 arr[j] = arr[j + 1];
                 arr[j + 1] = temp;
             }
}

// ------------------------------------------------------
void MergeSort::sortiere(int* arr, int size)
{
     cout << "Hier MergeSort-Verfahren!" << endl;

     v = arr;
     sz = size;
     
     auxarr = new int[size];
     mgsort(0, size - 1);
     delete[] auxarr;
}

// ------------------------------------------------------
void MergeSort::mgsort(int left, int right)
{
     if (right <= left)
         return;
         
     int k;
     int mid = (left + right)/2;
     mgsort(left, mid);
     mgsort(mid + 1, right);
         
     for(k = left; k <= mid; k++)
         auxarr[k] = v[k];
             
     for(k = mid + 1; k <= right; k++)
         auxarr[right + mid - k + 1] = v[k];
             
     int toleft = right;
     int toright = left;
         
     for(k = left; k <= right; k ++)
         if(auxarr[toright] < auxarr[toleft])
             v[k] = auxarr[toright++];        
         else
             v[k] = auxarr[toleft--];
}  

// ------------------------------------------------------
void QuickSort::sortiere(int* arr, int size)
{
     cout << "Hier QuickSort-Verfahren!" << endl;

     v = arr;
     sz = size;
     
     qsort(0, size - 1);
}

// ------------------------------------------------------
void QuickSort::qsort(int left, int right)
{
    int toleft = right;
    int toright = left;
    
    if (toright >= toleft)
        return;
        
    int pivot = v[(toright + toleft) / 2];
    
    while(toright <= toleft)
    {
        while((toright < right) && (v[toright] < pivot))
            toright++;
            
        while((toleft > left) && (v[toleft] > pivot))
            toleft--;
            
        if(toright <= toleft)
        {
            int t = v[toright];
            v[toright] = v[toleft];
            v[toleft] = t;
            toright++;
            toleft--;
        }        
    }
    
    if(toleft > left)
        qsort(left, toleft);
    
    if (toright < right)
        qsort(toright, right);
}  
