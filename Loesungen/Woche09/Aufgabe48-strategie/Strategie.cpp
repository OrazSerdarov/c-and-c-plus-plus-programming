/* Programmieren III
   HS Coburg
   Woche 9, Aufgabe 48
*/

#include <iostream>
#include <cstdio>
#include <ctime>
#include <memory>
#include "sortiertesfeld.h"
#include "sortierstrategie.h"
#include "sortieralgorithmen.h"

using namespace std;

// ------------------------------------------------------
void fuelleFeld(SortiertesFeld& f)
{
    for(int i = 0; i < 12; i++)
          f.fuegeHinzu(1 + (int)(100.0*rand()/(RAND_MAX+1.0)));
}     

// ------------------------------------------------------
void testeStrategie(const char* bezeichnung, SortiertesFeld& feld)
{
     cout << "Teste Verfahren: " << bezeichnung << endl;
     cout << "Vor dem Sortieren:  ";
     feld.gibAus();
     cout << endl;
     
     feld.sortiere();
     
     cout << "Nach dem Sortieren: ";
     feld.gibAus();
     cout << endl << endl;
}     

// ------------------------------------------------------
int main()
{
	auto felder = make_unique<SortiertesFeld[]>(3);

    // Fuelle Felder
    srand(time(0));
 
    for(int i=0; i<3; i++)
		fuelleFeld(felder[i]);
    
    // Setze Strategien
	felder[0].setzeStrategie(new BubbleSort);
    felder[1].setzeStrategie(new MergeSort);
    felder[2].setzeStrategie(new QuickSort);
    
    // Starte Tests
    testeStrategie("Bubble Sort", felder[0]);
    testeStrategie("Merge Sort", felder[1]);
    testeStrategie("Quick Sort", felder[2]);
    
    cin.peek();
	
    return 0;
}
    
