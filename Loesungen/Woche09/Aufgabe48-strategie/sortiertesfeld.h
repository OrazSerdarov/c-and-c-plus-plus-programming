/* Programmieren III
   HS Coburg
   Woche 9, Aufgabe 48
*/

#ifndef _SORTIERTESFELD_H_
#define _SORTIERTESFELD_H_

#include "sortierstrategie.h"
#include <array>
#include <memory>

const int MAXGROESSE = 100;

class SortiertesFeld
{
private:
       std::array<int, MAXGROESSE>		arr;
	   int								groesse;
       std::unique_ptr<Sortierstrategie>		strategie;
        
public:
       SortiertesFeld() : groesse(0) {  }

       SortiertesFeld(Sortierstrategie* stra) : groesse(0),
           strategie(stra)
       {  }

       ~SortiertesFeld() { }
           
       void setzeStrategie(Sortierstrategie* stra);
       
       void fuegeHinzu(int nummer);
       
       void gibAus() const;
       
       void sortiere();
};

#endif
       
                      
           
