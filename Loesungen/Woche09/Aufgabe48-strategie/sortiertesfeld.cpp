/* Programmieren III, WS 2010/11
   HS Coburg
   Woche 9, Aufgabe 48
*/

#include <iostream>
#include <iomanip>
#include "sortiertesfeld.h"
#include "sortierstrategie.h"

using namespace std;

void SortiertesFeld::setzeStrategie(Sortierstrategie* stra)
{
     if (stra == nullptr)
     {
         cerr << "Keine Strategie angegeben!" << endl;
         return;
     }
     
     strategie = unique_ptr<Sortierstrategie>(stra);
}
         
void SortiertesFeld::fuegeHinzu(int nummer)
{
     if (groesse < MAXGROESSE - 1)
        arr[groesse++] = nummer;
     else
         cerr << "Maximalgroesse erreicht!" << endl;
}
      
void SortiertesFeld::gibAus() const
{
     if (groesse == 0)
     {
         cout << "Feld ist leer!" << endl;
         return;
     }
     
     for(int i = 0; i < groesse; i++)
         cout << setw(4) << arr[i] << " ";
}

void SortiertesFeld::sortiere()
{
     if (strategie == nullptr)
     {
         cerr << "Keine Strategie angegeben!" << endl;
         return;
     }

     strategie->sortiere(arr.data(), groesse);
}     
    

