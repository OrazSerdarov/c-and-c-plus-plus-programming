/* Programmieren III
   HS Coburg
   Woche 9, Aufgabe 48
*/

#ifndef _SORTIERALGORITHMEN_H_
#define _SORTIERALGORITHMEN_H_

#include "sortierstrategie.h"

// ------------------------------------------------------
class BubbleSort : public Sortierstrategie {

public:
       BubbleSort() : Sortierstrategie() {}
       
       void sortiere(int* arr, int size);
};

// ------------------------------------------------------
class MergeSort : public Sortierstrategie {
private:
       int* v;
       int* auxarr;
       int sz;
       
       void mgsort(int left, int right);
       
public:
       MergeSort() : Sortierstrategie() {}
       
       void sortiere(int* arr, int size);
};

// ------------------------------------------------------
class QuickSort : public Sortierstrategie {
private:
       int* v;
       int sz;
       
       void qsort(int left, int right);

public:
       QuickSort() : Sortierstrategie() {}
       
       void sortiere(int* arr, int size);
};

#endif
