#include <iostream>

using namespace std;

class B
{
public:
	B() :d(0.0f) {}
	B(float _x) { d = _x; }
	float d;
};

class A : public B
{
public:
	A(int _k) /*: B(0.0f)*/ { i = _k; }
	int i;
};

int main()
{
	A a(3);
	B b;

	cout << "i: " << a.i << ", d: " << a.d << endl;
	cout << "b.d: " << b.d << endl;

	cin.peek();

	return 0;
}