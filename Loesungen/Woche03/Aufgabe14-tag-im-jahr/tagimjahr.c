/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 14
*/

#include <stdio.h>
#include <conio.h>

int istSchaltjahr(unsigned int jahr);
unsigned int tagDesJahres(  unsigned int tag, unsigned int monat, 
                            unsigned int jahr, unsigned int isj);

const int MONATSLAENGE[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int main()
{
    unsigned int tag, monat, jahr;
    int isj;
    unsigned int tdj;
    
    printf("Bitte geben Sie ein Datum ein (TT.MM.JJJJ)!  : ");
    scanf("%d.%d.%d", &tag, &monat, &jahr);
    
    //printf("Monat : ");
    //scanf("%ud", &monat);
    
    //printf("Jahr  : ");
    //scanf("%ud", &jahr);
    
    if (jahr < 100)
        jahr += 2000;
    
    isj = istSchaltjahr(jahr);
        
    if (monat == 0 || monat > 12 || tag == 0 || 
        (monat != 2 && tag > MONATSLAENGE[monat-1]) ||
        (monat == 2 && tag > MONATSLAENGE[monat-1] + isj))
    { 
        printf("""%2u.%2u.%u"" ist ein ungueltiges Datum!\n", 
                tag, monat, jahr);
        getch();
        return -1;
    }
    
    tdj = tagDesJahres(tag, monat, jahr, isj);
    
    printf("Der %2u.%2u. ist der %u. Tag des Jahres %u\n", 
            tag, monat, tdj, jahr);
            
    getch();
    return 0;
}

/* Bestimme, ob ein Jahr ein Schaltjahr ist */
int istSchaltjahr(unsigned int jahr)
{    
    if (jahr % 4 != 0)
        return 0;
        
    if (jahr % 100 == 0)
    {
        if (jahr % 400 == 0)
            return 1;
        else 
            return 0;
    }

    return 1;
}

/* Berechne den Tag des Jahres aus einem Datum */
unsigned int tagDesJahres(  unsigned int tag, unsigned int monat, 
                            unsigned int jahr, unsigned int isj)
{
    unsigned int tdj = 0;
    int i;
    
    for( i=0; i< monat-1; i++)
        tdj += MONATSLAENGE[i];
        
    if (monat > 2)
        tdj += isj;
        
    tdj += tag;
    
    return tdj;
}
    

