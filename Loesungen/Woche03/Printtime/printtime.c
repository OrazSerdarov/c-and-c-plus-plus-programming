/* Programmieren III, WS 2010/11
   HS Coburg
   Woche 3, Aufgabe 18
*/

void printTime(unsigned short time)
{
	printf("%i%i:%i%i Uhr\n", 
		(time & 0xf000)>>12, 
		(time & 0xf00)>>8, 
		(time & 0xf0)>>4, 
		time & 0xf);
}


int main()
{
	printTime(1);
	printTime(0x0012);
	printTime(0x0815);
	printTime(0x2015);

	system("pause");
	return 0;
}
