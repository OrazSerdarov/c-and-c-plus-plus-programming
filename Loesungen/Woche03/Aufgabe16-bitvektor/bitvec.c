/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 16
*/

#include <stdio.h>

#define ARRSIZE 24

int get (char* bitvec, int index) // return the bit denoted by index
{
	if (index >= 0 && index < ARRSIZE)
	    return bitvec[index/8] & (1 << index%8) ? 1 : 0;
	else
		printf("get: Index %i ung�ltig!\n", index);
	return -1;
}

void set (char* bitvec, int index) // set the bit denoted by index to 1
{
	if (index < ARRSIZE)
		bitvec[index/8] |= (1 << index%8);
	else
		printf("set: Index %i zu gross!\n", index);
} 

void reset (char* bitvec, int index) // reset the bit denoted by index to 0
{
	if (index < ARRSIZE)
	    bitvec[index/8] &= ~(1 << index%8);
	else
		printf("reset: Index %i zu gross!\n", index);
} 

int main ()
{
	char s[ARRSIZE/8];
	char t[ARRSIZE/8];
	int i;

	// Initialize with 0
	for(i = 0; i<ARRSIZE/8; i++)
	{
		s[i] = 0;
		t[i] = 0;
	}

	set(s, 1); set(s, 6); 
	set(s, 23); set(s, 24); 
    
	set(t, 8); set(t, 10); set(t, 11);
    reset(s, 5);

	printf("s: ");
	for (i = 0; i < ARRSIZE; i++)
		printf(" %i", get(s, i));

	printf("\nt: ");
	for (i = 0; i < ARRSIZE; i++)
		printf(" %i", get(t, i));

	reset(s, 1); reset(s, 6); 
	reset(s, 23); /* reset(s, 24); */
    
	printf("\n\ns: ");
	for (i = 0; i < ARRSIZE; i++)
		printf(" %i", get(s, i));

	printf("\n");
	system("pause");
    return 0;
}

