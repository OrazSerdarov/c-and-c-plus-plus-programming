/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 15
*/

#include <stdio.h>
#include <conio.h>

#define SIZE  5
#define SWAP(X,Y)  {int t=(X); (X)=(Y); (Y)=t;} 

int main()
{
	int arr[SIZE];
	int i, j;

	/*for(i = 0; i < SIZE; i++)
	{
		int v;
		printf("Bitte %i. Wert eingeben: ", i+1);
		scanf("%d", &v);
		arr[i] = v;
	}*/
	printf("Bitte 5 Werte eingeben: ");
	scanf("%i %i %i %i %i", arr, &arr[1], arr + 2, arr + 3, arr + 4);

	for(i = 0; i < SIZE; i++)	
         for(j = 0; j < SIZE - 1; j++)
             if(arr[j] > arr[j + 1]) 
				 SWAP(arr[j], arr[j+1]);

	printf("Median ist %d.\n", arr[SIZE/2]);
	getch();
	return 0;
}