/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 17
*/

#include <stdio.h>
#include <conio.h>

int istSchaltjahr(unsigned int jahr);
unsigned int wochentag( unsigned int tag, unsigned int monat, 
                        unsigned int jahr);

const int MONATSLAENGE[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

const char WOCHENTAGE[][11] = { "Sonntag", "Montag", "Dienstag", "Mittwoch",
                        "Donnerstag", "Freitag", "Samstag"};

int main()
{
    unsigned int tag, monat, jahr;
    int isj;
    unsigned int wt;
    
    printf("Bitte geben Sie ein Datum ein!\nTag   : ");
    scanf("%ud", &tag);
    
    printf("Monat : ");
    scanf("%ud", &monat);
    
    printf("Jahr  : ");
    scanf("%ud", &jahr);
    
    if (jahr < 100)
        jahr += 2000;
    
    isj = istSchaltjahr(jahr);
        
    if (monat == 0 || monat > 12 || tag == 0 || 
        (monat != 2 && tag > MONATSLAENGE[monat-1]) ||
        (monat == 2 && tag > MONATSLAENGE[monat-1] + isj))
    { 
        printf("""%2u.%2u.%u"" ist ein ungueltiges Datum!\n", 
                tag, monat, jahr);
        getch();
        return -1;
    }
    
    wt = wochentag( tag, monat, jahr);
    
    printf("Der %2u.%2u.%u ist ein %s\n", 
            tag, monat, jahr, WOCHENTAGE[wt]);
            
    getch();
    return 0;
}

/* Bestimme, ob ein Jahr ein Schaltjahr ist */
int istSchaltjahr(unsigned int jahr)
{    
    if (jahr % 4 != 0)
        return 0;
        
    if (jahr % 100 == 0)
    {
        if (jahr % 400 == 0)
            return 1;
        else 
            return 0;
    }

    return 1;
}

/* Berechne den Wochentag nach der Formel von Zeller */
unsigned int wochentag( unsigned int tag, unsigned int monat, 
                        unsigned int jahr)
{
    int w = 0;
    unsigned int h, j;
    
    if (monat <= 2)
    {
        monat += 12;
        jahr--;
    }
    
    h = jahr / 100;
    j = jahr % 100;
 
    w = tag + (monat + 1) * 13 / 5 + 5 * j / 4 +
        h / 4 - 2 * h - 1;
    
	//printf("w: %i (w%7 = %i)\n", w, w%7);
    while (w < 0)
        w += 7;
            
    return w % 7;
}
    

