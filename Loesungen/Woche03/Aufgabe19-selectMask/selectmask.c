/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 19
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>


void printBin(int number)
{
	int i, digit;
	for( i = 0; i <= 8; i = i+1)
    {
       digit = (number >> (8-i)) & 0x01;
       printf("%d", digit);
    }
	printf("\n");
}

int* selectMask(int* a, int n, int mask, int* sz)
{
    int i, count;
    int* res;
    
    for(i=0, count=0; i < n; i++)
        if ((a[i] & mask) == mask)
            count++;
     
	*sz = count;
    res = (int*)calloc(count, sizeof(int));
    for(i=0, count = 0; i < n; i++)
        if ((a[i] & mask) == mask)
            res[count++] = a[i] & ~mask;
            
    return res;
}  

int main()
{
	int i, m;
	const int n = 20;
	int *feld = nullptr, *erg = nullptr;

	feld = (int*)malloc(n * sizeof(int));
	for(i = 0; i < n; i++)
		feld[i] = i + 1;

	erg = selectMask(feld, n, 6, &m);
	
	for(i = 0; i < n; i++)
	{
		printf("%02d: %2d = ", i, feld[i]);
		printBin(feld[i]);
	}
	printf("\n");

	for(i = 0; i < m; i++)
	{
		printf("%02d: %2d = ", i, erg[i]);
		printBin(erg[i]);
	}

	getch();
	free(feld);
	free(erg);
	return 0;
}

