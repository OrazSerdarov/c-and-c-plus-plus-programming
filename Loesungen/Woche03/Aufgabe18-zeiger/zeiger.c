/* Programmieren III
   HS Coburg
   Woche 3, Aufgabe 18
*/

#include <stdio.h>
#include <string.h>
#include <conio.h>

int main()
{
	// a ist ein Zeiger auf einen konstanten float-Wert.
	const float *a;

	// b ist ein konstanter Zeiger auf eine float-Variable.
	float x;
	float * const b = &x;	 // oder float const * b = &x;

	// c ist ein Zeiger auf einen konstanten Zeiger auf eine float-Variable.
	float * const *c;

	// d ist ein Vektor mit 3 Zeigern auf float-Variablen.
	float *d[3];

	// e ist ein Zeiger auf einen Vektor mit 3 Zeigern auf float-Variablen.
	float* (*e)[3];

	// b)	Was bedeutet der Begriff „dereferenzieren“? Nennen und erklären 
	// Sie kurz die Operatoren, die eine Dereferenzierung beinhalten.

	// Der Begriff „dereferenzieren“ bezeichnet eine Operation, die zu einem 
	// Zeiger das Objekt (und nicht nur dessen Inhalt) liefert, auf das dieser Zeiger verweist.
	// Operatoren: * [] ->

	_getch();
	return 0;
}


