/* Programmieren III
   HS Coburg
   Woche 1, Aufgabe 4
*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   char c = 'x';

   printf("char-Konst.: %d Bytes, char-Var: %d, int: %d Bytes\n",
          sizeof((char)'x'),  sizeof(c),              sizeof(10));
   printf("Inhalt der char-Var.: %c, int-Konstante: %i\n", c, 10);

   system("pause");
   return 0;
}
