/* Programmieren III
   HS Coburg
*/

/* Welche Ausgabe erzeugt das folgende Programm? */

#include  <stdio.h>

int main(void)
{
   int a=10, b, c;
   
   a*=5+10;    printf("%d\n", a);
   a*=b=c=20;  printf("%d\n", a);
   b=b==c;     printf("%d\n", b);
   a>>=b+2;    printf("%d\n", a);
   a&=0x3e;    printf("%d\n", a);
   
   a=3;
   b=2;
   a*=b+=a<<=a+b;  printf("a=%d, b=%d\n", a, b);   
  
   getch();
   return(0);
}
