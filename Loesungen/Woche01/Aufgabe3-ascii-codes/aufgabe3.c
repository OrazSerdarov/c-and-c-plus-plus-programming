/* Programmieren III
   HS Coburg
   Woche 1, Aufgabe 3
*/

#include <stdio.h>

int main(void) 
{
    short int i;

	for(i = 66; i <= 98; i++)
		printf("%c (%d)", i, i);

	printf("\n");
	
	system("pause");
	return 0;
}