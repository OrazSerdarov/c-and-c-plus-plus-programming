/* Programmieren III
   HS Coburg
   Woche 1, Aufgabe 2
*/

#include <stdio.h>

int main(void) 
{
    printf("Thomas Wieland\n");
	printf("Hochschule Coburg\n");
	printf("Friedrich-Streib-Str. 2\n");
	printf("96450 Coburg\n");
	printf("09561/317-392\n");
	printf("thomas.wieland@hs-coburg.de\n");
	
	system("pause");
	return 0;
}