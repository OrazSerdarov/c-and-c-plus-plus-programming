/* Programmieren III
   HS Coburg
*/

/*
	Zur Absicherung gegen �bertragungsfehler bei der Kommunikation 
	�ber Kabel in Datennetzen f�gt man u.U. ein Pr�fbit zu einer 
	Nachricht hinzu. Dabei versteht man unter gerader Parit�t eines Bytes, 
	dass gilt:

	a_0 + a_1 + a_2 + a_3 + a_4 + a_5 + a_6 + p = 0
 
	wobei mit a_i die zu �bertragenden Bits gemeint sind und mit p das Pr�fbit, 
	bei Addition im Dualsystem.
	
	Schreiben Sie unter Verwendung der Bitoperatoren eine C-Funktion, die zu 
	einem �bergebenen Byte (als unsigned char) das Pr�fbit berechnet und es 
	als vorderstes Bit setzt, also etwa folgende Signatur besitzt:
		unsigned char setparity(unsigned char c);
	
	Erstellen Sie auch eine Funktion, die � wie bei einem Empf�nger der Nachricht � 
	die Pr�fung erneut vornimmt, sie mit dem gesetzten Bit vergleicht und das 
	urspr�ngliche Byte oder eine Fehlermeldung zur�ckliefert. Testen Sie Ihre 
	Funktion an geeigneten Beispielen.
*/

#include <string.h>
#include <stdio.h>

unsigned char compparity(unsigned char c)
{
    unsigned char par = 0;
    int i;
    
    for(i=0; i<7; i++)
    {
        if (c & 1)
            par++;
        c >>= 1;
		if (c == 0) break;
    }
    return par % 2;
}

unsigned char setparity(unsigned char c)
{
    unsigned char par = compparity(c);
    if (par != 0)
        return c | 128;
    else
        return c & 127;
}

unsigned char checkparity(unsigned char c)
{
    unsigned char par = compparity(c);
    unsigned char bit = c >> 7;
    
    if (par != bit)
    {
        printf("Fehler bei %d!\n",c);
        return 0;
    }
    else
        return c & 127;
}

int main(void)
{
    unsigned char  eing[255];
    unsigned char  code[255];
    int i;
    
    printf("Bitte Text eingeben: ");
    gets(eing);
    
    printf("Text mit Paritaet:   ");
    for(i = 0; i < strlen(eing); i++)
    {
        code[i] = setparity(eing[i]);
        printf("%c",code[i]);
    }
    code[strlen(eing)] = 0;
    code[0] ^= 4; // Fehler einf�egen zu Testzwecken
    
    printf("\nWiederhergest. Text: ");
    for(i = 0; i < strlen(code); i++)
    {
        eing[i] = checkparity(code[i]);
        printf("%c",eing[i]);
    }
    printf("\n");
    system("pause");
    
    return 0;
}
