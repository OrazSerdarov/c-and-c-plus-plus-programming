/* Programmieren III
   HS Coburg
   Woche 1, Aufgabe 7
*/

#include <stdio.h>

int main()
{
	int y = 0;
	int ref = 0;

	char inseln[][10] = {"Bermudas", "Fidschi", "Komoren", "Kuba"};
	int index[] = {1, 3, 0, 2};

	while(y < 4) {
		printf("Insel: ");
		ref = index[y];
		printf("%s\n", inseln[ref]);
		y = y + 1;
	}

	system("pause");
	return 0;
}