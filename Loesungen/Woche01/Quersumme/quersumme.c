/* quersumme.c */
#include <stdio.h>

int main()
{
	int i;
	int quer = 0;
	char eingabe[100];

	printf("Bitte Zahl eingeben: ");
	scanf_s("%s", eingabe, 99);  // Platz f�r terminierende 0 lassen

	for (i = 0; eingabe[i]; i++)
	{
		if (isdigit(eingabe[i]))
			quer += eingabe[i] - '0';
	}

	printf("Quersumme von %s ist %d.\n", eingabe, quer);

	system("pause");
	return 0;
}