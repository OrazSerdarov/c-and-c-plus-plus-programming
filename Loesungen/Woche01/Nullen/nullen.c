/* Programmieren III
   HS Coburg
*/

int main()
{
	/* Erl�utern Sie den Unterschied zwischen 0, '0', '\0' und "0". 
	   Deklarieren Sie jeweils eine Variable eines passenden Typs und 
	   weisen Sie ihr die entsprechende Konstante zu.
	*/
	int  a   =   0;
	char b   =  '0';
	char c   = '\0';
	char d[] =  "0";

	return 0;
}