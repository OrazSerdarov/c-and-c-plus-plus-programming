/* Programmieren III
   HS Coburg
   Woche 1, Aufgabe 5
*/

#include  <stdio.h>
#include  <ctype.h>

int main(void)
{
   char eing;

   printf("    Hauptmenue\n");
   printf("    ==========\n\n");
   printf("(A)endern\n"
          "(B)eenden\n");
   printf("(D)rucken\n");
   printf("(E)ingeben\n");
   printf("(L)oeschen\n\n");
   printf("Was wuenschen Sie zu tun ?\n");

   scanf("%c", &eing);
   printf("\n\nSie haben \"%c\" gewaehlt.\n", toupper(eing));
   system("pause");
   return(0);
}
