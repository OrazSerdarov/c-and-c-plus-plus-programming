/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 21
*/

#include <stdio.h>
#include <conio.h>

int substr (const char* str1, const char * str2);
int strlaenge (const char* str);

int main(void)
{
   char quelle[40] = "Das ist unser Text!";
   char subs[10] = "ist";
   char subs2[10] = "hsc";
   
   printf("\"%s\" kommt in \"%s\" ab %i vor.\n", subs, quelle, substr(quelle, subs));   

   printf("\"%s\" kommt in \"%s\" ab %i vor.\n", subs2, quelle, substr(quelle, subs2));   
   
   _getch();
   return 0;
}

   
int substr (const char* str1, const char * str2)
{
    int i = 0;
    int l1 = strlaenge(str1);
    int l2 = strlaenge(str2);
    
    while(i < l1 - l2)
    {
        char identisch = 1;
        int k;
        
        for (k = 0; k<l2; k++)
            if ((str1+i)[k] != str2[k])
            {
                identisch = 0;
                break;
            }
            
        if (identisch)
            return i;
            
        i++;
    }
    
    return -1;
}
        
int strlaenge (const char* str)
{
    int i = 0;
    
    while(*str != 0)
    {
        str++;
        i++;
    }
    
    return i;
}

