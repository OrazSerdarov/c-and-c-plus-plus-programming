/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 26
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

const char filename[] = "memo.txt";

void openError(const char* s)
{  
	fprintf(stderr, "Fehler beim Oeffnen der Datei %s.\n", s);
	exit(1); 
}

void writeError(const char* s)
{  
	fprintf(stderr, "Fehler beim Schreiben in die Datei %s.\n", s); 
	exit(2); 
}

void readError(const char* s)
{  
	fprintf(stderr, "Fehler beim Lesen der Datei %s.\n", s); 
	exit(3); 
}

int main()
{
   char line[80];
   const int size = sizeof(line);
   int number = 0;
   int i = 0;
   FILE* memoFile;
   time_t sec;

   memoFile = fopen(filename, "a");
   if(!memoFile)
       openError(filename);

   sec = time(NULL);
   fprintf(memoFile, "%s", ctime( &sec ));    // Schreibt die aktuelle Zeit in die Datei

   printf("Geben Sie Ihre Notiz ein (max. %d Zeichen):\n"
          "(Ende mit einem Punkt in einer neuen Zeile)\n", size);
   
   while(fgets(line, size, stdin))  // Liest einige Zeilen Text von der Tastatur
   {                                // und schreibt sie in die Datei "memo.txt"
      if( line[0] == '.')
         break;
	  if( !(fprintf(memoFile, "%d: %s\n", ++i, line)) )
         writeError(filename);
	  //fflush(stdin);
   }
   fclose(memoFile) ;

   memoFile = fopen(filename, "r");
   if( !memoFile)
      openError(filename);

   while(fgets(line, size, memoFile))  // Gibt den Inhalt der Datei 
   {                                  // mit Zeilennummern am Bildschirm aus.
	  printf("%5i: %s", ++number, line);
   }

   if( !feof(memoFile))
      readError(filename);

   fclose(memoFile);
   system("pause");
   return 0;
}
