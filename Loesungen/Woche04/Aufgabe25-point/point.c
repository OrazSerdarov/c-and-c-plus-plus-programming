/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 25
*/

#include <stdio.h>
#include <conio.h>

struct Point {
	double x;
	double y;
};

void printPoint(struct Point p)
{
	printf("(%.1lf, %.1lf)", p.x, p.y);
}

int main()
{
	struct Point p1 = { -1.1, 3.5},
		p2 = { 2.4, -0.7};

	double dx = p2.x - p1.x, dy = p2.y - p1.y;

	printf("\nErster Punkt:   P1 = "); printPoint(p1);
	printf("\nZweiter Punkt:  P2 = "); printPoint(p2);
	printf("\n");

	if (dx == 0.0)
	{
		printf("Die Gerade durch beide Punkte ist eine senkrechte Linie "
			   "bei y = %.1lf\n", p1.y);
	}
	else
	{
		double m = dy / dx;
		double c = p1.y - m * p1.x;

		printf("Fuer die Gerade y = mx + c durch P1 und P2 gilt:\n");
		printf("m = %.3lf  und  c = %.3lf\n", m, c);
	}
	_getch();
	return 0;
}
