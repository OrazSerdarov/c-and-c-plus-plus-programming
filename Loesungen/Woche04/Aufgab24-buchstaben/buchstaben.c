/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 24
*/

#include  <stdio.h>

#define  LAENGE   80    /* maximale Laenge der eingegebenen Zeichenkette  */

void  lies_zeile_ein(char *string);
int   zaehl_zeichen(char *string, char zeichen);

int  main(void)
{
    char    zeichenkette[LAENGE+1],  such_zeichen;

    printf("\n\n\nGeben Sie eine Zeichenkette (max. 80 Zeichen) ein !\n\n");

    lies_zeile_ein(zeichenkette);  /* Einlesen einer Zeichenkette in einer   */
                                   /*  Zeile in den Vektor   zeichenkette    */

    printf("  zu zaehlendes Zeichen: ");
    such_zeichen=getchar();
 
    printf("\n---> Zeichen %c ist in der obigen Zeichenkette\n", such_zeichen);
    printf("      %d mal enthalten\n",zaehl_zeichen(zeichenkette, such_zeichen));

	getch();
   return(0);
}

void  lies_zeile_ein(char *string)
{
    int  i=0;

    do  {                      /*  hier werden die Zeichen einzeln eingelesen  */
       string[i++]=getchar();  /*  und zwar in das Array  string,              */
                               /*  bis 80 Zeichen oder RETURN (= \n-Zeichen)   */
    } while (string[i-1]!='\n'  &&  i<LAENGE);     /*  eingegeben wurde      */
  
    string[i]='\0';          /*  Stringende kennzeichnen                     */
}

int  zaehl_zeichen(char *string, char zeichen)
{
    int   zaehler=0;

    do  {
       if (*string == zeichen) /* Wenn ein uebereinstimmendes Zeichen    */
          ++zaehler;           /* gefunden wird, dann  zaehler  erhoehen */
    } while (*string++);       /* bis string -Zeiger auf Null zeigt      */
 
    return(zaehler);
}
