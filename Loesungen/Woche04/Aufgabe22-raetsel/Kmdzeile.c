/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 22
*/

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char **pp, *z;
	if (argc > 1)
	{
		pp = argv + 1;
		z = *pp + strlen(*pp);

		while (*pp - --z)
			printf("%c", --*z);
		printf("%c\n", *z);
		printf("%s\n", argv[0]);
	}

	getch();
	return 0;
}
