/* Programmieren III
   HS Coburg
   Woche 4, Aufgabe 20
*/

#include <stdio.h>
#include <string.h>
#include <conio.h>

void strcpy1 (char *s, const char *t)
{
   int i = 0;
   while (s[i] = t[i])
      i++;
}

void strcpy2 (char * s, char * t)
{
   while (*s = *t)
   {
	   s++;
	   t++;
   }
}

int main(void)
{
   char quelle[40] = "Das ist unser Text!";
   char ziel1[40];
   char ziel2[40];
   
   strcpy1(ziel1, quelle);
   printf("Ergebnis 1: \"%s\"\n", ziel1);
   
   strcpy2(ziel2, quelle);
   printf("Ergebnis 2: \"%s\"\n", ziel2);
   
   _getch();
   return 0;
}
   


