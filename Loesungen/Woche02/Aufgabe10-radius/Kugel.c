/* Programmieren III
   HS Coburg
   Woche 2, Aufgabe 10
*/

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

#define PI 3.1415927
#define QUAD(X)    ((X)*(X))
#define CUBE(X)    ((X)*(X)*(X))

//inline double CUBE(double x) { return x*x*x; }

int main()
{
	double r;

	printf("Bitte geben Sie den Radius der Kugel ein: ");
	scanf("%lf", &r);

	printf("\nOberflaeche: %.3lf, Volumen: %.3lf\n",
		4.0 * M_PI * QUAD(r), 4.0/3.0 * PI * CUBE(r));

	getch();
	return 0;
}
