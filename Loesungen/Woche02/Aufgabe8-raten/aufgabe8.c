/* Programmieren III
   HS Coburg
   Woche 2, Aufgabe 8
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int gesucht, tipp, versuche;

	//printf("time: %li", time(0));
	srand(time(0));
	//printf(" rand: %i\n", rand());
	
	rand();
	versuche = 1;
	gesucht = (int) (10.0*rand()/(RAND_MAX+1.0));

	//printf("gesucht: %d\n", gesucht);
	do 
	{
		printf("Bitte eine Zahl eingeben: ");
		scanf("%i", &tipp);

		if (tipp == gesucht)
		{
			printf("RICHTIG! Du hast es geschafft!\n");
			system("pause");
			return 0;
		}
		if (tipp < gesucht)
			printf("Falsch, die gesuchte Zahl ist groesser.\n");
		else
			printf("Falsch, die gesuchte Zahl ist kleiner.\n");

		versuche++;
	} while (versuche <= 3);

	printf("Leider verloren. Die richtige Zahl war %i.\n", gesucht);

	system("pause");
	return 0;
}
	
