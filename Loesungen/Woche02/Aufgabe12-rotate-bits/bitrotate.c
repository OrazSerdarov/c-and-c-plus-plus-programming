/* Programmieren III
   HS Coburg
   Woche 2, Aufgabe 12
*/

#include <stdio.h>
#include <conio.h>

/* einfache Version */
unsigned char rotate_bits(unsigned char c, int n)
{
	unsigned char r = 0;
	int i, p;

	if (n >= 8)
		n %= 8;

	for(i=0, p=n; i<8; i++)
	{
		if (c & (1 << i))
			r |= (1 << p);
		p++;
		if (p == 8)
			p = 0;
	}

	return r;
}

/* elegante schnelle Version */
unsigned char fast_rotate(unsigned char c, unsigned int n)
{
	if (n >= 8) n %= 8;
	return (c << n) | (c >> (8-n));
}

void printbits(unsigned char c)
{
	int digit, i;

	for( i = 0; i <= 7; i = i+1)
    {
        digit = (c >> (7-i)) & 0x01;
        printf("%d", digit);
    }
}

int main()
{
	unsigned char t = 154, r;
	int i;

	for(i=0; i<8; i++)
	{
		printf("%i: %3i (", i, t);
		printbits(t);
		r = rotate_bits(t, i);
		printf("),\t %3i (", r);
		printbits(r);
		r = fast_rotate(t, i);
		printf("),\t %3i (", r);
		printbits(r);
		printf(")\n");
	}

	_getch();
	return 0;
}
