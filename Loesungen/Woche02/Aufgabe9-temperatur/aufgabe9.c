/* Programmieren III
   HS Coburg
   Woche 2, Aufgabe 9
*/

#include <stdio.h>

int main()
{
	float tf, tc;

	printf("Bitte geben Sie die Temperatur in \xf8 F ein: ");
	scanf_s("%f", &tf);

	tc = 5.0f/9 * (tf - 32);
	printf("Das Ergebnis ist: %.1f \xf8 C\n\n", tc);

	//fflush(stdin);
	printf("Bitte geben Sie die Temperatur in \xf8 C ein: ");
	scanf_s("%f", &tc);

	tf = 9.0f/5 * tc + 32;
	printf("Das Ergebnis ist: %.2f \xf8 F\n\n", tf);

	system("pause");
	return 0;
}