/* Programmieren III
   HS Coburg
   Woche 2, Aufgabe 11
*/

#include <stdio.h>
#include <conio.h>

int main(void)
{
	// Gegeben seien folgende Variablendeklarationen:

	int n = 3, k = 4;
	float a = 0, b = 3.2;
	int m;
	char c, e = 1;

	// Erl�utern Sie, z.B. durch das Setzen von Klammern, 
	// wie die folgenden Ausdr�cke interpretiert werden. 
	// Bestimmen Sie au�erdem die Resultatwerte der Ausdr�cke 
	// und geben Sie auftretende Seiteneffekte an.

	m = n % 2 ? -1 + n : n ;
	printf("m = %d\n", m);

	a = 1/2*b;
	printf("a = %f\n", a);

	c = 4 < 1 << n;
	printf("c = %d\n", c);

	c = 1/e/b+a;
	printf("c = %d\n", c);

	getch();
	return 0;
}

