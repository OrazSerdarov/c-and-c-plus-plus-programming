/* Programmieren III
   Hochschule Coburg
   Woche 6, Aufgabe 34
*/

#include <iostream>
#include <string>

using namespace std;
string strToUpper(const string& s1);

int main()
{
 

	string s = "abcdefg";
	string t = "hijklmn";

	string tModified = strToUpper(t);
	string sModified = strToUpper(s);


	cout << s << " -> " << sModified << endl;
	cout << t << " -> " << tModified << endl;

    cin.peek();
    return 0;
}

string& strToUpper(const string& s1)
{
	string s2;
	s2 = " ";

	for(unsigned i = 0; i < s1.length(); i++)
		s2 += toupper(s1[i]);
	
	return s2;
}
