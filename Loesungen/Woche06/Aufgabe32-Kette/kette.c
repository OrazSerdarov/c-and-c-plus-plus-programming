/* Programmieren III
   Hochschule Coburg
   Woche 6, Aufgabe 32
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct kettenglied {
	int 			    wert;
	struct kettenglied* vorg;
	struct kettenglied* nachf;
} Kglied;

Kglied* position = NULL;

void fuege_ein(int val)
{
     Kglied* neuesglied = (Kglied*)malloc(sizeof(Kglied));

     neuesglied -> wert = val;
     
     if (position == NULL)
     {
        neuesglied -> vorg = neuesglied;
        neuesglied -> nachf = neuesglied;
     }
     else
     {
        neuesglied -> nachf = position->nachf;
        neuesglied -> vorg = position;
        printf("neu: %d, position: %d\n", val, position->wert);
        neuesglied -> nachf -> vorg = neuesglied;
        position -> nachf = neuesglied;
     }  

     position = neuesglied;
}
   
int main()
{
  int n;
  struct kettenglied* p;
  
  fuege_ein( 1);
  for(p = position, n = 0; n <= 4; p = p->nachf, n++)
    printf("%i -> ", p->wert);
    
  printf("\n");
  fuege_ein( 2);
  for(p = position, n = 0; n <= 4; p = p->nachf, n++)
    printf("%i -> ", p->wert);
    
  printf("\n");
  fuege_ein( 3);
  for(p = position, n = 0; n <= 4; p = p->nachf, n++)
    printf("%i -> ", p->wert);
    
  printf("\n");
  position = position->vorg;
  fuege_ein( 4);
  for(p = position, n = 0; n <= 4; p = p->nachf, n++)
    printf("%i -> ", p->wert);
    
  printf("\n");
  position = position->nachf;
  position = position->nachf;
  for(p = position, n = 0; n <= 4; p = p->nachf, n++)
    printf("%i -> ", p->wert);
    
  printf("\n");
  system("pause");
  return 0;
 } 
