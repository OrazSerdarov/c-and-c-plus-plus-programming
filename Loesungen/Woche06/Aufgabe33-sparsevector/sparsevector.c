/* Programmieren III
   Hochschule Coburg
   Woche 6, Aufgabe 33
*/

#include <stdio.h>

struct SparseVecElem
{
	int index;
	double value;
	struct SparseVecElem* next;
};

struct SparseVecElem* first;

int setValue(int idx, double value)
{
	struct SparseVecElem* p = first;
	for(; p != NULL; p = p->next) {
		if (p->index == idx) {
			p->value = value;
			return idx;
		}
	}
	if (p == NULL) {
		struct SparseVecElem* ne = 
			(struct SparseVecElem*)malloc(sizeof(struct SparseVecElem));
		ne->index = idx;
		ne->value = value;
		ne->next = first;
		first = ne;
	}
}

double getValue(int idx)
{
	struct SparseVecElem* p = first;
	for(; p != NULL; p = p->next) {
		if (p->index == idx) {
			return p->value;
		}
	}

	return -1;
}

int main()
{
	setValue(17, 3.14);
	setValue(34, 6.28);
	setValue(1234, 0.001);

	printf("Wert[%d] = %.3lf\n", 17, getValue(17));
	printf("Wert[%d] = %.3lf\n", 34, getValue(34));
	printf("Wert[%d] = %.3lf\n", 35, getValue(35));

	system("pause");
	return 0;
}
