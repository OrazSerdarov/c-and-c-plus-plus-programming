/* Programmieren III
   Hochschule Coburg
   Woche 6, Aufgabe 31
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "liste.h"

/* Header der neuen Funktionen */
int empty(struct List *list);
int compare(struct Element *elem1, struct Element *elem2);
int removeMaximum(struct List *list);


/* Implementierung der neuen Listen-Funktionen */

/*  ------------------------------------------
    Name      : empty
    Zweck     : Test, ob die Liste leer ist
    Parameter : Zeiger auf Struktur f�r Liste
    R�ckgabe  : 1 bei leerer Liste, 0 sonst
    ------------------------------------------*/    
int empty(struct List *list)
{
	if (list == NULL)
		return -1;

    if (list->first == NULL)
        return 1;
        
    return 0;
}

/*  ------------------------------------------
    Name      : compare
    Zweck     : Vergleicht zwei Listenelement bez�glich der Tagesproduktion
    Parameter : Zeiger auf die beiden Elemente
    R�ckgabe  : -1 falls elem2 gr��er 
                0  falls beide gleich sind
                1  falls elem1 gr��er
    ------------------------------------------*/    
int compare(struct Element *elem1, struct Element *elem2)
{
	if (elem1 == NULL || elem2 == NULL)
		return 0;

    if (elem1->tagesproduktion < elem2->tagesproduktion)
        return -1;

    if (elem1->tagesproduktion > elem2->tagesproduktion)
        return 1;

    return 0;
}

/*  ------------------------------------------
    Name      : removeMaximum
    Zweck     : L�scht alle Listenelement mit dem maximalen Wert
    Parameter : Zeiger auf Struktur f�r Liste
    R�ckgabe  : Maximum 
    ------------------------------------------*/    
int removeMaximum(struct List *list)
{
    struct Element* current, *prev;
    int max;

    /* Bei leerer Liste kann nichts aufgerufen werden */
    if (list == NULL)
		return -1;

    if (list->first == NULL)
        return -1;
        
    /* Bei einem Element: alle l�schen */
    if (list->first == list->last)
    {
        max = list->first->tagesproduktion;
        clear(list);
        return max;
    }
    
    /* Durchlaufe die Liste */   
    prev = list->first;
    max = prev->tagesproduktion;
     
    for(current = list->first->next; current != NULL; current = current->next)
    {
        if (compare(prev, current) < 0) {
            max = current->tagesproduktion;
            prev = current;
        }
    }
 
    /* L�sche die Elemente mit Maximalwert */   
    removeElements(list, max);
    
    /* Gib Maximalwert zur�ck */
    return max;
}


int main(int argc, char *argv[])
{
    struct List list; 
	int max;

    init(&list); 
    insert(&list, 1, "18.11.10"); 
    insert(&list, 1, "19.11.10"); 
    insert(&list, 5, "20.11.10"); 
    insert(&list, 5, "21.11.10"); 
    insert(&list, 0, "22.11.10"); 
    print(&list); 
    max = removeMaximum(&list); 
    printf("Items with maximum value (%d) removed.\n", max);
    print(&list); 
    removeElements(&list, 0); 
    print(&list); 
    clear(&list);
    
    if (empty(&list))
        printf("List is now empty.\n");
    else
        printf("List not empty.\n"); 
  
    _getch();
    return 0;
}
