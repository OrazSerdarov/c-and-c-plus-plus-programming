/* Programmieren III
   HS Coburg
   Woche 6, Aufgabe 45
*/

#include <ctime>
#include "random.h"

using namespace std;

namespace myLib
{
static long m = 32768;
static long b = 9757;
static long c = 6925;

int rand()
{
	static int n = (int)(time(0) % m);  // der Keim
	n = (int)((n * b + c) % m);
	return n;
}

double randDouble()
{
	return (double)rand() / m;
}

}