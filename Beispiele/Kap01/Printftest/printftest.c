#include <stdio.h>

/*-----------------------------------------------------------------------------*/
int test1 (void)
{
   int a = 6;
   char c = '0';
   printf ("\nAusgaben:");
   printf ("\nDer Wert von a ist %d", a);
   printf ("\nDas Zeichen c ist eine %c", c);
   printf ("\na hat den Wert %d. Das Zeichen %c hat den Wert %d\n",
           a, c, c);
   return 0;
}

/*-----------------------------------------------------------------------------*/
int test2(void)
{
   int zahl1 = 3;
   float zahl2 = 16.5;
   int anzahl;
   printf("\n\n");
   /* Fall 1: Formatelemente korrekt */
   anzahl = printf ("%d %3.1f", zahl1, zahl2); 
   printf ("\nZahl der geschriebenen Zeichen: %d", anzahl);
   printf("\n");
   /* Fall 2: Formatelemente falsch  */
   anzahl = printf ("%d %3.1f", zahl2, zahl1);
   printf ("\nZahl der geschriebenen Zeichen: %d\n", anzahl);
   return 0;
}

/*-----------------------------------------------------------------------------*/
int test3(void)
{
   double a = 0.000006;
   double b = 123.4;
   printf ("\na:\n%e \n%E \n%f \n%g \n%G", a, a, a, a, a);
   printf ("\nb:\n%g \n%G\n", b, b);
   return 0;
}

/*-----------------------------------------------------------------------------*/
int test4(void)
{
   float zahl = 12.3456f;
   printf ("\nzahl = %f", zahl);
   printf ("\nzahl = %5.2f", zahl);
   printf ("\nzahl = %5.0f", zahl);
   printf ("\nzahl = %.3f\n", zahl);
   return 0;
}

/*-----------------------------------------------------------------------------*/
int test5(void)
{
   char * s1  = "zeichenkette";
   char s2 [] =  "string";
   printf ("\n%s", s1);
   printf ("\n%s", s2);
   printf ("\n%15s", s1);
   printf ("\n%15s",s2);
   return 0;
}

/*-----------------------------------------------------------------------------*/
int main(void)
{
   int nr = 1;
   
   while(nr != 0)
   {
      printf("\nPRINTF-Tests: Welche Nummer: ");
      scanf("%i", &nr);
      
      switch(nr) {
      case 0: return 0;
      case 1: test1(); break;
      case 2: test2(); break;
      case 3: test3(); break;
      case 4: test4(); break;
      case 5: test5(); break;
      }
      printf("\n");
   }
   
   return 0;
}
           
      
      
