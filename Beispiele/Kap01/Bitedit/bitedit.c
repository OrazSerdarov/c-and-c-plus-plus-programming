# include <stdio.h>


/*
** Biteditor 
*/

int main( void )
{
    unsigned short unumber;
    short snumber;
    int digit, bitno, i;
    char cmd;
    
    unumber = 0;

    while(1)
    {
        for( i = 0; i <= 15; i = i+1)
        {
            digit = (unumber >> (15-i)) & 0x01;
            printf("%d", digit);
        }
        printf("  ");

        for( i = 0; i <= 5; i = i+1)
        {
            digit = (unumber >> 3*(5-i)) & 0x07;
            printf("%d", digit);
        }
        printf("  ");

        for( i = 0; i <= 3; i = i+1)
        {
            digit = (unumber >> 4*(3-i)) & 0x0f;
            if( digit < 10)
                printf("%d", digit);
            else
                printf("%c", 'a' - 10 + digit);
        }
        snumber = unumber;
        printf("  %d", snumber);
        printf("  %d", unumber);
        
        printf("  Command: ");
        scanf("%c", &cmd);
        if( cmd == 'x')
            break;

        printf("Bitnumber: ");
        scanf("%d", &bitno);
		fflush(stdin);
        
        if( cmd == '+')
            unumber = unumber | (0x0001 << bitno);
        if( cmd == '-')
            unumber = unumber & ~(0x0001 << bitno);
        if( cmd == '~')
            unumber = unumber ^ (0x0001 << bitno);
    }

    return 0;
}
