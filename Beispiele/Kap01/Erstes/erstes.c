/* Dreieckszahlen */
#include <stdio.h>

const int c = 33;

int main(void)
{
  int zahl = 0;
  int i;
  for (i = 1; i < 10; i++)
  {
    zahl += i;
    printf("Dreieckszahl %d: %d\n", i, zahl);
  }
  system("pause");
}

