/* Datei: zins.c */
#include <stdio.h>
#include <conio.h>

#define LAUFZEIT     10
#define GRUNDKAPITAL 1000.00
#define ZINS         2.5

int main (void)
{
   int jahr;            /* Vereinbarung von jahr als int-Variable */
   double kapital = GRUNDKAPITAL;
   char c;
   
   printf ("Zinstabelle fuer Grundkapital %7.2f EUR\n",                     
            GRUNDKAPITAL);
   printf ("Kapitalstand zum Jahresende:\n");
   for (jahr = 1; jahr <= LAUFZEIT; jahr++) 
   {
      printf ("\nJahr: %2d\t", jahr);
      kapital = kapital * (1 + ZINS/100.);
      printf ("Kapital: %7.2f EUR", kapital);
   }
   printf ("\n\nAus %7.2f EUR Grundkapital\n", GRUNDKAPITAL);
   printf ("wurden in %d Jahren %7.2f EUR\n", LAUFZEIT, kapital);
   
   c = getch();
   return 0;
}
