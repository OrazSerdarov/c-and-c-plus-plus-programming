#include <stdio.h>

int main(void)
{
  char str[] = "This is a string.";
  printf("%d \n", NULL);
  printf("%d \n", '\0');
  printf("%s\n", str);
  str[7] = '\0';
  printf("%s\n", str);

  str[7] = ' ';
  str[17] = '.';
  printf("%s\n", str);

  system("pause");
  return 0;
}
