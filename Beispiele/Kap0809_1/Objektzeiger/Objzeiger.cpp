#include <iostream>
#include <string>

using namespace std;

class CPerson
{
  private:
    string name;
    string vorname;
  public:
    CPerson(string ValueN, string ValueVN):   name(ValueN), vorname(ValueVN) { }
        
    string &getName() {return name;}
};

class CSparer : public CPerson
{
  private:
    double kredit;
    
  public:
	  CSparer() : CPerson("Zorn","Hasso"), kredit(0.0) {}
      CSparer(string name, string vorname): 
        CPerson(name, vorname), kredit(1000.0)
      { }

    double getKredit() {return kredit;}
};

int main()
{
  CPerson *zeigerPerson;
  CSparer *zeigerSparer;
  
  
  zeigerSparer = new CSparer("Wuchtig", "Knut");
  cout << "Inhalte von *zeigerSparer:" << endl;
  cout << zeigerSparer -> getName() << endl;
  cout << zeigerSparer -> getKredit() << endl;
  
  zeigerPerson = zeigerSparer; 
  cout << "Inhalte von *zeigerPerson:" << endl;
  cout << zeigerPerson -> getName() << endl;   
  CSparer* z = (CSparer*)(zeigerPerson);
  //CSparer* z = dynamic_cast<CSparer*>(zeigerPerson);
  cout << "z -> getKredit(): ";
  cout << z -> getKredit() <<  endl;  
  //cout << zeigerPerson -> getKredit() <<  endl;  // Fehler!! nur in der Klasse CSparer vorhanden
  
  delete zeigerSparer;

  //zeigerPerson = new CSparer[10];
  //delete[] zeigerPerson;
 
  cin.get();
  return 0;
}

