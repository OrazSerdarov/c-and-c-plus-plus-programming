#include <iostream>
#include <new>

using namespace std;

class Test
{
public:
	int Z1;
	Test() {cout << "Konstruktor von Test" << endl; }
	Test(int z) : Z1(z) { 	cout << "Konstruktor von Test mit z=" << z << endl; }
	virtual ~Test() {cout << "Destruktor von Test" << endl;}
	virtual void func() { cout << "Funktion in Test" << endl;}
};
class TestKind : public Test
{
public:
	int Z2;
	TestKind() {cout << "Konstruktor von TestKind" << endl;}
	~TestKind() {cout << "Destruktor von TestKind" << endl;}
	virtual void func() override { cout << "Funktion in TestKind" << endl;}
	virtual void f2() { cout << "2. Funktion in TestKind" << endl;}
};
class TestEnkel : public TestKind
{
public:
	int Z3;

	TestEnkel() { cout << "Konstruktor von TestEnkel" << endl;}
	~TestEnkel() { cout << "Destruktor von TestEnkel" << endl;}

	void func() { cout << "Funktion in TestEnkel" << endl;}
	virtual void f2() { cout << "2. Funktion in TestEnkel" << endl;}
};

int main()
{

	Test *Zeiger;

	Zeiger = new TestKind;
	cout << "Hier ist das Hauptprogramm!" << endl;
	Zeiger->func();
	delete Zeiger;

	cout << "----------------------------" << endl;
	Zeiger = new TestEnkel;
	Zeiger->func();
	((TestKind*)(Zeiger))->f2();
	delete Zeiger;

	cout << "----------------------------" << endl;
	
	//Test* z = new TestKind[5];
	
	Test** zz = new Test*[5];
	zz[0] = nullptr;
	zz[1] = new Test(1);
	zz[2] = new TestKind();
	zz[3] = new TestEnkel();
	zz[4] = nullptr;

	for(int i = 0; i < 5; i++)
		if(zz[i])
			(zz[i])->func();
	// ...

	for(int j = 0; j < 5; j++)
		delete zz[j];
	delete[] zz;



	cin.get();
	return 0;
}

