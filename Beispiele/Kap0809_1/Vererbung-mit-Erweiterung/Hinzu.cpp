#include <iostream>
#include <string>
using namespace std;

class CPerson
{
  protected:
    string Name;
    double Konto;
  public:
    CPerson(string ValueN): Name(ValueN) {Konto = 1000;
	cout << "Konstruktor CPerson " << ValueN << endl;
	}
    void abheben(double betrag)
    {
      Konto -= betrag;
    }
    double getKonto() const {return Konto;}
};

class CSparer : public CPerson
{
  private:
    double Kredit;                         
  public:
    CSparer(string name): CPerson(name), Kredit(0.0) {
//    CSparer(string name): Name(name), Konto(1000.0), Kredit(0.0) {
		cout << "Konstruktor CSparer " << name << endl;
	}

    void abheben(double betrag)            
    {
      CPerson::abheben(betrag);
      cout << "verf�gbarer Betrag EUR: ";
      cout << getKonto() << endl;
    }
	void setKredit(double hoehe) { Kredit = hoehe; Konto += Kredit; }
    double getKredit() {return Kredit;}
};

int main()
{
  CSparer MisterZ("Wuchtig");
  cout << "Kredithoehe vor dem Zuweisen ist unbestimmt: " << MisterZ.getKredit() << endl;
  MisterZ.setKredit(4000);
  MisterZ.abheben(500);
  cout << "Kredithoehe nach dem Zuweisen: EUR " << MisterZ.getKredit() << endl;

  CPerson MisterX("Knut");
  MisterX.getKonto(); //getKredit();
  cin.get();
  return 0;
}

