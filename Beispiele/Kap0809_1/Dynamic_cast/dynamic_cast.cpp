// dynamic_cast.cpp
#include <stdio.h>
#include <iostream>

class Alpha {
public:
    virtual void test() {printf("in Alpha\n");}
};
class Beta : public Alpha {
public:
    virtual void test() { printf("in Beta\n");}
    void test2() {printf("test2 in Beta\n");}
};

class Gamma : public Beta {
public:
    virtual void test(){printf("in Gamma\n");}
    void test2() {printf("test2 in Gamma\n");}
};

void Globaltest(Alpha& a) {
    try {
        Gamma &g = dynamic_cast<Gamma&>(a);
        printf("in GlobalTest\n");
    }
    catch(std::bad_cast) {
        printf("Can't cast to Gamma\n");
    }
}

int main() {
    Alpha *pa = new Gamma;
    Alpha *pa2 = new Beta;

    pa->test();

    Beta * pb = dynamic_cast<Beta *>(pa);
    if (pb)
        pb->test2();
	else
		printf("Cannot cast to Beta*!");

    Gamma * pg = dynamic_cast<Gamma*>(pa2);
    if (pg)
        pg->test2();
	else
		printf("Cannot cast to Gamma*!\n");

    Gamma ConStack;
    Globaltest(ConStack);

   // Fehler!
    Beta BetaOnStack;
    Globaltest(BetaOnStack);
    
    system("pause");
	return 0;
}
