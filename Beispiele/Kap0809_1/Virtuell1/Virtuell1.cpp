#include <iostream>

using namespace std;

class CMulti
{
  protected:
    int z1;
    int z2;
  public:
	CMulti() = default;
	CMulti(int _z1, int _z2) : z1(_z1), z2(_z2) { }
    virtual int rechne() {return z1 * z2;}
	virtual void ausgabe() {
		cout << z1 << " * " << z2 << " = " << rechne() << endl;
	}
	virtual void eingabe() final
	{
		cout << "Bitte z1 eingeben: ";
		cin >> z1;
	}
	void eineMethode() { cout << "eineMethode in CMulti" << endl; };
};
class CAdd : public CMulti
{
private:
	int summe;
public:
    CAdd(int _z1, int _z2) : CMulti(_z1, _z2) {  summe = _z1 + _z2; }
    int rechne() {return z1 + z2;}  // Neue Funktionalitšt
	void ausgabe() {
		cout << z1 << " + " << z2 << " = " << summe << endl;
	}
	void eineMethode() { cout << "eineMethode in CAdd" << endl; };
	//void eingabe() {
	//cout << "Geht hier nicht.";
	//}
};
class CDiv : public CMulti
{
public:
//	using CMulti::CMulti;
	CDiv(int _z1, int _z2) : CMulti(_z1, _z2) {  }
	int rechne() { return (z2 ? z1 / static_cast<double>(z2) : 0); }
};

int main()
{
  CMulti *zeiger1, *zeiger2;
  CAdd einObjekt(7, 9);
  CAdd zweitesObjekt(4, 5);

  zeiger1 = &einObjekt;
  zeiger2 = &zweitesObjekt;
  cout << "Die Rechenoperation mit den im Objekt gespeicherten Zahlen ergibt: " << zeiger1->rechne() << endl;		//16

  
  zeiger1->ausgabe();			//7+9=16
  *zeiger1 = *zeiger2;
  zeiger1->ausgabe();

  CDiv drittesObjekt(12, 5);
  drittesObjekt.ausgabe();			//12+5=17
  
  cin.get();
  return 0;
}

