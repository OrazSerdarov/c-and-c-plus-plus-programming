#include <iostream>
#include <cstring>

using namespace std;

class Zeichnung
{
  public:
    int X, Y;
    char Name[40];
  
	Zeichnung()
	{ cout << "Hier Konstruktor von Zeichnung" << endl; }

	Zeichnung(char* neuerName)
	{ 
		cout << "Konstruktor von Zeichnung: " << neuerName << endl; 
		strncpy(Name, neuerName, 40); 
	}

	const char* getName() const { return Name; }
};
class Linie : virtual public Zeichnung
{
  public:
    int Breite;
    int Farbe;

	Linie(char* neuerName) 
		: Zeichnung(neuerName) 
	{}
};

class Flaeche : virtual public Zeichnung
{
  public:
    int Farbe;

	Flaeche(char* neuerName) : Zeichnung(neuerName) {}
};

class Rechteck : public Linie, public Flaeche
{
  public:
	  Rechteck(char* neuerName) : 
		Zeichnung(neuerName), 
		Linie(neuerName), Flaeche(neuerName) {}
};

int main()
{

  Rechteck myRechteck("myRechteck");
  cout << "Name: " << myRechteck.getName() << endl;
  myRechteck.Linie::Farbe = 17;
  myRechteck.Flaeche::Farbe = 28;
  myRechteck.Breite = 200;
  myRechteck.Linie::X = 100;
  myRechteck.Flaeche::X = 200;
  cout << "Farbe der Linie: " << myRechteck.Linie::Farbe << endl;
  cout << "Farbe der Flaeche: " << myRechteck.Flaeche::Farbe << endl;
  cout << "Die Breite betraegt: " << myRechteck.Breite << endl; 

  cout << "Die gemeinsame Eigenschaft der Klasse ist nur einmal vorhanden!" << endl;
  cout << "Die X-Koordinate der Linie des Rechtecks betraegt: " << myRechteck.Linie::X << endl;
  cout << "Die X-Koordinate der Fl�che des Rechtecks betraegt: " << myRechteck.Flaeche::X << endl;
  cin.get();
  return 0;
}
