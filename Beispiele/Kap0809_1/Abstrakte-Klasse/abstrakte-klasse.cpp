#include <iostream>

using namespace std;

class Zahl
{
  public:

    virtual void ausgeben() const = 0;
	virtual ~Zahl() = 0 ;
};

Zahl::~Zahl() {} 

class Integer : public Zahl
{
  private:
    int value;
  public:
    Integer(int i) { value = i; };
   void ausgeben() const { cout << "Die int-Zahl ist " << value << endl; };
};

class GleitKomma : public Zahl
{
  private:
    double value;
  public:
    GleitKomma (double d) { value = d; };
    void ausgeben() const { cout << "Die Gleitkommazahl ist " << value << endl; };
};

void ausgabe(const Zahl& z)
{
	cout << "Ausgabe: ";
	z.ausgeben();
}

int main()
{
  GleitKomma gl(12.12);
  Integer in(173);
  ausgabe(gl);
  //gl.ausgeben();
  ausgabe(in);
  //in.ausgeben();
  cin.peek();
  return 0;
}

