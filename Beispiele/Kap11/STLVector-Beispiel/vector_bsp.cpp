
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

struct Konto {
    unsigned long nummer;
    string inhaber;

    Konto() : nummer(0L) {}
};

//---------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    vector<float> v1(10);
    vector<Konto> Kundenkonten;

    const unsigned n = v1.size();
    Kundenkonten.resize(n);

    for (unsigned i=0; i<n; i++)
        v1[i] = i*0.1;

    ifstream in_file("konten.txt");
    for (unsigned i=0; i<Kundenkonten.size(); i++)
        in_file >> Kundenkonten[i].nummer >> Kundenkonten[i].inhaber;

    for (auto k : Kundenkonten)
        cout << "Inhaber von Konto " << k.nummer << " ist "
             << k.inhaber << endl;

    for(unsigned i=0; i<v1.size(); i++)
        cout << v1[i] << " ";
        
    cin.get();
    return 0;
}
 
