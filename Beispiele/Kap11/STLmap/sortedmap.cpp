/* cppbuch/k28/sortedmap.cpp
   Beispiel zum Buch von Ulrich Breymann: Der C++ Programmierer; Hanser Verlag
   Diese Software ist freie Software. Website zum Buch: http://www.cppbuch.de/ 
*/
#include<map>
#include<string>
#include<iostream>
using namespace std;

// Zwei  typedefs zur Abk�rzung
using MapType = map<string, long>; // Vergleichsobjekt: less<string>()
using ValuePair = MapType::value_type;

int main() {
	MapType aMap;
	aMap.insert(ValuePair("Thomas", 5192835));
	aMap.insert(ValuePair("Werner", 24439404));
	aMap.insert(ValuePair("Manfred", 535353));
	aMap.insert(ValuePair("Heiko", 635352723));
	aMap.insert(ValuePair("Andreas", 42536347));
	aMap.insert(ValuePair("Karin", 42536347));
	// 2. Einf�gen von Heiko mit einer anderen (Tel.Nummer wird 
	// NICHT ausgef�hrt, weil der Schl�ssel schon existiert.
	aMap.insert(ValuePair("Heiko", 1000000));

	/* Wegen der auf einer Baumstruktur basierenden Implementierung
	  ist die Ausgabe nach Namen sortiert.
	*/
	cout << "Ausgabe:\n";
	//MapType::const_iterator iter = aMap.begin();
	for (auto paar : aMap) {
		cout << paar.first << ':'     // Name
			<< get<1>(paar) << endl;   // Nummer
	}

	cout << "Ausgabe der Nummer nach Eingabe des Namens\n"
		<< "Name: ";
	string derName;
	cin >> derName;
	cout << "Suche mit Iterator: ";
	auto iter = aMap.find(derName);         // O(log N)
	if (iter != aMap.end()) {
		cout << (*iter).second << endl;
	}
	else {
		cout << "Nicht gefunden!" << endl;
	}


	cout << "Suche mit operator[]() (Element muss existieren,\n"
		"andernfalls wird eine undef. Nummer (0) f�r den Namen angelegt): ";
	cout << aMap[derName] << endl;        // O(log N)                

	aMap["Ahmed"] = 3456128;

    cout << aMap["Ahmed"]  << endl;        // O(log N)                

     // im neuen Standard C++11:
    try {
       cout << "Suche mit at(): " << aMap.at(derName)  << endl;  // O(log N)
    } catch(const exception& e) {
       cout << "Nicht gefunden! Exception: " << e.what() << endl;
    }

	system("pause");
	return 0;
}
