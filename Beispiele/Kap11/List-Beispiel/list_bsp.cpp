
#include <list>
#include <stack>
#include <string>
#include <iostream>
#include <fstream>
#include <new>
#include <algorithm>

using namespace std;

struct Konto {
    unsigned long nummer;
    string inhaber;

    Konto() : nummer(0L) {}
		
};

ostream& operator<<(ostream& o, const Konto& k)
{
	o << k.nummer << "\t " << k.inhaber;
	return o;
}
/*
bool operator<(const Konto& k1, const Konto& k2) 
{ 
	return (k1.nummer < k2.nummer); 
}
*/

//---------------------------------------------------------------------------
void liesKonten(list<Konto>& kontoListe,
  list<Konto>& inverseListe, const string& dateiName)
{
    ifstream in_file(dateiName.c_str());
    while (in_file)
    {
      Konto einKonto;
      in_file >> einKonto.nummer >> einKonto.inhaber;
      inverseListe.push_front(einKonto);   // am Anfang einfügen
      kontoListe.push_back(einKonto);    // am Ende einfügen
    }

    //list<Konto>::iterator  iter;

    for(auto iter=kontoListe.begin(); iter != kontoListe.end(); iter++)
      cout << iter->nummer << "\t "      // erste Zugriffsmöglichkeit
           << (*iter).inhaber << endl;   // zweite Zugriffsmöglichkeit

	for (auto k : inverseListe)
		cout << k.nummer << "\t " << k.inhaber << endl;

}

//---------------------------------------------------------------------------
void testStack(const string& dateiName)
{
    stack<string>  myStack;

    ifstream in_file(dateiName.c_str());
    while (in_file)
    {
      unsigned long nummer;
      string inhaber;
      in_file >> nummer >> inhaber;

      myStack.push(inhaber);
      cout << myStack.top() << endl;
    }

    cout << "---------------" << endl;

    while (!myStack.empty())
    {
      cout << myStack.top() << endl;
      myStack.pop();
    }
}

//---------------------------------------------------------------------------
int main(void)
{
  list<Konto> kontoListe;
  list<Konto> inverseListe;

  liesKonten(kontoListe, inverseListe,"konten.txt");
  testStack("konten.txt");

  //kontoListe.sort();
  for (auto k : kontoListe)
	  cout << k << endl;

  //alternativ
  copy(kontoListe.begin(), kontoListe.end(), ostream_iterator<string>(cout, "\n"));

  system("pause");
  return 0;
}
