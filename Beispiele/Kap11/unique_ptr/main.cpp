/* cppbuch/k33/uniqueptr/main.cpp
   Beispiel zum Buch von Ulrich Breymann: Der C++ Programmierer; Hanser Verlag
   Diese Software ist freie Software. Website zum Buch: http://www.cppbuch.de/ 
*/
#include<iostream>
#include<memory>
using namespace std;

class Ressource {
public:
   Ressource(int i) 
      : id(i){
   cout << "Konstruktor Ressource()" << endl;
   }
   void hi() {
      cout << "hier ist Ressource::hi(), Id=" << id << endl;
   }

   ~Ressource() {
      cout << "Ressource::Destruktor, Id=" << id << endl;
   }
private:
   int id;
};

int main() {
   cout << "Zeiger auf dynamisches Objekt:" << endl;
   unique_ptr<Ressource> p1(new Ressource(1));
   cout << "Operator ->  ";
   p1->hi();
   cout << "Operator *   ";
   (*p1).hi();

   //  Null-Zeiger
   unique_ptr<Ressource> nullp((Ressource*)0);
   //nullp->hi();   // ok, Speicherzugriffsfehler!
   //cout << "release()" << endl;
   //auto p = p1.release();  // verhindert Destruktor-Aufruf
   p1.reset(nullptr);  //  Destruktor-Aufruf ok
   cout << "Ende von main()" << endl;
   
   cin.peek();
   return 0;
}
