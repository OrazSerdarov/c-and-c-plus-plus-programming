
#include <list>
#include <stack>
#include <string>
#include <vector>
#include <algorithm>    // f�r find, replace, sort etc.
#include <functional>   // f�r greater
#include <iterator>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>

//---------------------------------------------------------------------------
using namespace std;

struct Konto {
	unsigned long nummer;
	string inhaber;

	Konto() : nummer(0L) {}  // string hat selbst einen Default-Konstruktor
};

//---------------------------------------------------------------------------
void liesKonten(list<Konto>& kontoListe,
				list<Konto>& inverseListe, const string& dateiName)
{
	ifstream in_file(dateiName.c_str());
	if (in_file.bad()) return;

	while (in_file)
	{
		Konto einKonto;
		in_file >> einKonto.nummer >> einKonto.inhaber;
		inverseListe.push_front(einKonto);   // am Anfang einf�gen
		kontoListe.push_back(einKonto);    // am Ende einf�gen
	}

	list<Konto>::iterator  iter;

	for(iter=kontoListe.begin(); iter != kontoListe.end(); iter++)
		cout << iter->nummer << "\t "      // erste Zugriffsm�glichkeit
		<< (*iter).inhaber << endl;   // zweite Zugriffsm�glichkeit
}

//---------------------------------------------------------------------------

void findeInListe()
{
	list<unsigned>  l;
	unsigned n;

	for(n=1; n<=5; n++)
		for(unsigned m=1; m<=n; m++)
			l.push_back(m);

	for(auto i : l )
		cout << i << " ";
	cout << endl;

	// Beispiel f�r Suchen und Ersetzen
	/*list<unsigned>::iterator*/ auto iter = find(l.begin(), l.end(), 3);

	while (iter != l.end())
	{
		*iter = 99;
		iter++;
		iter = find(iter, l.end(), 3);  // n�chstes Element suchen
	}

	for(auto i : l)
		cout << i << " ";
	cout << endl;

	replace(l.begin(), l.end(), 99, 3);

	// Doch noch eine Liste mit '99' erzeugen
	list<unsigned>  l2;

	copy(l.begin(), l.end(), inserter(l2, l2.begin()));
	replace(l2.begin(), l2.end(), 3, 99);

	for(auto i = l2.begin(); i != l2.end(); i++)
		cout << *i << " ";
	cout << endl;

}

//---------------------------------------------------------------------------
void sortBeispiel()
{
	vector<unsigned>            v(12);

	for(unsigned i=0; i<4; i++)
		for(unsigned j=0; j<3; j++)
			v[i*3+j] = i+j;

	// Beispiel f�r Sortieren
	cout << "Unsortierter Vektor: ";
	for(auto i : v)
		cout << i << " ";
	cout << endl;

	sort(v.begin(), v.end());

	cout << "Sortierter Vektor: ";
	for (auto i : v)
		cout << i << " ";
	cout << endl;
}
//---------------------------------------------------------------------------
// Pr�dikatsfunktion
template <int suchnr>   // �bergabe des Suchparameters als Template
bool hatZiffern(const Konto& einKonto)
{
	ostringstream ostr, teilstr;
	ostr << einKonto.nummer << ends;
	teilstr << suchnr << ends;
	string nrstr(ostr.str());
	if (nrstr.find(teilstr.str()) != string::npos)
		return true;

	return false;
}

//---------------------------------------------------------------------------
void sucheNach28(const string& dateiName)
{
	list<Konto>  kontoListe;

	// Konten einlesen
	ifstream in_file(dateiName.c_str());
	if (in_file.bad()) return;

	while (in_file)
	{
		Konto einKonto;
		in_file >> einKonto.nummer >> einKonto.inhaber;
		kontoListe.push_back(einKonto);    // am Ende einf�gen
	}

	// Beispiel f�r bedingtes Suchen
	const string teilstr{ "28" };

	cout << endl << "Suche nach Kontonummern mit '28':" << endl;
	//auto iter1 = find_if(kontoListe.begin(), kontoListe.end(), hatZiffern<28>);

	auto iter2 = find_if(kontoListe.begin(), kontoListe.end(), 
		[=] (const Konto& k) -> bool {
		ostringstream ostr;
		ostr << k.nummer;
		string nrstr(ostr.str());
		if (nrstr.find(teilstr) != string::npos)
			return true;
		return false;
	});

	while (iter2 != kontoListe.end())
	{
		cout << iter2->nummer << "\t " << iter2->inhaber << endl;
		iter2++;

		// n�chstes Element suchen
		iter2 = find_if(iter2, kontoListe.end(), hatZiffern<28>);
	}
}

//---------------------------------------------------------------------------
int neue_algorithmen(void)
{
	int source[5]={0, 12, 34, -50, 80}; 
	int target[5]; // kopiere  5 elemente von der Quelle zum Ziel

	copy_n(source,5,target);

	if (all_of(source, source+5, [] (int v) -> bool {
		if (v < 0) return false;
		return true;
	}))
		cout << "Alle Werte positiv" << endl;
	else
		cout << "Nicht alle Werte positiv" << endl;

	return 0;
}

//---------------------------------------------------------------------------
int main(void)
{
	list<Konto> kontoListe;
	list<Konto> inverseListe;

	liesKonten(kontoListe, inverseListe,"konten.txt");

	//findeInListe();

	sortBeispiel();
	cout << "-------------------------------------" << endl;
	//sucheNach28("konten.txt");


	cout << "-------------------------------------" << endl;
	//neue_algorithmen();

	cin.peek();
	return 0;
}

