
/* Aufgabe aus der Klausur WS 2004/2005:

Welche Ausgabe hat folgendes Programm, wenn man es mit dem 
Kommandozeilenargument 7643123 aufruft? Erl�utern Sie, wie 
es zu dieser Ausgabe kommt.

*/

#include <stdio.h>
#include <conio.h>
	
int main(int argc, char *argv[])
{
  char *z;
  char code[] = "SKIANZUG";

  printf("%s\n", argv[0]);
  z = *++argv;
  z += 4;

  while (*z - '7')
    printf("%c", code[*z-- - '0']);
	
  printf("\n");
  system("pause");
  return 0;
}
