#include <stdio.h>
#include <string.h>
#include <conio.h>

void bsp1()
{
   char string1 [20] = "**********";
   char string2 [] = "####";
   memcpy (string1+3, string2, strlen (string2));
   printf ("Ergebnis von memcpy: %s\n\n", string1);
}

void bsp2()
{
   char string [] = "12345678";
   memmove (string + 2, string, strlen (string) - 2);
   printf ("Ergebnis von memmove: %s\n\n", string);
}

void bsp3()
{
   char string3 [] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06};
   char string4 [] = {0x01, 0x02, 0x03, 0x14, 0x05, 0x06};
   printf ("Vergleich String3 mit String4 ergibt: %d \n\n",
            memcmp (string3, string4, sizeof (string3)));
}

void bsp4()
{
   char str1 [] = "Zeile1: Text";
   char * str2;
   if((str2 = memchr (str1, ':', strlen (str1))) != NULL)
   {  
      /* memchr() liefert einen Pointer str2 auf das Zeichen ':'  */ 
      /* str2 + 2 zeigt um 2 Zeichen nach dem ':' weiter, also    */
      /* auf das 'T'                                              */
      printf ("Ergebnis von memchr%s\n\n", str2 ); 
   }                
}

void bsp5()
{
   char setstring [20] = "Hallo";
   printf ("Ergebnis von memset: %s\n", 
	   memset (setstring, '*', 5));   
}
int main()
{
	//bsp1();
	//bsp2();
	//bsp3();
	//bsp4();
	bsp5();

	getch();
	return 0;
}
   
   
 
