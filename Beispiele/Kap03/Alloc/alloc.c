#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(void)
{
   /* -------------------------------------------------
     malloc-Beispiel  
   */
   int * pointer = NULL;
   int * tmppointer;

   if ((pointer = malloc (sizeof (int))) != NULL)
   {
      *pointer = 3;
      printf ("pointer zeigt auf eine int-Zahl mit Wert %d\n",
               *pointer);
      free (pointer);

      printf ("Nach dem free: pointer zeigt auf eine int-Zahl mit Wert %d\n",
               *pointer);
   }
   else
   {
      printf ("Nicht genuegend Speicher verfuegbar\n");
      return (-1);
   }
   
   /* -------------------------------------------------
     calloc-Beispiel  
   */
   if ((pointer = calloc (1, sizeof (int))) != NULL)
   {
      *pointer = 4;
      printf ("pointer zeigt auf eine int-Zahl mit Wert %d\n",
               *pointer);
      free (pointer);
   }
   else
   {
      printf ("Nicht genuegend Speicher verfuegbar\n");
      return (-1);
   }
   
   /* -------------------------------------------------
     realloc-Beispiel  
   */
   if ((pointer = malloc (sizeof (int))) != NULL)
   {
      *pointer = 5;
      printf ("Nach malloc():\n");
      printf ("pointer zeigt auf eine int-Zahl mit Wert %d\n\n",
               *pointer);
   }
   else
   {
      printf ("Nicht genuegend Speicher verfuegbar\n");
      return (-1);
   }
   
   tmppointer = realloc (pointer, 2 * (sizeof(int)));
   if (tmppointer == NULL)
   {
      printf ("Nicht genuegend Speicher verfuegbar\n");
	  free(pointer);
      return (-1);
   }  
   pointer = tmppointer;

   pointer[1] = 6;
   printf ("Nach realloc():\n");
   printf ("pointer zeigt auf eine int-Zahl mit Wert %d\n",
            *pointer);
   printf ("Das 2. Element von pointer hat den Wert %d\n\n", 
            pointer[1]);
   free (pointer);
   
   getch();
   return 0;
}   

