/* Datei: ptr_add.c */
#include <stdio.h>
#include <conio.h>

int main (void)
{
   int alpha [5] = {1,2,3,4,5};
   int * pointer;
   int lv = 0;

   pointer = alpha;
   while (lv < 5)
   {
      printf ("%d\n", *pointer);
      pointer++;
      lv++;
   }

   for(pointer = alpha, lv = 0; lv < 5; pointer++, lv++)
      printf ("%3d", *pointer);

   printf("\n");
   getch(); 
   return 0;
}
