/* Datei: stat1.c */
#include <stdio.h>
#include <conio.h>

void f1 (void);
extern void f2 (void);

static int zahl = 6;

int getZahl()
{
	return zahl;
}

int main (void)
{
   printf ("\nhier ist main, zahl = %d", zahl);
   f1();
   f2();
   
   getch();
   return 0;
}

void f1 (void)
{
   printf ("\nhier ist f1, zahl = %d", zahl);
}
