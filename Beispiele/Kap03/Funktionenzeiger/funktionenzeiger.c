#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <conio.h>

#define PI (float)(4.0f*atan(1))

float berechne( float rad, float (*welche_funktion)(float rad) )
{
    return(welche_funktion(rad));
}

/* Alternative Deklaration mit typedef */
typedef float (*Berechnungsfunktion) (float);

typedef unsigned int UINT;

float berechne2( float rad, Berechnungsfunktion welche_funktion)
{
    return((*welche_funktion)(rad));
}

/* auszuwählende Funktionen */
float zyl_vol(float r) { return(r*r*PI); }

float kug_vol(float r) { return(r*r*r*4.0f/3.0f*PI); }

/* main-Block */
int main(void)
{
    float radius, hoehe;
    char wahl;
    printf("\nVolumen eines Zylinders oder einer Kugel ( Z/K ) ? ");
    wahl = toupper(getchar());
    
    printf("\nGeben Sie Radius ein: "); scanf("%f", &radius);
    if (wahl=='Z') {
        printf("Geben Sie Hoehe des Zylinders ein: "); scanf("%f", &hoehe);
        printf(" ---> Volumen (Zylinder): %f\n", hoehe*berechne(radius,zyl_vol));
    } 
    else
        printf(" ---> Volumen (Kugel): %f\n", berechne(radius,kug_vol));
        
    getch();
	return 0;
}
