/* Datei: static.c */


int f() {
	static int z = 0;
	z++;
	return z;
}

int main() {
	int i;
	for(i = 0; i < 10; i++)
		printf("%i ", f());

	getch();
	return 0;
}

