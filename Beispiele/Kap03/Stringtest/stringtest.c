/* Datei: stringtest.c */
/* Programm zum Test von Funktionen zur Stringbearbeitung         */
#include <stdio.h>
#include <string.h>
#include <conio.h>

void bsp1()
{
   /* strcpy  */
   char string1 [25];
   char string2 [] = "Zu kopierender String";
   strcpy_s(string1, sizeof(string1), string2);
   printf ("Der kopierte String ist: %s\n", string1);
}

void bsp2()
{
   /* strcat  */
   char catstring [50] = "concatenate";
   printf ("%s\n", catstring);
   strcat_s(catstring, sizeof(catstring), " = zusammenfuegen");
   printf ("%s\n", catstring);
}

void bsp3()
{
   /* strcmp  */
   printf ("%d\n", strcmp ("abcde", "abCde"));
   printf ("%d\n", strcmp ("abcde", "abcde"));
   printf ("%d\n", strcmp ("abcd", "abcde"));

   /* strncmp */   
   printf ("%d\n", strncmp ("abcdef", "abcdE", 4));
   printf ("%d\n", strncmp ("abCd", "abcd", 4));   
}

void bsp4()
{
   /* strlen  */
   char langerstring [100] = "So lang ist dieser String:";
   printf ("So gross ist das char-Array: %d\n", sizeof (langerstring));
   printf ("%s %d\n", langerstring, strlen (langerstring));
}

int main (void)
{
	//bsp1();
	
	//bsp2();
	
	//bsp3();
	bsp4();
 
	getch();
	return 0;
}      
