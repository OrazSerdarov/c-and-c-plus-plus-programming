/* Datei ext1.c */
#include <stdio.h>
#include <conio.h>

extern void f2 (void);
void f1 (void);
extern int zahl;  /* reine Deklaration */

int main (void)
{
   printf ("\nhier ist main, zahl = %d", zahl);
   f1 ();
   f2 ();
   
   getch();
   return 0;
}

int zahl = 6;  /* Definition */

void f1 (void)
{
   printf ("\nhier ist f1, zahl = %d", zahl);
}
