#include  <stdio.h>

struct  Datum  {
    int   tag;
    int   monat;
    int   jahr;
};
struct  Person  {
    char    name[20];
    char    vorname[20];
    struct  Datum  geb;
    struct  Datum  anst;
};

int main(void)
{
/* Version f�r C99: 
   struct Person kollege1 = {
         .name = "Meier", .vorname = "Fritz", .geb = { 13, 7, 1965 },
         .anst.tag = 25, .anst.monat = 4, .anst.jahr = 1987
   };
*/
   struct Person kollege1 = {
	   "Meier", "Fritz", { 13, 7, 1965 }, {25, 4, 1987 } 
   };

   struct Person kollege2 = {
         "Aller",         /* kollege2.name                      */
         "Franziska",     /* kollege2.vorname                   */
         { 20, 4, 1959 }, /* kollege2.geb.tag ,...monat,...jahr */
         { 30, 9, 1991 }  /* kollege2.anst.tag,...monat,...jahr */
   };

   printf("kollege1:\n"    "  %s %s, GebDatum: %d.%d.%d, Eingestellt: %d.%d.%d\n",
          kollege1.vorname, kollege1.name,
          kollege1.geb.tag, kollege1.geb.monat, kollege1.geb.jahr,
          kollege1.anst.tag, kollege1.anst.monat, kollege1.anst.jahr );
   printf("kollege2:\n"
          "  %s %s, GebDatum: %d.%d.%d, Eingestellt: %d.%d.%d\n",
          kollege2.vorname, kollege2.name,
          kollege2.geb.tag, kollege2.geb.monat, kollege2.geb.jahr,
          kollege2.anst.tag, kollege2.anst.monat, kollege2.anst.jahr );

   system("pause");
   return 0;
}
