/* Datei: liste.c */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Deklarationen und Prototypen --> geh�rt in die Header-Datei */
struct messdaten { struct messdaten * next;
                   char   sensorname[10];
                   float  messwert;
                 };

int element_hinzufuegen (char *, float);
struct messdaten * element_suchen (char *);
int element_loeschen (char *);
void elemente_ausgeben (void);

struct messdaten * start_pointer = NULL;

/* Funktionsdefinitionen */
int main (void)
{
   int lv;
   int anzahl;
   char name [10];
   float mw;
   struct messdaten * ptr;
   struct messdaten * ptr_2;

   printf ("\nWieviele Elemente wollen Sie eingeben ? ");
   scanf ("%d", &anzahl);

   for (lv = 1; lv <= anzahl; lv++)
   {
      printf ("\nGeben Sie den Namen des Sensors %d ein: ", lv);
      scanf ("%s", name);
      printf ("Geben Sie den Messwert ein: ");
      scanf ("%f", &mw);

      if(element_hinzufuegen (name, mw) != 0)
      {
         printf ("\nFehler beim Hinzufuegen eines Elements");
         return 1;
      }
   }

   /* Ausgabe der kompletten Liste */
   elemente_ausgeben ();

   printf ("\n\nWelchen Eintrag wollen Sie suchen ? ");
   scanf ("%s", name);

   if ((ptr = element_suchen (name)) == NULL)
      printf ("\nElement mit Namen %s nicht vorhanden", name);
   else
      printf ("\nDer Sensor %s hat den Messwert %f",
              ptr->sensorname, ptr->messwert);

   printf ("\n\nWelchen Eintrag wollen Sie loeschen ? ");
   scanf ("%s", name);

   if (element_loeschen (name) != 0)
   {
      printf ("\nFehler beim Loeschen eines Listenelements");
      return 1;
   }

   /* Ausgabe der kompletten Liste */
   elemente_ausgeben ();

   /* Restliche Elemente loeschen */
   ptr = start_pointer;
   while (ptr != NULL)
   {
      ptr_2 = ptr->next;
      free (ptr);
      ptr = ptr_2;
   }
   
   getch();
   return 0;
}

/* Neues Element an den Anfang der Liste einfuegen */
int element_hinzufuegen (char * name, float messwert)
{
   struct messdaten * ptr;

   if ((ptr = (struct messdaten *)malloc (
                         sizeof (struct messdaten))) == NULL)
      return 1;
   else
   {
      strncpy (ptr->sensorname, name, 10);
      ptr->messwert = messwert;
      ptr->next = start_pointer;
      start_pointer = ptr;
      return 0;
   }
}

/* Element in der Liste suchen */
struct messdaten * element_suchen (char * name)
{
   struct messdaten * ptr = start_pointer;

   while (ptr != NULL && strcmp (ptr->sensorname, name))
      ptr = ptr->next;

   /* der Pointer auf das gesuchte Element wird zurueckgegeben */
   return ptr;
}

/* Element aus der Liste loeschen */
int element_loeschen (char * name)
{
   struct messdaten * ptr = start_pointer, * vorgaenger;

   while (ptr != NULL && strcmp (ptr->sensorname, name))
   {
      vorgaenger = ptr;
      ptr = ptr->next;
   }

   if (ptr == NULL)
      return 1;
   else
   {
      if (ptr == start_pointer)
         start_pointer = ptr->next;
      else
         vorgaenger->next = ptr->next;

   free (ptr);
   return 0;
  }
}

/* Ausgabe der kompletten Liste */
void elemente_ausgeben (void)
{
   struct messdaten * ptr;

   for (ptr = start_pointer; ptr != NULL; ptr = ptr->next)
   {
      printf ("\nDer Sensor %s hat den Messwert %f",
              ptr->sensorname, ptr->messwert);
   }
}
