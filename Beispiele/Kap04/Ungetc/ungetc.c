/* Datei: ungetc.c */
/* Summieren aller ganzen Zahlen in Datei mit weiterem Text.      */
#include <stdio.h>
#include <ctype.h>
#include <conio.h> 

int main (void)
{
   const char * const filename = "bspungetc.txt";
   //const char * const filename = "bspungetc.bsp";
   FILE * fp;
   int lc, zahl, summe=0;
   if ((fp = fopen (filename, "r")) == NULL)
   {
      fprintf (stderr,
         "Fehler beim Oeffnen der Datei %s\n", filename);
	  getch();
      return 1;
   }
   
   while ((lc = fgetc(fp)) != EOF)
   {
      if (isdigit (lc)) /* Zeichen ist Ziffer -> zu viel gelesen  */
      {  /* Zurueck damit in den Dateipuffer...                   */
         ungetc (lc, fp);
         /* Und jetzt formatiert eine ganze Zahl lesen:           */
         fscanf (fp, "%d", &zahl);
         summe += zahl;
      }
   }
   printf ("Die Summe aller ganzen Zahlen in %s ist %d.\n",
      filename, summe);
   fclose (fp);
   
   getch();
   return 0;
}
