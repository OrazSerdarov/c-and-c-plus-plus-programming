/* Datei: fwrite.c */
/* Schreiben von Strukturen in eine Binärdatei */
#include <stdio.h>
#include <conio.h> 

int main (void)
{
   int status = 0;
   int index;
   float puffer [4] = {1.0f, 1.1f, 1.2f, 1.3f};
   const char * const filename = "bsp.bin";
   FILE * fp;
   
   if ((fp = fopen (filename, "wb")) == NULL)
   {
      fprintf (stderr,
         "Fehler beim Oeffnen der Datei %s\n", filename);
      return 1;
   }
   
   /* Die Groesse eines Objektes wird aus Portabilitaetsgruenden  */
   /* stets mit Hilfe von sizeof angegeben.                       */
   status = fwrite (puffer, sizeof (puffer), 1, fp);
   if (status < 1)
   {
      /* Fehlerbehandlung   */
      fprintf (stderr,
         "Fehler beim binaeren Schreiben auf Datei %s\n", filename);
      return 2;
   }
   
   printf ("%d Array-Objekt wurde geschrieben\n", status);
   fclose(fp);
     
   /* --------------------------------------------------
      Wiedereinlesen
   */
   if ((fp = fopen ("bsp.bin", "rb")) == NULL)
   {
      perror("Fehler beim Oeffnen der Datei:");
      system("pause");
      return 1;
   }
   
   /* Die Groesse eines Objektes wird aus Portabilitaetsgruenden  */
   /* stets mit Hilfe von sizeof angegeben.                       */
   if (fread (puffer, sizeof(puffer), 1, fp) < 1)
   {
      /* Fehlerbehandlung   */
      fprintf (stderr,
         "Fehler beim binaeren Lesen von Datei %s\n", filename);
      return 2;
   }

   /* Array ausgeben:                                             */
   printf ("Inhalt der Datei %s:\n", filename);
   
   for (index = 0; index < sizeof (puffer) / sizeof (puffer[0]); 
        index++)
      printf ("%4.1f\n", puffer[index]);
      
   getch();
   return 0;
}
