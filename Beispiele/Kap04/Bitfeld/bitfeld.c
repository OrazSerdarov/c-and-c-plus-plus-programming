/* Datei: bitfeld.c */
#include <stdio.h>
#include <math.h>

#define HOEHE  17
#define BREITE 53
int main (void)
{
   /* Struktur f�r einen Plot-Punkt:                          (1) */
   struct plotPunkt{ 
            unsigned char fnktNr:2;  /* Nummer der Funktion, zu   */
                                     /* der der Punkt geh�rt      */
            unsigned char xAchse:1;  /* Punkt gehoert zur X-Achse */
            unsigned char yAchse:1;  /* Punkt gehoert zur Y-Achse */
            unsigned char mehrfachGesetzt:1;   
            /* Falls mehr als eine Funktion auf den Punkt trifft. */
   } ausgabeArray[HOEHE][BREITE];                          /* (2) */
                            /*D.h. 17 Zeilen x 53 Spalten         */

   const double PI = 4. * atan (1.);                       /* (3) */
   int i;                                          /* Laufendes x */ 
   int j;                                          /* Laufendes y */ 

   /* Achsen in ausgabeArray eintragen:                       (4) */
   for (i = 0; i < BREITE; i++)
      ausgabeArray[HOEHE/2][i].xAchse = 1;
   for (j = 0; j < HOEHE; j++)
      ausgabeArray[j][0].yAchse = 1;

   /* ausgabeArray mit Funktionsnummer je Funktion fuellen:   (5) */
   for (i = 0; i < BREITE; i++)
   {
      int f;
      double y [3]; /* Fuer die 3 Funktionswerte der 3 Funktionen */
      double x = (double) i / (BREITE-1) * 2. * PI;
      y[0] = sin (x);
      y[1] = cos (x);
      y[2] = y[0] * y[0];  /* == sin(x) * sin(x)                  */
      for (f = 0; f < sizeof (y) / sizeof (y[0]); f++)
      {  
         j = (int) ((1. - y[f]) / 2. * (HOEHE-1) + 0.5);
         /* Schon eine Funktion in diesem Punkt?              (6) */
         if (ausgabeArray[j][i].fnktNr > 0)	
            ausgabeArray[j][i].mehrfachGesetzt = 1;
         else
            ausgabeArray[j][i].fnktNr = f+1;
      }
   }

   /* ausgabeArray drucken:                                   (7) */
   for (j = 0; j < HOEHE; j++)
   {
      for (i = 0; i < BREITE; i++)
      {
         if (ausgabeArray[j][i].mehrfachGesetzt)  printf ("*");
         else if (ausgabeArray[j][i].fnktNr == 1) printf ("s");
         else if (ausgabeArray[j][i].fnktNr == 2) printf ("c");
         else if (ausgabeArray[j][i].fnktNr == 3) printf ("x");
         else if (ausgabeArray[j][i].xAchse)      printf ("-");
         else if (ausgabeArray[j][i].yAchse)      printf ("|");
         else                                     printf (" ");
      }
      printf ("\n");
   }
   printf ("s: sin(x)\n");
   printf ("c: cos(x)\n");
   printf ("x: sin(x)*sin(x)\n");
   
   getch();
   return 0;
}
