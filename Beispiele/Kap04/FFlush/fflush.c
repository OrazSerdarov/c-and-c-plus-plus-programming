/* Datei: fflush.c */
#include <stdio.h>
#include <conio.h> 

int main (void)
{
   FILE * fp;
   const char * const filename = "bsp.txt";

   /* Datei oeffnen, eine Zeile Wichtiges anhaengen:              */
   if ((fp = fopen (filename, "a")) == NULL)
   {
      /* Fehlerbehandlung:                                        */
      fprintf (stderr, 
         "Datei '%s' konnte nicht zum Anhaengen" 
         " geoeffnet werden!\n", filename);
      return 1;
   }
   fprintf (fp, "Wichtige neue Information...");                /*(1)*/
   
   if (fflush (fp) != 0)
   {
      fprintf (stderr, 
         "Fehler bei fflush. "
         "Achtung: Daten in %s nicht gesichert!\n", filename);
      return 1;
   }

   {
      /* Schlecht programmiert... Absturz                     (2) */
      int i=0, j = 1 / i;
      /* Doch die Datei ist schon gesichert. Dank fflush().       */
   }

   return 0;
}
