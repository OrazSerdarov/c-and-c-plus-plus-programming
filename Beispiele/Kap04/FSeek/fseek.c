/* Datei: fseek.c */
/* Schreiben und Korrigieren in einer Datei                   */
#include <stdio.h>
#include <ctype.h>   /* Definition von toupper (Makro)        */
#include <conio.h> 

int main (void)
{
   const char * const filename = "bsp.txt";
   FILE * fp;
   char str[] = {'A', 'B', 'C', 'd', 'E', 'F', 'G'};
   int index;
   char c;

   if ((fp = fopen (filename, "w")) == NULL)
   {
      fprintf (stderr,
         "Fehler beim Oeffnen der Datei %s\n", filename);
      return 1;
   }

   for (index = 0; index < 7; index++)
   {
      if (fputc (str [index], fp) == EOF)
      {
         fprintf (stderr, "Fehler beim Schreiben mit fputc\n");
         return 2;
      }
   }

   fclose(fp);
   
   printf("Datei geschrieben!\n");
   c = getch();
   
   if ((fp = fopen (filename, "r+")) == NULL)
   {
      fprintf (stderr,
         "Fehler beim Oeffnen der Datei %s\n", filename);
      return 1;
   }

   if (fseek (fp, 3L, SEEK_SET) != 0)
   {  /* Positionieren nach dem 3. Byte                           */
      fprintf (stderr, "Fehler beim Positionieren mit fseek\n");
      return 3;
   }

   /* Die Funktion toupper() wandelt Klein- in                    */
   /* Grossbuchstaben um:                                         */
   if (fputc (toupper (str [3]), fp) == EOF)
   {
      fprintf (stderr, "Fehler beim Korrigieren mit fputc\n");
      return 4;
   }
   fclose (fp);
   
   printf("Datei geaendert!\n");
   getch();
   return 0;
}
