/* Datei: union.c */
#include <stdio.h>

int main (void)
{
   union zahl { int   intnam;
                long  longnam;
                float floatnam;
              };
   union zahl feld [2], * ptr;
   float * floatptr;
   int* intptr;
   
   /* Groesse einer Union und ihrer Alternativen                  */
   printf ("\nGroesse der Union: %d", sizeof (union zahl));       //  8
   printf ("\nGroesse der Array-Komponenten: %d", sizeof (feld[1]));     //8
   printf ("\nGroess von int  : %d", sizeof (int));						//4
   printf ("\nGroesse von long : %d", sizeof (long));					//8
   printf ("\nGroesse von float: %d\n", sizeof (float));				//8

   feld[0].longnam = 5L;							
   printf ("\nInhalt von feld[0]: %ld", feld[0].longnam);				//5
   feld[0].intnam = 10;		
   printf ("\nInhalt von feld[0]: %d", feld[0].intnam);				//10
   feld[0].floatnam = 100.0;
   printf ("\nInhalt von feld[0]: %6.2f", feld[0].floatnam);			//100.0
   printf ("\n-------------------------------------");

   feld[1] = feld[0];             /* Zuweisung einer Union        */
   printf ("\nInhalt von feld[1]: %6.2f", feld[1].floatnam);			//100.0
   feld[1].floatnam += 25.;
   ptr = &feld[1];                /* Adresse einer Union          */				
   
   /* Umwandlung Zeiger auf Union in Zeiger auf Alternative       */
   floatptr = (float *) ptr;
   intptr = (int*) ptr;

   printf ("\nInhalt von feld[1]: %6.2f",
            ptr -> floatnam);               /* Inhalt Alternative */				//125.0
   printf ("\nInhalt von feld[1]: %6.2f",
            *floatptr);                     /* Inhalt Alternative */					//125.0
   printf ("\nInhalt von feld[1]: %d",
            *intptr);               /* Inhalt Alternative */							//125
   printf ("\ndas war's\n\n\n");

   getch();
   return 0;
}

