#include <iostream>

using namespace std;

// ----------------------------------------------
// Deklaration der Klassen
// ----------------------------------------------
class X;  // Vorw�rts-Deklaration

class Y 
{ 
public:
	void f(X* x);
	void f2(X* x);
};

class X
{
private:
  int i;
public:
  void initialise();
  friend void g(X*, int);  // gilt als Prototyp-Deklaration
  friend void Y::f(X*);
  friend class Z;          // muss vorher nicht deklariert sein
  int getI() { return i; };                        // gilt auch als Vorw�rts-Deklaration
};

void g(X* x, int i) { x->i = i; }

class Z
{
public:
	void times2(X* x);
	void divide2(X* x);
};


// ----------------------------------------------
// Implementierung der Methoden
// ----------------------------------------------
void Y::f(X* x) 
{
	cout << "Wert von x.i: " << x->i << endl;
}

void Y::f2(X* x) 
{
	x->getI();
}

void X::initialise() 
{ 
	i = 0; 
}

void Z::times2(X* x) 
{
	x->i *= 2;
}

void Z::divide2(X* x) 
{
	x->i /= 2;
}

// ----------------------------------------------
// main-Methode
// ----------------------------------------------
int main()
{
	X  x;
	Y  y;
	Z  z;

	x.initialise();
	y.f(&x);
	y.f2(&x);

	g(&x, 5);
	y.f(&x);

	z.times2(&x);
	y.f(&x);

	cin.get();
	return 0;
}


