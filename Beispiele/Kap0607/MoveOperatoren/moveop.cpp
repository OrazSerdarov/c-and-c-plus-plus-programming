#include <iostream>
#include <cstring>
#include <ctime>
#include <vector>

using namespace std;

class Classic
{
	// der Speicherbereich
	char* memory;
	int   size;

public:

	Classic() { memory = nullptr; size = 0; }

	// Der Copy-KONSTRUKTOR
	Classic(const Classic& sObj) : memory(nullptr),
		size(sObj.size)
	{
		if (!size) return;

		// Anlegen des Speichers
		memory = new char[size];

		// Kopieren der Elemente
		memcpy(memory, sObj.memory, size);
	}

	Classic(int nBytes) : size(nBytes)     
	{        
		memory = new char[nBytes];     
	}

	~Classic()
	{
		if(memory != nullptr)
			delete[] memory;
	}

	// Normaler Zuweisungsoperator
	void operator = (const Classic& sOther)
	{
		// Bisherigen Speicher freigeben
		delete[] memory;

		// Neuen Speicher anlegen, 
		// Inhalte kopieren
		if (sOther.size ==0)
		{
			size = 0;
			memory = nullptr;
		}
		else
		{
			size = sOther.size;
			memory = new char[size];

			memcpy(memory, sOther.memory, size);
		}
	}
};

class Simple
{
	// der Speicherbereich
	char* memory;
	int   size;

public:

	Simple() {
		
		
		memory = nullptr; size = 0; }

	// Der MOVE-KONSTRUKTOR
	Simple(Simple&& sObj)
	{
		// �bernahme des Speichers
		memory = sObj.memory;

		// Kopie der Elementanzahl
		size = sObj.size;

		// Losl�sen der Verantwortung
		sObj.memory = nullptr;
	}

	Simple(int nBytes) : size(nBytes)     
	{        
		memory = new char[nBytes];     
	}

	~Simple()
	{
		if(memory != nullptr)
			delete[] memory;
	}

	// Normaler Zuweisungsoperator
	void operator = (const Simple& sOther)
	{
		// Bisherigen Speicher freigeben
		delete[] memory;

		// Neuen Speicher anlegen, 
		// Inhalte kopieren
		if (sOther.size ==0)
		{
			size = 0;
			memory = nullptr;
		}
		else
		{
			size = sOther.size;
			memory = new char[size];

			memcpy(memory, sOther.memory, size);
		}
	}

	// Move-Operator
	void operator = (Simple&& sOther)
	{
		// Speicher freigeben 
		delete[] memory;    

		// Den anderen Speicher �bernehmen
		memory = sOther.memory; 

		// Verantwortung abgeben
		sOther.memory = nullptr;
	}
};

int main()
{

	const int siBufferSize = 1024*1024;
	const int siObjectCount = 100;
	double time1, t_start;

	cout << "Erst ohne Move-Konstruktor" << endl;
	// Erst ohne Move-Konstruktor
	t_start = clock();
	vector<Classic> vec1;
	for (int i=0; i<siObjectCount; ++i)
	{
		vec1.push_back(Classic(siBufferSize));
	}

	time1 = clock() - t_start;
	time1 /= CLOCKS_PER_SEC;
	cout << "Zeit f�r Classic: " << time1 << " s" << endl;

	// Now with move constructor
	t_start = clock();
	vector<Simple> vec2;
	for (int i=0; i<siObjectCount; ++i)
	{
		vec2.push_back(Simple(siBufferSize));
	}

	time1 = clock() - t_start;
	time1 /= CLOCKS_PER_SEC;
	cout << "Zeit f�r Simple: " << time1 << " s" << endl;

	cin.peek();
	return 0;
}

