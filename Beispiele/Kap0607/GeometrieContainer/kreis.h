/* Beispiele zu Programmieren III
   HS Coburg, Kap. 7
*/

#define PI       3.14159265358979323846

// ----------------------------------------------------
class Kreis 
{
private:
    double radius;
    
public:
    Kreis(double _r) : radius(_r) {}
	Kreis(const Kreis& k) : radius(k.radius) {}
	~Kreis() {}
    
    double flaeche() const {
		return radius * radius * PI; }
        
    double umfang() const {
		return 2.0 * radius * PI; }
};

