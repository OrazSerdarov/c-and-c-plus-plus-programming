/* Beispiele zu Programmieren III
   HS Coburg, Kap. 7
*/

#include <iostream>
#include <cmath>
#include "kreis.h"

using namespace std;

class GeometrieContainer {
private: 
  int     anzahl;
  Kreis** kreise;

public:
	GeometrieContainer(int n = 0);
	GeometrieContainer(const GeometrieContainer& gc);
	~GeometrieContainer();

	GeometrieContainer& operator=(const GeometrieContainer& gc);

	Kreis* at(int i) const {
		return kreise[i]; }

	void set(int i, Kreis* k);
};

// ----------------------------------------------------
GeometrieContainer::GeometrieContainer(int n) : 
	  anzahl(n), kreise(0) {
		if (n <= 0)
			return;

		cout << "Hier Standardkonstruktor (" << n << " Elemente)" << endl;
		kreise = new Kreis*[n];
		for(int i = 0; i < n; i++)
			kreise[i] = new Kreis(i + 1.0);
}

// ----------------------------------------------------
GeometrieContainer::GeometrieContainer(const GeometrieContainer& gc) :
	  anzahl(gc.anzahl), kreise(0) {
	if (anzahl <= 0)
		return;

	cout << "Hier Kopierkonstruktor" << endl;
	*this = gc;
}

// ----------------------------------------------------
GeometrieContainer& GeometrieContainer::operator=(const GeometrieContainer& gc)
{
	int i;

	if (this == &gc)
		return *this;

	cout << "Hier Zuweisungsoperator" << endl;
	if (kreise)
	{
		for(i = 0; i < anzahl; i++)
			if (kreise[i])
				delete kreise[i];
		delete[] kreise;
	}

	anzahl = gc.anzahl;

	kreise = new Kreis*[anzahl];

	for(i = 0; i < anzahl; i++)
		if (gc.kreise[i])
		kreise[i] = new Kreis(*(gc.kreise[i]));

	return *this;
}

// ----------------------------------------------------
GeometrieContainer::~GeometrieContainer() 
{
	for(int i = 0; i < anzahl; i++)
		if (kreise[i])
			delete kreise[i];
	delete[] kreise;
}

// ----------------------------------------------------
void GeometrieContainer::set(int i, Kreis* k)
{
	if (i < 0 || i >= anzahl)
		return;

	if (kreise[i])
		delete kreise[i];

	if (k)
		kreise[i] = new Kreis(*k);
	else
		kreise[i] = nullptr;
}


// ----------------------------------------------------
void kreisDaten(Kreis* k)
{
    cout << "Flaeche: " << k->flaeche() << endl;
    cout << "Umfang:  " << k->umfang()  << endl;
}

// ----------------------------------------------------
int main()
{

	Kreis     k(5.0);
	GeometrieContainer  gc(3);
    
	gc.set(0, &k);
	gc.set(1, &k);
	gc.set(2, &k);

    cout << "Daten zum Kreis: " << endl;
    kreisDaten(gc.at(0));

	GeometrieContainer gc2 = gc;
	Kreis k2(10.0);
	gc2.set(0, &k2);

	cout << "Daten zum 2. Kreis: " << endl;
    kreisDaten(gc2.at(0));


    system("pause");
    return 0;
}
    
        

