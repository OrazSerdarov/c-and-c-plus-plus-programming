// Datei Datum.h
//
// Datumsklasse zur Illustration von Konstruktoren und Destruktoren
// (C) 2004, T. Wieland. 

#ifndef _DATUM_H_
#define _DATUM_H_

class Datum
{
private:
  unsigned int t, m, j;
  const int MINJAHR;

public:
  Datum();
  Datum(unsigned int _t,
	unsigned int _m,
	unsigned int _j = 2011);
  Datum(unsigned int _t);
 explicit Datum(const Datum& _datum);
  ~Datum() {} // leer, da hier nicht ben�tigt
  void setze(unsigned int _t,
	     unsigned int _m,
	     unsigned int _j);
  void setzeAufHeute();
  void ausgeben() const;
  bool istSchaltjahr();
}

#endif // _DATUM_H_
