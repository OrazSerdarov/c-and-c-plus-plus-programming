#include <iostream>
#include <conio.h>
#include "Datum.h"


int main()
{

	Datum heutigesDatum;
  std::cout << "Heute ist der ";
  
  heutigesDatum.ausgeben();

  Datum d2 = heutigesDatum;
  Datum d3(heutigesDatum);
  
  std::cin.peek();
  return 0;
}
