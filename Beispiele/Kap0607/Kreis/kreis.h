/* Beispiele zu Programmieren III
   HS Coburg, Kap. 7
*/

#define PI       3.14159265358979323846

#include <iostream>
#include "punkt.h"

#ifndef _KREIS_H_
#define _KREIS_H_

// ----------------------------------------------------
class Kreis 
{
private:
	Punkt  mittelpunkt;
    double radius;
    
public:
	Kreis() : mittelpunkt(0.0, 0.0), radius(0.0) {};
    Kreis(double _r) : mittelpunkt(0.0, 0.0), radius(_r) {
		std::cout << "1. Konstruktor von Kreis mit Radius " << _r << std::endl; }
	Kreis(const Punkt& _p, double _r): mittelpunkt(_p), radius(_r) {
		std::cout << "2. Konstruktor von Kreis mit Radius " << _r << std::endl; }
	Kreis(const Kreis& _k) : mittelpunkt(_k.mittelpunkt), radius(_k.radius) {
		std::cout << "Kopierkonstruktor von Kreis mit Radius " << _k.radius << std::endl; }
	~Kreis() {
	    std::cout << "Destruktor von Kreis" << std::endl; 
	}
    
    double flaeche() const {
		return radius * radius * PI; }
        
    double umfang() const {
		return 2.0 * radius * PI; }
};

#endif