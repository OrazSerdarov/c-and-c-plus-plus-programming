/* Beispiele zu Programmieren III
   HS Coburg, Kap. 7
*/

#include <iostream>
#include "punkt.h"
#include "kreis.h"

using namespace std;

Kreis bestimmeKreis(double v)
{
	Kreis k(Punkt(v, v), v);
	return k; 
}

int main()
{
	/*
	 Punkt     p(2.5, 3.1);
	 Kreis*    pk = new Kreis(p, 4.0);

	 cout << "1. Kreis: \n";
	 cout << "Umfang: " << pk->umfang() << ", Flaeche: " 
		  << pk->flaeche() << endl;

	 {
		Kreis     k(5.0);
	    cout << "2. Kreis: \n";
	    cout << "Umfang: " << k.umfang() << ", Flaeche: " 
		     << k.flaeche() << endl;
	 }

	 {
		Kreis k = bestimmeKreis(2.0);
	    cout << "3. Kreis: \n";
	    cout << "Umfang: " << k.umfang() << ", Flaeche: " 
		     << k.flaeche() << endl;
	 }
	 
	 cout << "Programmende\n";
	 delete pk;
	*/
	 /*
	 Kreis** ka = new Kreis*[4];
	 Kreis* kb = new Kreis[4];
	 Punkt m(0.0, 0.0);

	 for (int i = 0; i < 4; i++) {
		 ka[i] = new Kreis(m, i + 1);
			 cout << i + 1 << ". Kreis des Arrays, Umfang: " << ka[i]->umfang() << endl;
			 cout << i + 1 << ". Kreis des 2. Arrays, Umfang: " << kb[i].umfang() << endl;
			 delete ka[i];
	 }
	 delete[]ka;
	 delete []kb;
	 */

	 cin.peek();
	 return 0;
}




