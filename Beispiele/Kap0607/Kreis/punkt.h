/* Beispiele zu Programmieren III
   HS Coburg, Kap. 7
*/
#include <iostream>

#ifndef _PUNKT_H_
#define _PUNKT_H_

class Punkt
{
private:
	double   x, y;  // Koordinaten

public:
	Punkt(double _x = 0.0, double _y = 0.0) :
	  x(_x), y(_y) {
	  std::cout << "Konstruktor von Punkt";
	  ausgabe();
	  std::cout << std::endl;
	}
	~Punkt() {
		// std::cout << "Destruktor von Punkt" << std::endl; 
	}

	void setX(double _x) { x = _x; }
	void setY(double _y) { y = _y; }
	double getX() const{ return x; }
	double getY() const{ return y; }
	void ausgabe() {
		std::cout << "(" << x << ", " << y << ")"; }
};

#endif
