#include <iostream>
#include <cstring>
#include "Vektor.h"

using namespace std;

int main()
{
  unsigned int i=0;

  Vektor v(15);
  Vektor v2;

  for(i=0; i<15; i++)
    v.set(i, i/4.0);

  for(i=0; i<15; i++)
    cout << v.get(i) << " ";

  cout << endl;
  v.set(20, -1.0);

  Vektor v3 = v;

  v2 = v;

  for(i=0; i<v2.getSize(); i++)
    cout << v2.get(i) << " ";

  cout << endl;
  system("pause");
  return 0;
}
