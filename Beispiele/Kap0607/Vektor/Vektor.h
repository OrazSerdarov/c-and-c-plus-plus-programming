#ifndef _VEKTOR_H_
#define _VEKTOR_H_

// Verwendung von Assertions
#include <cassert>

// Deklaration der Klasse
class Vektor
{
private:
  unsigned int size;
  double* v;

public:
  Vektor() : size(0), v(0) {}
  Vektor(unsigned int _size);
  Vektor(const Vektor& _vek);
  Vektor& operator=(const Vektor& _vek);
  ~Vektor() { if (v) delete[] v; }
  void resize(unsigned int _size);
  unsigned int getSize() const
  { return size; }
  
  double get(unsigned int _i) const;
  void set(unsigned int _i, double _val);

};

// Konstruktor mit Groessenvorgabe
inline Vektor::Vektor(unsigned int _size) :         
  size(_size)
{ 
  v = new double[size]; 
}

// Kopierkonstruktor
inline Vektor::Vektor(const Vektor& _vek) :
  size(0)
{
    v = new double[_vek.size];
    if (v == 0) // kein Speicher!
    {
      return;
    }
    size = _vek.size;

    // Kopieren des Inhalts
    for(unsigned i=0; i<size; i++)
      v[i] = _vek.v[i];
}

Vektor& Vektor::operator=(const Vektor& _vek)
{
	if (this != &_vek)
	{
		if (v)
			delete[] v;
		size = _vek.size;
		v = new double[size];

		for(int i=0; i < size; i++)
			v[i] = _vek.v[i];
	}
	return (*this);
}

// Initialisierung mit Groessenvorgabe
inline void Vektor::resize(unsigned int _size)
{
  if (v)
    delete[] v;

  size = _size;
  v = new double[size];
}

// Lesezugriff
inline double Vektor::get(unsigned int _i) const
{
  // Vorbedingung: _i g�ltig und v vorhanden
  assert(_i<size && v!=0);
  return v[_i];
}

// Schreibzugriff
inline void Vektor::set(unsigned int _i, double _val) 
{
  // Vorbedingung: _i g�ltig und v vorhanden
  assert(_i<size && v!=0);
  v[_i] = _val;
}


#endif  
