// Datei Stack.cpp
//
// Klasse zur Illustration von Ausnahmen
// (C) 2004 - 2015, T. Wieland. 

#include <iostream>

using namespace std;


#define NOEXCEPT noexcept

//----------------------------------------------------
class StackError {
public:
    StackError() NOEXCEPT {}
	virtual const char* what() const NOEXCEPT{
        return "Einfacher Stack-Fehler";
    }
};

//----------------------------------------------------
class StackEmpty : public StackError 
{
public:
	virtual const char* what() const NOEXCEPT{
        return "Stack leer!";
    }        
};

//----------------------------------------------------
class StackFull : public StackError
{
public:
	virtual const char* what() const NOEXCEPT{
        return "Stack voll!";
    }        
};

//----------------------------------------------------
class SimpleStack
{
private:
   float* stack;
   int max;			// maximal g�ltiger Index
   int top;			// Index, an der oberster Wert liegt

public:
  SimpleStack(int _maxEntries);           // Konstruktor
  ~SimpleStack() { delete[] stack; } // Destruktor (inline)
  const float& pop();
  void push(float);
  bool isEmpty() { return top == 0; }
  bool isFull() { return top == max; }
};

SimpleStack::SimpleStack(int _maxEntries)
{
  stack = new float[_maxEntries];
  max = _maxEntries;
  top = 0;
}
  
const float& SimpleStack::pop() 
{
  if (isEmpty()) throw StackEmpty();
  return stack[--top];
}

void SimpleStack::push(float x) 
{
  if (isFull()) throw StackFull();
  stack[top++] = x;
}

//----------------------------------------------------
int main()
{
  SimpleStack stack(30);
  int i = 0;
  
  try 
  { 
    //cout << stack.pop(); 
    for (i=0;i<35; i++)
      stack.push(i);
  }  
  catch(StackError& se) 
  { 
    cerr << "So geht's nicht! (i = " << i << "): " << se.what() << endl; 
  }

  system("pause");
  return 0;
}
