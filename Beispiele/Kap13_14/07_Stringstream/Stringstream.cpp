#include <sstream>
#include <iostream>
#include <iomanip>
using namespace std;


void ausgabestream()
{
	ostringstream outf;         // Dynamischen String-Ausgabe-Stream erzeugen
	// Stream wie �blich f�llen
	outf << setw(6) << setfill('0') << 2.7 / 2 << " abc";
	cout << "Der zusammengestellte String: " << outf.str() << endl;
}

void eingabestream()
{
	char   c, t[20], s[]="123 123.456 Hallo c ...";
	int    i;
	double d;
	istringstream str(s);
	str >> i >> d >> t >> c;	// Der String wird elementweise zerlegt

	cout << "Inhalt von i: " << i << endl;
	cout << "Inhalt von d: " << d << endl;
	cout << "Inhalt von t: " << t << endl;
	cout << "Inhalt von c: " << c << endl;
}

int main()
{
	cout << "Demo f�r Ausgabestream: " << endl;
	ausgabestream();	
	cout << "------------------------" << endl;
	cout << "Demo f�r Eingabestream: " << endl;
	eingabestream();

	cin.get();
	return 0;
}  

