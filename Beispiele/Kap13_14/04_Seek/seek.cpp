/* Datei: seek.cpp */
/* Schreiben und Korrigieren in einer Datei                   */
#include <iostream>
#include <fstream>
#include <cctype>   // Definition von toupper (Makro)        

using namespace std;

int main (void)
{
   const char * const filename = "bsp.txt";
   ofstream of(filename);
   char str[] = {'A', 'B', 'C', 'd', 'E', 'F', 'G'};
   int index;

   if (!of)
   {
      cerr << "Fehler beim Oeffnen der Datei " << filename << endl;
      return 1;
   }

   for (index = 0; index < 7; index++)
   {
      of.put(str[index]);
      if (of.eof())
      {
         cerr << "Fehler beim Schreiben mit put" << endl;
         return 2;
      }
   }

   // Positionieren nach dem 3. Byte                           
   of.seekp(3, ios_base::beg);

   // Die Funktion toupper() wandelt Klein- in Grossbuchstaben um: 
   of.put(toupper(str[3]));
   of.close();
   
   system("pause");
   return 0;
}
