#include <iostream>
#include <string>
#include <iomanip>

using namespace std;
int main()
{
  int i = 0;
  string zeichenkette;
  cout << "Ihre Eingabe: ";
  cin >> hex >> i;
  if(cin.fail())
  {
    cout << "Fehler: kein Integerwert gefunden!" << endl;
    cin.clear();
    cin >> zeichenkette;
    if(cin.good())
      cout << "aber folgender Text:  " << zeichenkette << endl;
    else
      cout << "!Fehler! - Eingabe gescheitert!" << endl;
  }
  else
    cout << "Die Zahl ist: "  << i << endl;
  cin.ignore(100, '\n');
  cin.get();
  return 0;
}

