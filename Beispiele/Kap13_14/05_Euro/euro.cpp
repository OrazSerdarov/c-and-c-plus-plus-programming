#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
  int i,j;
  const double wk = 1.08945; // Kurs vom 18.01.2016

  cout.precision(2);
  cout.setf(ios_base::right | ios_base::fixed, 
    ios_base::adjustfield | ios_base::floatfield);
  cout << setw(10) << "US-$" << setw(10) 
       << "Euro" << " | " << setw(10) 
       << "Euro" << setw(10) << "US-$" << endl;

  for(i=1;i<=44;i++) cout << '-';
  cout << endl;

  for(i=1;i<=1000;i*=10) {
    for(j=1;j<=5 && i*j<=1000;j+=1) {
      cout << setw(10) << (float)i*j 
	       << setw(10) << i*j/wk << " | "
           << setw(10) << (float)i*j 
	       << setw(10) << i*j*wk << endl;
    }
  }
  
  cin.peek();
  return(0);
}


