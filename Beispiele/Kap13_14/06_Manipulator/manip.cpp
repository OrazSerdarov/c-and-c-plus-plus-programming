#include <iostream>
#include <iomanip>

using namespace std;

// einfacher eigener Manipulator ohne Argumente
ostream& startline(ostream& os) {
	static int zeile = 1;
	return os << setw(3) << zeile++ << ": ";
}

// Manipulator mit int-Argument als Klasse
class space {
	friend ostream& operator<<(ostream&, const space&);
public:
	space(int nn) : n(nn) {}
private:
	const int n;
};

ostream& operator<<(ostream& os, const space& s) {
	for (int i=0; i<s.n; i++)
		os << ' ';
	return os;
}


int main() {
	cout << startline << "Beginn" << endl;
	for (int i=0; i<10; i++)
		cout << startline << i << endl;
	cout << startline << "Ende" << endl;

	cout << endl << "0123456\n";
	cout << '#' << space(5) << '#' << endl;

	cin.peek();
	return 0;
}