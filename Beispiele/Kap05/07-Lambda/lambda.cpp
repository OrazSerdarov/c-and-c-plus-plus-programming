/* Programmieren III
   HS Coburg
*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	char s[]="Hello World!";
	int Uppercase = 0; //modified by the lambda

	for_each(s, s+sizeof(s), [&Uppercase] (char c){
		if (isupper(c))
			Uppercase++;

	});

	cout<< Uppercase<<" uppercase letters in: "<< s<<endl;

	cout << "---------------------------------------" << endl;

	vector<int> indices(10);
	int count = 20;
	generate(indices.begin(),indices.end(), [&](){ return --count; });
	sort(indices.begin(), indices.end(), 
		[](int a, int b) ->bool { return a < b; });

	for_each(indices.begin(), indices.end(), [](int &i){ cout << i << " "; });

	cout << endl;

	cin.peek();
	return 0;
}