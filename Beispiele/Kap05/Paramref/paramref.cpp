//
// paramref.cpp
// Beispielprogramm fuer Parameteruebergabe als Referenz
//

#include <iostream>

using namespace std;

void swap_values (int x, int y)
{
  int temp = x;
  x = y;
  y = temp;
  return;
}

void swap_refs (int& x, int& y)
{
  int temp = x;
  x = y;
  y = temp;
  return;
}

int main (void)
{
  int big = 10;
  int small = 20; 

  cout << "big1: "<< big << " small1: "<< small << endl;
  
  swap_values (big, small);
  cout << "big2: "<< big << " small2: "<< small << endl;
  
  swap_refs (big, small);
  cout << "big3: "<< big << " small3: "<< small << endl;

  std::swap(big, small);
  cout << "big4: " << big << " small4: " << small << endl;

  system("pause");
  return 0;
}

