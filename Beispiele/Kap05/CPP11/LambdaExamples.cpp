// Copyright (C) 
// Ajay Vijayvargiya

// This source code was published on CodeProject.com, under CPOL license
// This code CANNOT be used for similar publication on the web or as printed material.
// This can, however, be used as educational reference in educational institutions.
// You are allowed to use the accompanying code in your programs, 
//    commercial or non commercial. 

// This source code is only intended to explain the new C++ standard (codenamed as C++0x),
// and thus may contain bugs, some logical issues etc.
// The explanation is relevant to Visual C++ 2010 (Compiler version: 16.0)

// http://www.codeproject.com/KB/cpp/cpp10.aspx 

// Code that is invalid/errornous is marked as //#

// The source file assumes that you've read and understood autos and decltype

#include"stdafx.h"



// IMPORTANT!
// This source file assumes that you've read and understood the 
// section "Lambda Expressions" in the article linked above.
// Or, you are parallelly reading and following this code.

void LambdaWithCapture();
void LambdaWithCallback();
void LambdaWithSTL();

void LambdaExamples()
{
	// Defining and calling a lambda
	double pi = []{ return 3.14159; }(); // Returns double

	// Defining and storing the lambda in auto variable
	// Note that the function call is NOT at the end of lambda
	auto IsEven = [](int n) { return n%2==0; };

	// calling it now
	IsEven(21);


	// Lambda with two arguments - CALLING
	int nMax = [](int n1, int n2) 
	{	
		return (n1>n2) ? (n1) : (n2);
	} 
	(56, 11); // Calling here

	std::cout << nMax << std::endl;

	// Defining similar lambda, and storing into variable
	// Also note that it gives explicit return type

	auto Min= [](int n1, int n2) -> int
	{
		return (n1<n2) ? (n1) : (n2);
	};

	std::cout << Min(2, 55) << std::endl;

	// Lambda that returns sum of all elements
	auto SumArray = [](int Array[], int nSize) -> __int64// second [] is NOT lambda, but array!
	{
		__int64 nSum = 0;

		for(int i=0; i<nSize; ++i)
			nSum += Array[i];

		return nSum;
	};


	int aElemens[5] = { 83844, 18000000, 0xFFFF0000, 1900000012, 1234567890};

	// Calling lambda
	std::cout << "Array sum is: " << SumArray(aElemens, 5) << std::endl;	

	LambdaWithCapture();

	LambdaWithCallback();

	LambdaWithSTL();	
}

void LambdaWithCapture()
{
	// The function illustrates how capture-specification can be used for stateless 
	// and stateless lambdas.

	int nTickCount;
	int n = 500;

	nTickCount = GetTickCount();

	auto ShowTickCount = [nTickCount]() -> void
	{
		// nTickCount is captures by value
		// the following will print the SAME value, 
		// even if nTickCount is changed at upper level
		std::cout<<"Tick count: " << nTickCount << "\n";
	};

	ShowTickCount();
	
	// Reset
	nTickCount = 0;
	ShowTickCount();


	

	// Now let's capture nTickCount by reference
	// ALL other variables are captured by value
	auto ShowTickCountEx = [=, &nTickCount]
	{
		// This will be updated
		std::cout<<"Updated Tick count is: " << nTickCount << "\n";		

		// This will not.
		std::cout<<"N = " << n << "\n";
	};

	nTickCount = GetTickCount() + 10;		
	ShowTickCountEx();

	n=4000;
	nTickCount = GetTickCount();
	ShowTickCountEx();

	auto UpdateTickCount = [&nTickCount](int nMod)
	{
		// Update tick count with modulus given.
		nTickCount = GetTickCount() % nMod;
	};

	UpdateTickCount(40);

	std::cout << "After update: " << nTickCount << "\n";

	// Let's temporarily modify nTickCount withing lambda
	// This requires mutable keyword. 
	auto UpdateTCTemp = [nTickCount]() mutable
	{
		nTickCount = 0;

		std::cout << "Temporary update: "<< nTickCount << "\n";
	};

	// Temporary update
	UpdateTCTemp();

	// But original tickcount is till same!
	ShowTickCountEx();


	// We see that with lambdas, we can capture:
	// - By value
	// - By mutable value
	// - By reference
	
	// But unfortunately, lambdas doesn't allow:
	// - Capture by const reference.
	// Thus, following is not possible (but desirable IMO)
	//# [const nTickCount&](){}
}

void CallbackSomething(int nNumber, std::function<void(int)> callback_function) 
{
    // Call the specified 'function'
    callback_function(nNumber);
}


// Function
void IsEven(int n)
{
   std::cout << ((n%2 == 0) ? "Yes" : "No") << "\n";
}

// Class with operator () overloaded
class Callback
{
	/*const*/ int Predicate;
public:
	Callback(int nPredicate) : Predicate(nPredicate) {}

	void operator()(int n)
	{
		if( n < Predicate)
         std::cout << "Less than " << Predicate << "\n";
      else
         std::cout << "More than " << Predicate << "\n";

	}
};

void LambdaWithCallback()
{
   // Passing function pointer
   CallbackSomething(10, IsEven);

   // Passing function-object
   CallbackSomething(23, Callback(24));

   // Another way..
   Callback obj(99);
   CallbackSomething(44, obj);

   // Locally defined lambda!
   CallbackSomething(59, [](int n)    { std::cout << "Half: " << n/2 << "\n";}     );



   // Stateful lambdas
   int Predicate = 40;

   // Lambda being stored in 'stateful' variable
   auto Stateful  = [&Predicate](int n)		// By reference
   {  
	   if( n < Predicate)
		   std::cout << "Less than " << Predicate << "\n";
	   else
		   std::cout << "More than " << Predicate << "\n"; 
   };

   CallbackSomething(59, Stateful ); // More than  40

   Predicate=1000; 
   CallbackSomething(100, Stateful);  
}



void LambdaWithSTL()
{	
	using namespace std;
    
	// Simplified example first
	int Array[10] = {1,2,3,4,5,6,7,8,9,10};
	for_each(Array, &Array[10], IsEven);

	for_each(Array, Array+10, [](int n){ std::cout << n << std::endl;});


	// With std::vector
	vector<int> IntVector;

	// Just copy from Array!
	IntVector.assign(Array, Array+10);


	// This lambda uses multiple features:
	// -- decltype to deduce argument type!
	// -- auto keyword in for-loop for iterator
	// -- shows std namesspace is NOT inherited from outer scope!

	// If you change IntVector to something else (like std::list<float>)
	// the following code would still compile!
	auto DisplayVector = [](/*const*/ decltype(IntVector) & theVector)
	{
		int nPos=0;
		
		for(auto iter = theVector.begin(); iter != theVector.end(); ++iter)
		{
			// std:: is must! namespace does NOT get inherited in lambda!
			std::cout << "Element [" << nPos << "] is " << *iter << std::endl;

			++nPos;
		}
	};

	DisplayVector(IntVector); 

	// Let's double them! NOTE the '&' in lambdas' argument.
	// IMPORTANT!
	// If you miss to put ampersand, the compiler will NOT give you any 
	// warning/error. for_each just calls the function, since it uses
	// function type as template (and not as function pointer, or function object).
	// It doesn't care if argument is int or int&. 'const' can also be put.
	for_each(IntVector.begin(), IntVector.end(), [](int& n) { n*=2; });


	// Display
	DisplayVector(IntVector);

	// Transform the Array into Vector, multiplying it by 100
	transform(Array , Array+10 , IntVector.begin(),
		[](int n) { return n * 100; } );

	DisplayVector(IntVector); 
}