#include <cmath>
#include "moritz.h"

namespace Moritz
{

const double EPS = 0.1;

double algorithm(double x)
{
  return std::sin(x);
}

}

