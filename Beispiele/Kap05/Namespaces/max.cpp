#include <cmath>
#include "max.h"

const double Max::EPS = 0.001;

double Max::algorithm(double x)
{
  return std::sqrt(x);
}
