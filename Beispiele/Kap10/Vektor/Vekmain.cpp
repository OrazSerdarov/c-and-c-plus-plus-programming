#include <iostream>
#include <cstring>
#include "Vektor.h"

using namespace std;

int main()
{
  unsigned int i=0;

  Vektor v(15);

  for(i=0; i<15; i++)
    v.set(i, i/4.0);

  for(i=0; i<15; i++)
    cout << v.get(i) << " ";

  cout << endl;

  system("pause");
  return 0;
}
