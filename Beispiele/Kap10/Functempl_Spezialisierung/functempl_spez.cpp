#include  <iostream>
using namespace std;

class CBruch {
 public:
    CBruch( int z = 1, int n = 1 ) : m_zaehler(z), m_nenner(n) { }

    void print(void) { cout << m_zaehler << "/" << m_nenner << endl; }

    const CBruch operator+= (const CBruch& b) {
        m_zaehler = m_zaehler* b.m_nenner + b.m_zaehler*m_nenner;
        m_nenner  = m_nenner * b.m_nenner;
        return *this;
    }

 private:
    int m_zaehler, m_nenner;
};

template <typename T>
T summeArray(T array[], unsigned n) {
   T summe = 0;
   for (unsigned i=0; i < n; i++)
       summe += array[i];
   return summe;
}
template <> char *summeArray<char *>(char *array[], unsigned n) {
   double        summe = 0;
   static char   rueckgabe[50];
   for (unsigned i=0; i < n; i++)
       summe += atof(array[i]);
   sprintf(rueckgabe, "%g", summe);
   return rueckgabe;
}

int main(void) {
   int    ganz [5] = { 10, 20, 30, 40, 50                    };
   double gebr [4] = { 3.14, 12.43, 7.0, 2.0                 };
   CBruch bruch[3] = { CBruch(1,2), CBruch(3,4), CBruch(3,2) };
   char   *str [4] = { "3.14", "12.43", "7.0", "2.0"         };

   cout << "Summe von ganz : " << summeArray(ganz, 5) << endl;
   cout << "Summe von gebr : " << summeArray(gebr, 4) << endl;
   CBruch sum = summeArray(bruch, 3);
   cout << "Summe von bruch: "; sum.print();
   cout << "Summe von str  : " << summeArray(str, 4) << endl;

   cin.peek();
   return 0;
}
