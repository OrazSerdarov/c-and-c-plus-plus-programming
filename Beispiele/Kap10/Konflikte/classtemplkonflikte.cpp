#include <iostream>
using namespace std;
 
template<typename T, typename U>
class   C   { public: void fkt() { cout << "(1), "; } };
 
template<typename U>
class C<int, U>    { public: void fkt() { cout << "(2), "; } };
 
template<typename T>
class C<T, double> { public: void fkt() { cout << "(3), "; } };
 
template<typename T, typename U>
class C<T*, U>     { public: void fkt() { cout << "(4), "; } };
 
template<typename T, typename U>
class C<T, U*>     { public: void fkt() { cout << "(5), "; } };
 
template<typename T, typename U>
class C<T*, U*>    { public: void fkt() { cout << "(6), "; } };
 
template<typename T>
class C<T, T>      { public: void fkt() { cout << "(7), "; } };


 
int main(void) {
   C<float, int>   o1; o1.fkt(); // (1): T,   U
   C<int, float>   o2; o2.fkt(); // (2): int, U
   C<float, double>o3; o3.fkt(); // (3): T,   double
   C<float*, float>o4; o4.fkt(); // (4): T*,  U
   C<float, float*>o5; o5.fkt(); // (5): T,   U*
   C<float*, int*> o6; o6.fkt(); // (6): T*,  U*
   C<float, float> o7; o7.fkt(); // (7): T,   T
   cout << endl;
   //......Folg. nicht m�glich, da Konflikt zwischen ....
   // C<int, int>       o8;  o8.fkt(); // (2): int, U    <--> (7): T, T
   // C<double, double> o9;  o9.fkt(); // (3): T, double <--> (7): T, T
   // C<float*, float*>o10; o10.fkt(); // (4), (5), (6) und (7)
   // C<int, int*>     o11; o11.fkt(); // (1), (2) und (5)
   // C<int*, int*>    o12; o12.fkt(); // (4), (5), (6) und (7)
   cin.peek();
}
