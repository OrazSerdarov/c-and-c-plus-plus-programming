#include <iostream>
#include <string>
#include "Vektor.hpp"
#include "Datum.hpp"

using namespace std;

class A {
	int x;

public:
	A(int _x = 7) : x(_x) {};
	int getX() { return x; }
};

int main()
{
  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

  for(i=0; i<5; i++)
    cout << v.at(i) << " ";

  cout << endl;

  Vektor<A> va(10);
  for(i=0; i<10; i++)
	cout << "ca["<< i << "]="<<va.at(i).getX() << endl;

  // Vektor von Datum
  Vektor<Datum> daten(31);

  for(i=0; i<31; i++)
    daten.at(i).setze(i+1, 12, 2013);
  /*
  for(i=23; i<31; i++)
    daten.at(i).ausgeben();*/

  daten.print();
  
  // Vektor von Vektor
  Vektor<Vektor<double>> m(3);
  for(i=0; i<3; i++) 
  {
    m.at(i).resize(3);
    m.at(i).init(0);
  }

  for(i=0; i<3; i++)
    m.at(i).at(i) = 1;

  for(i=0; i<3; i++)
  {
    for(int j=0; j<3; j++)
        cout << m.at(i).at(j) << " ";
    cout << endl;
  }     

  cin.get();
  return 0;
}
