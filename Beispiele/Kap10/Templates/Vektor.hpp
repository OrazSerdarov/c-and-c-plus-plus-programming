#ifndef _VEKTOR_H_
#define _VEKTOR_H_

// Verwendung von Assertions
#include <cassert>
#include <iostream>

// Deklaration der Klasse
template <typename T> class Vektor
{
private:
  unsigned int size;
  T* v;

public:
  Vektor() : size(0), v(0) {}
  Vektor(unsigned int _size);
  Vektor(const Vektor& _vek);
  ~Vektor() { if (v) delete[] v; }
  void resize(unsigned int _size);
  void init(const T& _val);
  unsigned int getSize() const {
    return size; }
  const T& at(unsigned int _i) const;
  T& at(unsigned int _i);
  void print() const;
};

// Konstruktor mit Groessenvorgabe
template <typename T> 
Vektor<T>::Vektor(unsigned int _size) :         
  size(_size)
{ 
  v = new T[size]; 
}

// Kopierkonstruktor
template <typename T>
Vektor<T>::Vektor(const Vektor<T>& _vek) :
  size(_vek.size)
{
	if (!size) {
		v = nullptr;
		return;
	}
	
	v = new T[size];

	for(unsigned i = 0; i< size; i++)
		v[i] = _vek.v[i];
}

// Initialisierung mit Groessenvorgabe
template <typename T> 
void Vektor<T>::resize(unsigned int _size)
{
  if (v)
    delete[] v;

  size = _size;
  v = new T[size];
}

// Vorbelegung des Array mit einem Wert
template <typename T>
void Vektor<T>::init(const T& _value)
{
  for(unsigned int i=0; i<size; i++)
    v[i] = _value;
}

// Lesezugriff
template <typename T>
const T& Vektor<T>::at(unsigned int _i) const
{
  // Vorbedingung: _i g�ltig und v vorhanden
  assert(_i<size && v!=0);
  return v[_i];
}

// Schreibzugriff
template <typename T>
T& Vektor<T>::at(unsigned int _i) 
{
  // Vorbedingung: _i g�ltig und v vorhanden
/*	static T temp(1);
	if(_i >= size)
		return temp;
		*/
  assert(_i<size && v!=0);
  return v[_i];
}

// Ausgabe

template <typename T>
void Vektor<T>::print() const
{
  using namespace std;

  for(int i = 0; i < size; i++)
      cout << " " << v[i];
      
  cout << endl;
}

  
#endif  
