#include <iostream>
using namespace std;

//.............................................................. Klassentemplate
template<typename T = int>
class CArray {
 public:
    T& operator[](int index);
 private:
    enum { max = 5  };
    T    m_array[max];
};

template<typename T> // keine Default-Angabe mehr
T& CArray<T>::operator[](int index) {
   if (index < 0 || index >= max) {
      cerr << "    ----> Ungültiger Index: " << index << endl;
      exit(1);
   }
   return m_array[index];
}

//......................................................................... main
int main(void) {
   int            i;
   //.................... Instantiierung von Objekten zum Klassentemplate
   CArray<int>    r1; // int-Array durch explizite Angabe
   CArray<>       r2; // int-Array, da Default-Platzhalter int ist
   CArray<double> f1, f2; // double-Arrays durch explizite Angabe

   for (i=0; i < 5; i++) {
      r1[i] = i;
      f1[i] = r1[i] * r1[i] * 3.14;
   }
   for (i=0; i < 5; i++) {
      r2[i] = i+5;
      f2[i] = r2[i] * r2[i] * 3.14;
   }
   for (i=0; i < 5; i++)
      cout << r1[i] << ": " << f1[i] << ",   " << r2[i] << ": " << f2[i] << endl;

   int j = 5;
   int* i;
   i = j;

   cout << i << endl;;
   cin.peek();
   return 0;
}
