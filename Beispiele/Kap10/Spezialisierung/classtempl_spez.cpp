#include  <iostream>
using namespace std;

//........................................................ Klassentemplate CAdd
template<typename T>
class CAdd {
 public:
    CAdd(T array[], unsigned n) : m_sum(0) {
       for (unsigned i=0; i < n; i++)
           m_sum += array[i];
    }
    void print(char *text) { cout << text << m_sum << endl; }
 private:
    T  m_sum;
};

//........................................ Spezialisiertes Klassentemplate CAdd
template<>
class CAdd<char *> {

 public:
    CAdd(char *array[], unsigned n) {
       m_sum = new char [1000]; // der Einfachheit halber
       *m_sum = 0;
       for (unsigned i=0; i < n; i++)
           strcat(m_sum, array[i]);
    }
    void print(char *text) { cout << text << m_sum << endl; }
 private:
    char *m_sum;
};


//................................................... Normale Klasse CAddString
class CAddString {
 public:
    CAddString(char *array[], unsigned n) {
       m_sum = new char [1000]; // der Einfachheit halber
       *m_sum = 0;
       for (unsigned i=0; i < n; i++)
           strcat(m_sum, array[i]);
    }
    void print(char *text) { cout << text << m_sum << endl; }
 private:
    char *m_sum;
};

//........................................................................ main
int main(void) {
   int    ganz[5] = { 10, 20, 30, 40, 50                    };
   double gebr[4] = { 3.14, 12.43, 7.0, 2.0                 };
   char   *str[4] = { "Ich ", "bin ", "nun ", "ein String"  };

   CAdd<int>    iSum(ganz, 5); iSum.print("Summe von ganz: ");
   CAdd<double> dSum(gebr, 4); dSum.print("Summe von gebr: ");
   CAdd<char *> sSum(str, 4);  sSum.print("Zusammengefuegt: ");
	 CAddString   ssSum(str, 4);  sSum.print("Zusammengefügt: ");

   cin.peek();
   return 0;
}
