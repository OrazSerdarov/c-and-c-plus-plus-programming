#include <iostream>
using namespace std;

class Zugriff
{
public:
	Zugriff() : anz(0) {}
	int anzahl() const 
	{
		return ++anz;
	}
private:
	mutable int anz;
};

int main()
{
	const 	Zugriff zugriffe;

	cout << zugriffe.anzahl() << endl;
	cout << zugriffe.anzahl() << endl;
	cout << zugriffe.anzahl() << endl;

	cin.peek();
	return 0;
}


