#include <iostream>
#include <cstring>

template <typename T>
T max (T a, T b)
{
  return (a>b)? a: b;
}

template<>
const char* max(const char* a, const char* b)
{
  return std::strcmp(a,b)>0? a: b;
}

int main()
{
  using std::cout;
  using std::endl;

  cout << "max(1, 5): " 
       << max(1, 5) << endl;
  cout << "max(3, 3.5): " 
       << max<double>(3, 3.5) << endl;
  cout << "max(abc, bcde): " 
       << max("abc", "bcde") << endl;

  system("pause");
  return 0;
}
