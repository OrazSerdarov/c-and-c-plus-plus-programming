/************************************************
Algorithmen und Problemloesungen mit C++,
http://www.algorithmen-und-problemloesungen.de
Copyright @2007 by Doina Logofatu
************************************************/

#include <fstream>
#include <vector>
#include <cmath>
#include <iostream>

double const pi(4*atan(1.0));

using namespace std;

class Complex{
private: 
  double _re, _im;
public:
  Complex (){_re=0; _im=0;};
  Complex(double re, double im){
    _re=re; _im=im;
  }
  Complex(double re) {
	  _re = re; _im = 0.0;
  }
  ~Complex(){};
  Complex operator+(Complex&);
  Complex operator-(Complex&);
  Complex operator*(Complex&);
  friend istream& operator>>(istream&, Complex&);
  friend ostream& operator<<(ostream&, Complex&);
};

inline Complex Complex::operator+(Complex&z){
  return Complex(_re+z._re, _im+z._im);
}

inline Complex Complex::operator-(Complex&z){
  return Complex(_re-z._re, _im-z._im);
}

inline Complex Complex::operator*(Complex&z){
  return Complex(_re*z._re-_im*z._im, _re*z._im+_im*z._re);
}

istream& operator>>(istream& is, Complex& z){
  is >>z._re>>z._im;
  return is;
}

ostream& operator<<(ostream& os, Complex& z){
  os<<z._re<<"  "<<z._im;
  return os;
}

vector<Complex> FFT(vector<Complex> a){
  int i, n=(int)a.size();
  if(n<=1) return a;
  Complex wn(cos(2*pi/n), sin(2*pi/n));
  Complex w(1.0, 0.0);
  vector<Complex> a0, a1, y0, y1, y;
  for(i=0; i<n; i++){
    if(i%2==0)a0.push_back(a[i]);
    else a1.push_back(a[i]);
    y.push_back(Complex(0, 0));
  } 
  y0=FFT(a0);
  y1=FFT(a1);  
  for(i=0; i<n/2; i++){
    y[i]=y0[i]+w*y1[i];
    y[i+n/2]=y0[i]-w*y1[i];
    w=w*wn;
  }
  return y;
}

int main(){
  vector<Complex> a, y;
  Complex w, aux, E, res;
  ifstream in("fourier.in");
  ofstream out("fourier.out");
  while(in && !in.eof() && in>>aux){
    a.push_back(aux);
  }
  int n=(int)a.size();
  y=FFT(a);  
  for(int i=0; i<(int)y.size(); i++)
  out <<y[i]<<endl;

  cout << "Transformation abgeschlossen." << endl;
  cin.peek();
  return 0;
}
