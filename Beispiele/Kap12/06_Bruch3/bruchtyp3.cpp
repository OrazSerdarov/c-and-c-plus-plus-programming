#include <iostream>
using namespace std;
//................................................................. Klasse CBruch
class CBruch
{
public:
    CBruch( int z = 1, int n = 1 ) : m_zaehler(z), m_nenner(n) { }

    void print(char *name) { //..................... Ausgabe eines Bruches
       cout << name << " = " << m_zaehler << "/" << m_nenner << endl;
    }
    const CBruch operator- () const {
        // Erzeugt ein tempor�res unbenanntes CBruch-Objekt, initialisiert 
        // es mit -m_zaehler und m_nenner, und  gibt es als Wert zur�ck.
      return CBruch(-m_zaehler, m_nenner);
    }
	operator double() const {
		if (m_nenner != 0)
			return static_cast<double>(m_zaehler) / m_nenner;
		else
			return 0.0;
	}
private:
    int m_zaehler, m_nenner;
};
//.......................................................................... main
int main(void)
{
    CBruch  b1( 2, 4 );  // 1. Bruch (2/4)
    CBruch  b2( 3, 8 );  // 2. Bruch (3/8)
    CBruch  b3;          // 3. Bruch (1/1)

    b1 = -b2;
    b1.print("b1");  // Ausgabe von b1
	cout << b1 << endl;  // implizite Umwandlung nach double

    b3 = -b1;
    b3.print("b3"); // Ausgabe von b3
	cout << static_cast<double>(b3) << endl;  // explizite Umwandlung

	cin.peek();
	return 0;
}
