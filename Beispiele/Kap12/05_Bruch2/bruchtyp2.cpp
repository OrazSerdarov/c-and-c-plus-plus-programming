#include <iostream>

//................................................................. Klasse CBruch
class CBruch
{
    friend       CBruch& operator+= (CBruch& b1, const CBruch& b2);
    friend const CBruch  operator+  (const CBruch& b1, const CBruch& b2);
public:
    CBruch( int z = 1, int n = 1 ) : m_zaehler(z), m_nenner(n) { }

    void print(const char *name) const { //......... Ausgabe eines Bruches
       std::cout << name << " = " << m_zaehler << "/" << m_nenner << std::endl;
    }
private:
    int m_zaehler, m_nenner;

    int ggT( int n, int m) { return (m==0) ? n : ggT(m, n%m); }

    void kuerzen(void) {
       int ggTeiler = ggT(m_zaehler, m_nenner);
       m_zaehler /= ggTeiler;
       m_nenner  /= ggTeiler;
    }
};

//.................................................. Globale Operatorenfunktionen
const CBruch operator+ (const CBruch& b1, const CBruch& b2) {
   CBruch  sum;
   sum.m_zaehler = b1.m_zaehler* b2.m_nenner + b2.m_zaehler*b1.m_nenner;
   sum.m_nenner  = b1.m_nenner * b2.m_nenner;
   sum.kuerzen();
   return sum;
}
CBruch& operator+= (CBruch& b1, const CBruch& b2) {
    b1 = b1 + b2;
    return b1;
}
//.......................................................................... main
int main(void) {
    CBruch  b1( 1, 4 );  // 1. Bruch (1/4)
    CBruch  b2( 3, 8 );  // 2. Bruch (3/8)
    CBruch  b3;          // 3. Bruch (1/1)
    b1 += b2;        // Addiere b2 zu b1
    b1.print("b1");  // Ausgabe von b1

    b3 = b1 + b2;   // Addiere b1 und b2 --> b3
    b3.print("b3"); // Ausgabe von b3

	std::cin.peek();
	return 0;
}
