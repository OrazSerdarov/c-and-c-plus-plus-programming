#include <iostream>
using namespace std;
//................................................................. Klasse CBruch
class CBruch
{
public:
    CBruch( int z = 1, int n = 1 ) : m_zaehler(z), m_nenner(n) { }

    void print(char *name) { //..................... Ausgabe eines Bruches
       cout << name << " = " << m_zaehler << "/" << m_nenner << endl;
    }

    const CBruch& operator++() { //........ PR�FIX-Operator
       m_zaehler += m_nenner;
       return *this;    // ver�ndertes Objekt in Kopie zur�ckgeben
    }
    const CBruch operator++(int) { //...... POSTFIX-Operator
       CBruch tmp = *this;    // Objektwert sichern
       m_zaehler += m_nenner;
       return tmp;  // Kopie des nicht ver�nderten Objektes zur�ckgeben
    }
private:
    int m_zaehler, m_nenner;
};
//.......................................................................... main
int main(void) {
    CBruch  b1(1, 2), b2;

    b1.print("b1");  // Ausgabe von b1
    b2 = ++b1;
    b1.print("   Nach b2 = ++b1:   b1");  // Ausgabe von b1
    b2.print("   Nach b2 = ++b1:   b2");  // Ausgabe von b2

    b1 = CBruch(1, 2);
    b1.print("b1");  // Ausgabe von b1
    b2 = b1++;
    b1.print("   Nach b2 = b1++:   b1");  // Ausgabe von b1
    b2.print("   Nach b2 = b1++:   b2");  // Ausgabe von b2

	cin.peek();
	return 0;
}
