
#include <list>
#include <stack>
#include <string>
#include <vector>
#include <algorithm>    // für find, replace, sort etc.
#include <functional>   // für greater
#include <iterator>
#include <sstream>
#include <iostream>
#include <fstream>

//---------------------------------------------------------------------------
using namespace std;

struct Konto {
  unsigned long nummer;
  string inhaber;

  Konto() : nummer(0L) {}  // string hat selbst einen Default-Konstruktor
};

//---------------------------------------------------------------------------
void liesKonten(list<Konto>& kontoListe, list<Konto>& inverseListe, const string& dateiName)
{
  ifstream in_file(dateiName.c_str);
  if (in_file.bad()) return;

  while (in_file)
  {
    Konto einKonto;
    in_file >> einKonto.nummer >> einKonto.inhaber;
    inverseListe.push_front(einKonto);   // am Anfang einfügen
    kontoListe.push_back(einKonto);    // am Ende einfügen
  }

  list<Konto>::iterator  iter;

  for(iter=kontoListe.begin(); iter != kontoListe.end(); iter++)
    cout << iter->nummer << "\t "      // erste Zugriffsmöglichkeit
         << (*iter).inhaber << endl;   // zweite Zugriffsmöglichkeit
}

//---------------------------------------------------------------------------

void findeInListe()
{
  list<unsigned>  l;
  unsigned n;

  for(n=1; n<=5; n++)
    for(unsigned m=1; m<=n; m++)
        l.push_back(m);

  // Beispiel für Suchen und Ersetzen
  auto iter = find(l.begin(), l.end(), 3);

  while (iter != l.end())
  {
    *iter = 99;
    iter++;
    iter = find(iter, l.end(), 3);  // nächstes Element suchen
  }

  replace(l.begin(), l.end(), 99, 3);

  // Doch noch eine Liste mit '99' erzeugen
  list<unsigned>  l2;

  copy(l.begin(), l.end(), inserter(l2, l2.begin()));
  replace(l2.begin(), l2.end(), 3, 99);
}

//---------------------------------------------------------------------------
void sortBeispiel()
{
	vector<int>            v(12);
  vector<unsigned>::iterator  iter;

  for(unsigned i=0; i<4; i++)
    for(unsigned j=0; j<3; j++)
      v[i*3+j] = i+j;

  // Beispiel für Sortieren
  cout << "Unsortierter Vektor: ";
  for(auto e : v)
    cout << e << " ";
  cout << endl;

  sort(v.begin(), v.end());

  cout << "Sortierter Vektor: ";
  for (auto e : v)
	  cout << e << " ";
  cout << endl;
}
//---------------------------------------------------------------------------
// Prädikatsfunktor
class HatZiffern {
private:
	string ziffern;

public:
	HatZiffern(const string& zz = "") :ziffern(zz) {}
	bool operator()(const Konto& einKonto)
	{
		ostringstream ostr;
		ostr << einKonto.nummer;
		string nrstr(ostr.str());
		if (nrstr.find(ziffern) != string::end)
			return true;
		return false;
	}
};

//---------------------------------------------------------------------------
// Prädikatsfunktion
string g_ziffern;

bool hatZiffern(const Konto& einKonto)
{
	ostringstream ostr;
	ostr << einKonto.nummer;
	string nrstr(ostr.str());
	if (nrstr.find(g_ziffern) != string::end)
		return true;
	return false;
}

//---------------------------------------------------------------------------
void sucheNach28(const string& dateiName)
{
  list<Konto>  kontoListe;

  
  // Konten einlesen
  ifstream in_file(dateiName.c_str);
  if (in_file.bad()) return;

  while (in_file)
  {
    Konto einKonto;
    in_file >> einKonto.nummer >> einKonto.inhaber;
    kontoListe.push_back(einKonto);    // am Ende einfügen
  }

  // Beispiel für bedingtes Suchen
  cout << endl << "Suche nach Kontonummern mit '28':" << endl;

  auto iter = kontoListe.begin();
  while (iter != kontoListe.end())
  {
	  // nächstes Element suchen

	  // Variante mit Prädikatsfunktor
	  //iter = find_if(iter, kontoListe.end(), HatZiffern("28"));

	  // Variante mit Prädikatsfunktion
	  g_ziffern = "28";
	  iter = find_if(iter, kontoListe.end(), hatZiffern);

	  // Variante mit Lambda-Ausdruck
	  //iter = find_if(iter, kontoListe.end(), [=](const Konto& einKonto) -> bool {
	  // ostringstream ostr;
	  // ostr << einKonto.nummer;
	  // string nrstr(ostr.str());
	  // if (nrstr.find("28") != string::npos)
	  //  return true;
	  // return false;
	  //});

	  if (iter == kontoListe.end()) break;

	  cout << iter->nummer << "\t " << iter->inhaber << endl;
	  iter++;
  }
}

//---------------------------------------------------------------------------
int main(void)
{
  list<Konto> kontoListe;
  list<Konto> inverseListe;

  liesKonten(kontoListe, inverseListe,"konten.txt");

  findeInListe();

  sortBeispiel();

  cout << "-------------------------------------" << endl;
  sucheNach28("konten.txt");

  cin.peek();
  return 0;
}

