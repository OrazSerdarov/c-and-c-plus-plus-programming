#include <iostream>
using namespace std;
//............................................................ Klasse CBruch
class CBruch
{
public:
    CBruch( int z = 1, int n = 1 ) : m_zaehler(z), m_nenner(n) { }

    const CBruch operator+ (const CBruch& b) const {
       CBruch  sum;
       sum.m_zaehler = m_zaehler* b.m_nenner + b.m_zaehler*m_nenner;
       sum.m_nenner  = m_nenner * b.m_nenner;
       sum.kuerzen();
       return sum;
    }
    CBruch& operator+= (const CBruch& b) {
       *this = *this + b; // Aufruf von operator+
	   // alternativ: *this = operator+(b);
       return *this;
    }
    void print(const char *name) const { //..... Ausgabe eines Bruches
       cout << name << " = " << m_zaehler << "/" << m_nenner << endl;
    }

private:
    int m_zaehler, m_nenner;

    int ggT( int n, int m) { return (m==0) ? n : ggT(m, n%m); }

    void kuerzen(void) {
       int ggTeiler = ggT(m_zaehler, m_nenner);
       m_zaehler /= ggTeiler;
       m_nenner  /= ggTeiler;
    }
};
//.......................................................................... main
int main(void) {
    CBruch  b1( 1, 4 );  // 1. Bruch (1/4)
    CBruch  b2( 3, 8 );  // 2. Bruch (3/8)
    CBruch  b3;          // 3. Bruch (1/1)

    b1 += b2;        // Addiere b2 zu b1
    b1.print("b1");  // Ausgabe von b1

    b3 = b1 + b2;   // Addiere b1 und b2 --> b3
    b3.print("b3"); // Ausgabe von b3

	cin.peek();
	return 0;
}
