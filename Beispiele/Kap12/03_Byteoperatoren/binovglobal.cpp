#include "binovglobal.h"

//.................. Konstante globale Funktionen, die keine �nderungen vornehmen
const Byte operator+ (const Byte& l, const Byte& r) { return Byte(l.b + r.b); }
const Byte operator- (const Byte& l, const Byte& r) { return Byte(l.b - r.b); }
const Byte operator* (const Byte& l, const Byte& r) { return Byte(l.b * r.b); }
const Byte operator/ (const Byte& l, const Byte& r) {
  if (r.b == 0) {
     std::cerr << "Division durch 0 (Operator /)" << std::endl;
     return Byte(); // 0-Byte zur�ckgeben
  }
  return Byte(l.b / r.b);
}
const Byte operator% (const Byte& l, const Byte& r) {
   if (r.b == 0) {
      std::cerr << "Division durch 0 (Operator %)" << std::endl;
      return Byte(); // 0-Byte zur�ckgeben
   }
   return Byte(l.b % r.b);
}
const Byte operator^ (const Byte& l, const Byte& r) { return Byte(l.b ^ r.b);  }
const Byte operator& (const Byte& l, const Byte& r) { return Byte(l.b & r.b);  }
const Byte operator| (const Byte& l, const Byte& r) { return Byte(l.b | r.b);  }
const Byte operator<<(const Byte& l, const Byte& r) { return Byte(l.b << r.b); }
const Byte operator>>(const Byte& l, const Byte& r) { return Byte(l.b >> r.b); }

//........................ Zuweisungen nehmen �nderungen vor und liefern Referenz
Byte& operator+=(Byte& l, const Byte& r) { l.b += r.b; return l; }
Byte& operator-=(Byte& l, const Byte& r) { l.b -= r.b; return l; }
Byte& operator*=(Byte& l, const Byte& r) { l.b *= r.b; return l; }
Byte& operator/=(Byte& l, const Byte& r) {
   if (r.b == 0)
      std::cerr << "Division durch 0 (Operator /)" << std::endl;
   else
      l.b /= r.b;
   return l;
}
Byte& operator%=(Byte& l, const Byte& r) {
   if (r.b == 0)
      std::cerr << "Division durch 0 (Operator %)" << std::endl;
   else
      l.b %= r.b;
   return l;
}
Byte& operator^= (Byte& l, const Byte& r) { l.b ^= r.b;  return l; }
Byte& operator&= (Byte& l, const Byte& r) { l.b &= r.b;  return l; }
Byte& operator|= (Byte& l, const Byte& r) { l.b |= r.b;  return l; }
Byte& operator>>=(Byte& l, const Byte& r) { l.b >>= r.b; return l; }
Byte& operator<<=(Byte& l, const Byte& r) { l.b <<= r.b; return l; }

//.......................................................... Vergleichsoperatoren
bool operator==(const Byte& l, const Byte& r) { return l.b == r.b; }
bool operator!=(const Byte& l, const Byte& r) { return l.b != r.b; }
bool operator< (const Byte& l, const Byte& r) { return l.b <  r.b; }
bool operator> (const Byte& l, const Byte& r) { return l.b >  r.b; }
bool operator<=(const Byte& l, const Byte& r) { return l.b <= r.b; }
bool operator>=(const Byte& l, const Byte& r) { return l.b >= r.b; }
bool operator&&(const Byte& l, const Byte& r) { return l.b && r.b; }
bool operator||(const Byte& l, const Byte& r) { return l.b || r.b; }
