#ifndef BINOVGLOBAL_H
#define BINOVGLOBAL_H
#include <iostream>
#include <iomanip>

class Byte {
 public:
   Byte(unsigned char byte = 0) : b(byte) {}
   //............... Konstante globale Funktionen, die keine �nderungen vornehmen
   friend const Byte operator+ (const Byte& links, const Byte& rechts);
   friend const Byte operator- (const Byte& links, const Byte& rechts);
   friend const Byte operator* (const Byte& links, const Byte& rechts);
   friend const Byte operator/ (const Byte& links, const Byte& rechts);
   friend const Byte operator% (const Byte& links, const Byte& rechts);
   friend const Byte operator^ (const Byte& links, const Byte& rechts);
   friend const Byte operator& (const Byte& links, const Byte& rechts);
   friend const Byte operator| (const Byte& links, const Byte& rechts);
   friend const Byte operator<<(const Byte& links, const Byte& rechts);
   friend const Byte operator>>(const Byte& links, const Byte& rechts);
   //..................... Zuweisungen nehmen �nderungen vor und liefern Referenz
   friend Byte& operator+= (Byte& links, const Byte& rechts);
   friend Byte& operator-= (Byte& links, const Byte& rechts);
   friend Byte& operator*= (Byte& links, const Byte& rechts);
   friend Byte& operator/= (Byte& links, const Byte& rechts);
   friend Byte& operator%= (Byte& links, const Byte& rechts);
   friend Byte& operator^= (Byte& links, const Byte& rechts);
   friend Byte& operator&= (Byte& links, const Byte& rechts);
   friend Byte& operator|= (Byte& links, const Byte& rechts);
   friend Byte& operator>>=(Byte& links, const Byte& rechts);
   friend Byte& operator<<=(Byte& links, const Byte& rechts);
    // Folg. nicht m�gl., da Zuweisungsoperator nur als Memberfkt. �berladdbar
    // friend Byte& operator=  (Byte& links, const Byte& rechts);
   //....................................................... Vergleichsoperatoren
   friend bool operator== (const Byte& links, const Byte& rechts);
   friend bool operator!= (const Byte& links, const Byte& rechts);
   friend bool operator<  (const Byte& links, const Byte& rechts);
   friend bool operator>  (const Byte& links, const Byte& rechts);
   friend bool operator<= (const Byte& links, const Byte& rechts);
   friend bool operator>= (const Byte& links, const Byte& rechts);
   friend bool operator&& (const Byte& links, const Byte& rechts);
   friend bool operator|| (const Byte& links, const Byte& rechts);

   operator int() {return b; }

   void print(void) const {
      for (int i=7; i>=0; i--)
         std::cout << ((b>>i)&1);
      std::cout << " (" << std::setw(3) << int(b) << ")";
   }
 private:
   unsigned char b;
};
#endif
