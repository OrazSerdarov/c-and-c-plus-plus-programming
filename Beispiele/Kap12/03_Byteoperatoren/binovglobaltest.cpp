#include "binovglobal.h"
using namespace std;

#define BINOP(OP) \
    a.print(); cout << setfill(' ') << setw(3) << #OP << " "; \
    b.print(); cout << " = "; (a OP b).print(); cout << endl;

#define BINZUWEIS(OP) \
    cout << "a="; a.print(); cout << "; "; \
    cout << "b="; b.print(); cout << "; "; \
    cout << "a " #OP " b:  "; (a OP b).print(); cout << endl;

#define VERGLEICH(OP) \
    a.print(); cout << setfill(' ') << setw(3) << #OP << " "; \
    b.print(); cout << ((a OP b) ? " = true" : " = false"); cout << endl;

int main(void) {
   Byte a, b;

   a = 12; b = 9;
   cout << "---------------------------------------- Bin�re Operatoren" << endl;
   BINOP(+) BINOP(-) BINOP(*) BINOP(/) BINOP(%)
   BINOP(^) BINOP(&) BINOP(|) BINOP(<<) BINOP(>>)
   cout << "------------------------------------ Zuweisungs-Operatoren" << endl;
   BINZUWEIS(+=) BINZUWEIS(-=) BINZUWEIS(*=) BINZUWEIS(/=)  BINZUWEIS(%=)
   BINZUWEIS(^=) BINZUWEIS(&=) BINZUWEIS(|=) BINZUWEIS(>>=) BINZUWEIS(<<=)
   cout << "----------------------------------------------- Vergleiche" << endl;
   a = 12; b = 9;
   VERGLEICH(<) VERGLEICH(>) VERGLEICH(==) VERGLEICH(!=) VERGLEICH(<=)
   VERGLEICH(>=) VERGLEICH(&&) VERGLEICH(||)
   cout << "------------------------------------- a = (b - a) + a * b;" << endl;
   cout << "a="; a.print();
   cout << "; b="; b.print();
   a = (b - a) + a * b;
   cout << ";  a = "; a.print(); cout << endl;
   cout << "----------------------------------------------- a = b = c;" << endl;
   Byte c = 123;
   cout << "a="; a.print();
   cout << "; b="; b.print();
   cout << "; c="; c.print();
   a = b = c;
   cout << ";  a="; a.print(); cout << endl;

   cin.peek();
   return 0;
}
