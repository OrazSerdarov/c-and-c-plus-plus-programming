#include <iostream>
#include <functional>

using namespace std;


void sayHiNTimes(function<void()> const &f, unsigned n) {

	for (int i = 0; i < n; i++)
		f();
}

void sayHi() {

	cout << "Hallo" << endl;
}


int main(void) {

	void(*s)() (&sayHi);
	sayHiNTimes(s, 5);



	cin.peek();
	return 0;
}