#include <iostream>
using namespace std;
//................................................................. Klasse Danach
class Danach {
 public:
    Danach(char n) : z(n) {}
    char getZ(void) const { return z; }
 private:
    char z;
};
//.................................................................. Klasse Davor
class Davor {
 public:
    Davor(char n) : z(n) {}
    const Davor& operator, (const Danach& x) const {
       cout << " Davor::Komma-Operator: " << z << ", " << x.getZ() << endl;
       return *this;
    }
    char getZ(void) const { return z; }
 private:
    char z;
};
//....................................................... globaler Komma-Operator
Danach& operator, (int wert, Danach& y) {
   cout << "Danach::Komma-Operator: " << wert << ", " << y.getZ() << endl;
   return y;
}
//.............................................................. Klasse InitListe
class InitListe {
 public:
   InitListe(int *start) : zeiger(start) {}
   InitListe operator, (int x) {
      *zeiger = x;
      return InitListe(zeiger + 1);
   }
 private:
   int *zeiger;
};
//.................................................................. Klasse Array
class Array {
 public:
   Array(int n) : anz(n), werte(new int[n]) { }
   InitListe operator= (int x) {
      werte[0] = x;
      return InitListe(werte + 1);
   }
   void print(void) const {
      for (int i=0; i<anz; i++)
         cout << werte[i] << ", ";
      cout << endl;
   }
 private:
   int  anz, *werte;
};
//.......................................................................... main
int main(void) {
   Davor   a('a');
   Danach  b('b'), c('c');

   a, b;   // 1. Aufruf des Komma-Operators
   100, c; // 2. Aufruf des Komma-Operators

   Array array1(5);
   array1 = 1, 2, 3, 4, 5, 6;
   array1.print();

   Array array2(10);
   array2 = 10, 20, 30, 40, 50, 60, 70, 80, 90, 100;
   array2.print();

   system("pause");
}
