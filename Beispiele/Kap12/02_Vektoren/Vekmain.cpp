#include <iostream>
#include <cstring>
#include "Vektor.h"
#include "Datum.h"



using namespace std;

int main()
{


  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

 {
  // lokaler Zeiger 
  const int* rv = v;

  for(i=0; i<5; i++)
    cout << rv[i] << " ";
 }

  cout << endl;

  // Vektor von Datum
  Vektor<Datum> daten(31);

  for(i=0; i<31; i++)
    daten.at(i).setze(i+1, 1, 2017);

  for(i=23; i<31; i++)
	  cout << daten[i];
	  //daten[i].ausgeben();
    //daten.at(i).ausgeben();

  // Vektor von Vektor
  Vektor<Vektor<double> > m(3);
  for(i=0; i<3; i++) 
    m[i].resize(3);

  for(i=0; i<3; i++)
    m.at(i).at(i) = 1;

  // Vektor von Zeichen
  Vektor<char> code(27);
  char c='a';
  for(i=0; i<26; i++)
    code[i] = c++;

  code[i] = 0; // Nullterminierung
    
  cout << "'fghi' kommt bei " << strstr(static_cast<const char*>(code), "fghi")
	   << " vor." << endl;
	   
	cin.peek();
  return 0;
}
