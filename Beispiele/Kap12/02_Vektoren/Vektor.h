#ifndef _VEKTOR_H_
#define _VEKTOR_H_

#include <ostream>
// Verwendung von Assertions
#include <cassert>

// Deklaration der Klasse
template <typename T> class Vektor
{
private:
  unsigned int size;
  T* v;

  void swap(Vektor<T>& vek);

public:
  Vektor() : size(0), v(0) {}
  Vektor(unsigned int sz);
  Vektor(const Vektor& vek);
  ~Vektor() { if (v) delete[] v; }
  void resize(unsigned int sz);
  unsigned int getSize() 
  { return size; }


  
  const T& at(unsigned int i) const;
  T& at(unsigned int i);
  
  // Index-Operatoren ohne Bereichspr�fung


  const T& operator[](unsigned int i) const
  { return v[i]; }
  T& operator[](unsigned int i)
  { return v[i]; }


  // begin und end
  T* begin() 
  { return v; }
  T* end() 
  { return v + size; }
  
  Vektor<T>& operator=(Vektor<T> vek);
  operator const T*() const;
};

// Konstruktor mit Groessenvorgabe
template <typename T> 
Vektor<T>::Vektor(unsigned int sz) :         
  size(sz)
{ 
  v = new T[size]; 
}

// Kopierkonstruktor
template <typename T>
Vektor<T>::Vektor(const Vektor<T>& vek) :
	// 1. Initialisierung der Attribute
	v(0), size(0)
{
	// 2. Reservieren des Speichers
	v = new T[vek.size];
	if (v == 0) // kein Speicher!
	{
		exit(1);  // Ende des Programms
	}

	// 3. Kopieren des Inhalts
	size = vek.size;
	std::copy(std::begin(vek.v), std::end(vek.v), std::begin(v));
}

// Initialisierung mit Groessenvorgabe
template <typename T> 
void Vektor<T>::resize(unsigned int sz)
{
  if (v)
    delete[] v;

  size = sz;
  v = new T[size];
}

// Lesezugriff
template <typename T>
const T& Vektor<T>::at(unsigned int i) const
{
  // Vorbedingung: i g�ltig und v vorhanden
  assert(i<size && v!=0);
  return v[i];
}

// Schreibzugriff
template <typename T>
T& Vektor<T>::at(unsigned int i) 
{
  // Vorbedingung: i g�ltig und v vorhanden
  assert(i<size && v!=0);
  return v[i];
}

// Vertauschen
template <typename T>
void Vektor<T>::swap(Vektor<T>& vek)
{
	swap(size, vek.size);
	swap(v, vek.v);
}

// Zuweisungsoperator
template <typename T>
Vektor<T>& Vektor<T>::operator=(Vektor<T> vek)
{
	swap(vek);
	return *this;
}

// Typumwandlungsoperator
template <typename T>
Vektor<T>::operator const T*() const
{
  return v;
}

template <typename T>
std::ostream& operator<<(std::ostream& o, const Vektor<T>& vector)
{
  for(auto i : vector)
    o << i << " ";
  return o;
}



#endif  
