#include  <iostream>
#include  <iomanip>
#include  <cmath>
using namespace std;

const double PI = 4*::atan(1.0);

class CArray
{
public:
   CArray(double schrittweite) {
      m_len = int( 2*PI/schrittweite + 1);
      m_wert = new double[m_len];
      m_schrittweite = schrittweite;
   }
   double& operator [](double index) { 
      if (index < -PI || index > PI) {
         cerr << "Nur Indizes erlaubt von -PI bis +PI" << endl;
         exit(1);
      }
      int i = int((index + PI) * (1/m_schrittweite) );
      return m_wert[i]; 
   }
   friend ostream &operator<<(ostream &os, const CArray &a) {
      double max, min;
      int i;
      max = min = a.m_wert[0];
      for (i=1; i<a.m_len; i++) {
         if (a.m_wert[i] > max)
            max = a.m_wert[i];
         if (a.m_wert[i] < min)
            min = a.m_wert[i];
      }
      for (i=0; i<a.m_len; i++)
         os << setw(int((a.m_wert[i]*2/(max-min) + 1)*35)) << " *" << endl;
      return os;
   }
private:
   int    m_len;
   double *m_wert, m_schrittweite;
};

int main(void) {
   double x;
   CArray cosArray(0.2);

   for (x=-PI; x<=PI; x+=0.2)
      cosArray[x] = cos(x);
   cout << cosArray;

   CArray mathfunkArray(0.1);
   cout << "----------------------------------------------------------" << endl;
   for (x=-PI; x<=PI; x+=0.1)
      mathfunkArray[x] = ::cos(x) * ::cos(x) * ::sin(x);
   cout << mathfunkArray;
   cout << "----------------------------------------------------------" << endl;
   for (x=-PI; x<=PI; x+=0.1)
      mathfunkArray[x] = ::cos(x) * ::cos(x) * ::sin(x) * ::exp(::cos(x)*::sin(x));
   cout << mathfunkArray;

   cin.peek();
}
