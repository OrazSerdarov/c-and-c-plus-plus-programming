#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

template<typename T, int n>
class Stapel
{
public:
	Stapel() {hoehe = 0; }
	T pop() { 
		if (hoehe > n || hoehe < 0)
			return -1; 
		else 
			return speicher[--hoehe]; 
	}
	
	void push(T t) {
		if (hoehe > n || hoehe < 0)
			return; 
		else{
		 speicher[hoehe++]=t;
		}
	}
	bool istLeer() {
		if (hoehe == 0)
			return true;
		else
			return false;
	}
	void show() {
		int temp = hoehe;
		while (temp > 0)
			std::cout << speicher[--temp] << std::endl;

	}
	Stapel& operator+( Stapel& stapel) {

		T *arr1 = speicher;
		T *arr2 = stapel.speicher;
		

		memcpy_s(arr1 + hoehe, sizeof(speicher), arr2, sizeof(stapel.speicher));
	
		hoehe += stapel.hoehe;

		return *this;

	}
	Stapel& operator+=(Stapel & stapel) {

		
		return  (*this) + stapel;

	}

	

private:
	int hoehe;
	T speicher[n];
	
};


int main(void) {
	Stapel<int, 50> S1, S2;
	Stapel<bool, 30> S3;
	Stapel<int, 100> S4;

	 S1.push(20.0);					//nein da 20.0 kein int ist
	 S1.push(30);
	 S1.show();
	 S2.push(100);

	 std::cout << "S2  " << std::endl;
	 S2.show();

 	 S1+=  S2;
	 S1.show();

	 //S2.push(S1.pop());				//ja da s1.pop() ein int zuruckliefert und s2.push() einen int erwartet   / s1,s2 sind vom gleichen Typ
	 
	 //S3.push(S2.istLeer());			// ja da istLeer bool zurueckgibt und s3 vom Typ bool ist 
	 //S3.show();

	 //S2.push(30);
	//S1 =S1 + S2;				//nein  da erstens das + zeichen nicht defeniert ist  zweitens = nicht defeniert ist

	// S4.push(S1.hoehe);			// hoehe ist privat und somit kann nicht von aussen nicht zugegriefen werden


	std::cin.peek();
	return 0;
}