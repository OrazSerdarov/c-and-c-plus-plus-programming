#ifndef _CHARACTER_H_
#define _CHARACTER_H_
#include <iostream>
#include <string.h>
#include <string>

class Character {
	
	int mp;
	int hp;
	std::string klasse;

public:
	Character();
	Character(int, int , std::string);
	~Character();
	friend std::ostream& operator<<(std::ostream&,const Character &);
};






#endif 