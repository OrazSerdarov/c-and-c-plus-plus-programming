#include <string>
#include <iostream>

using namespace std;
class BasisKlasse {

public:

	string name1() { return "BasisKlasse::name1"; }
	virtual string name2() { return "BasisKlasse::name2"; }
	virtual ~BasisKlasse() { cout << "BasisKlasse::~BasisKlasse"; };

};

class AbgelKlasse : public BasisKlasse {

public:

	string name1() { return " AbgelKlasse::name1"; }
	virtual string name2() { return " AbgelKlasse::name2"; }
	virtual ~AbgelKlasse() { cout << " AbgelKlasse::~AbgelKlasse"; };

};


int main() {

	AbgelKlasse *akl = new AbgelKlasse();					
	BasisKlasse* bkl = akl;
	cout << akl->name1() << endl;					//AbgelKlasse::name1
	cout << akl->name2() << endl;					//AbgelKlasse::name2
	cout << bkl->name1() << endl;					//BasisKlasse::name1
	cout << bkl->name2() << endl;					//AbgelKlasse::name2
	delete bkl;										//AbgelKlasse::~AbgelKlasse
											//BasisKlasse::~BasisKlasse

	cin.peek();
	return 0;
}