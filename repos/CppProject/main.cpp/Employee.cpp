#include "Employee.h"
#include <stdio.h>
#include <string.h>
#include <algorithm> 

Employee::Employee(char * n, int newSalary) :salary(newSalary) {

	name = new char[80];

	strcpy_s(name,sizeof(char)*80, n);
}
Employee::Employee(const Employee &e):salary(e.salary) {


	if (!e.name)
		return;

	name = new char[80];
	strcpy_s(name, (sizeof(char)*80), e.name);

}	
Employee::~Employee() {
	if (name)
		delete name;
}


Employee& Employee::operator=(Employee e) {

	std::swap(e.name, name);

	
	return *this;


}


int Employee::getSalary() { return salary; }
char*  Employee::getName() { return name; }


