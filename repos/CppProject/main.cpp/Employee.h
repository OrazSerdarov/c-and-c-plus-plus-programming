#ifndef  __EMPLOYEE_H__			
#define __EMPLOYEE_H__

class Employee {
	int salary = 20000;
	char* name;

public:		
	Employee(char * ,int );

	
	Employee(const Employee &);		//1

	~Employee();						//3
		
	Employee& operator=(Employee);		//2
	
	int getSalary();
	char* getName();


};					
#endif __EMPLOYEE_H__