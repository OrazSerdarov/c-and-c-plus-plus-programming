#include <stdio.h>
#include <iostream>
#include <map>


using namespace std;


int main(void) {

	map<int, char > aMap = { {10,'a'}, {20,'b'} };
	aMap[30] = { 'c' };


	for (auto a : aMap)
		cout << a.first << a.second << endl;

	map<int, char>::const_iterator  it= aMap.begin();
	for (; it != aMap.end(); it++)
		cout << it->first << "  " << it->second << endl;


	system("pause");
	return 0;

}

