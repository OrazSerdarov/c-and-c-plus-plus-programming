﻿#include <iostream>



using namespace std;


class Set {
private:

	int  maxCard;    // maximale Elementzahl
	int* elems;     // Elemente der Menge
	int  card;  // aktuelle Kardinalitaet

public:
	enum ErrCode { OK, ADDERR, RMVERR };

	
	Set(int size = 50);
	Set(const Set& anotherSet);
	~Set();

	Set& operator=(const Set& anotherSet);
	void ausgeben();
	friend    bool operator&    (int,Set);  // Test auf Mitgliedschaft
	friend    bool operator==   (const Set&, const Set&);  // Gleichheit
	friend    bool operator!=   (const Set&, const Set&);  // Ungleichheit
	friend    Set  operator*    (const Set&, const Set&);  // Schnitt
	friend    Set  operator+    (const Set&, const Set&);  // Vereinigung
	friend    Set  operator-    (const Set&, const Set&);  // Mengenminus
	friend    bool operator<=   (const Set&, const Set&);  // Teilmenge
	friend    ostream& operator<< (ostream&, const Set&); // Ausgabe
	friend    istream& operator>> (istream&, Set&); // Eingabe eines Elements

	ErrCode   AddElem(int);                 // Hinzufuegen
	ErrCode   RmvElem(int);                 // Herausnehmen

};
Set operator*(const Set& s, const Set& t) {

	Set u;  // lokales Objekt

	for (int i = 0; i < s.card; i++)
		if (s.elems[i] & t)   // Wenn Element von s auch in t
		{
			// Fuege es hinzu und erhoehe den Elementzaehler
			u.elems[u.card++] = s.elems[i];
		}

	return u;
	

}
void Set::ausgeben() {

	for (int i = 0; i < card; i++)
		cout << elems[i]<<", ";
	cout << endl;
}
bool operator!=(const Set& s1, const Set& s2) {

	return !(s1 == s2);

}
bool operator&(int z,  Set s) {

	int i;
	for (i = 0; (i < s.card) && (z != s.elems[i]); i++);

	if (i == s.card)  // Falls die Schleife am Ende der Menge ist
		return false;

	return true;


}

bool operator==(const Set& set1, const Set& set2)  {

	if (set1.card != set2.card)
		return false;
	if (set1.maxCard != set2.maxCard)
		return false;

	for (int i = 0; i < set1.card; i++)
		if (set1.elems[i] != set2.elems[i])
			return false;

	return true;
}

Set& Set::operator=(const Set& anotherSet) {

	cout << "Vorherige Elemente" << endl;
	for (int i = 0; i < card; i++)
		cout << elems[i] << " ";
	cout << endl;

	if (!elems)
		delete[]elems;

	this->card = anotherSet.card;
	this->maxCard = anotherSet.maxCard;
	this->elems = new int[maxCard];

	for (int i = 0; i < card; i++)
		elems[i] = anotherSet.elems[i];


	cout << "Neue kopierte Elemente" << endl;
	for (int i = 0; i < card; i++)
		cout << elems[i] << " ";
	cout << endl;

	return *this;


}

Set::ErrCode Set::AddElem(int i) {

	if (card >= maxCard)
		return ErrCode::ADDERR;
	
	*(elems + card) = i;
	card++;
	return ErrCode::OK;
}


Set::ErrCode Set::RmvElem(int z) {

	if (!card)
		return ErrCode::RMVERR;
	
	for (int i = 0; i < card; i++)
		if (elems[i] == z) {
			for (; i < card; i++)
				elems[i] = elems[i + 1];

			card--;      // Vermindere den Elementzaehler
			break;
		}

	return OK;
}

Set::Set(int size):maxCard(size),card(0) {
	
	elems = new int[size];
}
Set::Set(const Set &anotherSet):maxCard(anotherSet.maxCard),card(anotherSet.card) {

	elems = new int[maxCard];

	if (!card)
		return;

	for (int i = 0; i < card; i++)
		elems[i] = anotherSet.elems[i];

	cout << "In Kopierkonstruktor \t Elemente in dem neuen Objekt" << endl;
	for (int i =0 ; i<card;i++)
		cout << elems[i] << "  ";
	cout << endl;
}

Set::~Set() {

	if (!elems)
		return;

	delete[] elems;

}




int main(void) {

	Set aSet(10);

	aSet.AddElem(10);
	aSet.AddElem(20);
	aSet.AddElem(30) ;
	aSet.AddElem(40) ;
	aSet.AddElem(50) ;
	aSet.AddElem(60) ;
	aSet.AddElem(70) ;
	aSet.AddElem(80) ;
	aSet.AddElem(90) ;
	aSet.AddElem(100) ;
	aSet.AddElem(110) ;

	Set anotherSet;

	anotherSet.AddElem(1000);

	cout << "aSet:" << endl;
	aSet.ausgeben();


	cout << "anotherSet:" << endl;
	anotherSet.ausgeben();

	anotherSet = aSet;

	cout << "anotherSet after =:" << endl;
	anotherSet.ausgeben();

	cout <<"anotherSet != aSet:  "<< (anotherSet != aSet) << endl;


/*
	bool b = aSet == anotherSet;
	
	cout << "aSet and anotherSet is equal:" << b << endl;

	Set set3;
	set3.AddElem(500);
	set3.AddElem(600);

	b = aSet == set3;

	cout << "aSet and set3 is equal:" <<  b<< endl;
	*/
	cin.peek();
	return 0;
}