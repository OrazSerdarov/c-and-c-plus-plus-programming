#include <iostream>
#include <stdio.h>
#include <algorithm>


using namespace std;


class PraeFunc {
	int n;
	
public :
	PraeFunc(int i) {
		n = i;
	}
	bool operator()() {
		
		if (n < 0)
			return false;

		return true;
	}

};


int main(void)
{

	int a[10] = { 3, 5, -8, 13, 21, 34, -55, 89, 144, 233 };
	int n = 0;

	if (all_of(a, a + 10, [&](int v) -> bool {
		cout << "v: " << v << endl;
		if (v < 0) { n++; return false; }
		return true;}))
		cout << n << ", +" << endl;
	else
		cout << n << ", -" << endl;


	cout << "Als Praedikatfunktion" << endl;

	if (all_of(a, a + 10, new PraeFunc(a) ));
		cout << n << ", +" << endl;
	else
		cout << n << ", -" << endl;

	cin.peek();
	return 0;

}