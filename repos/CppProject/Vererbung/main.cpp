#include <string>
#include <iostream>
using namespace std;

class CPerson
{
private:
	string Name;
	double Konto;
public:
	CPerson(string ValueN) : Name(ValueN) { Konto = 1000; }
	void abheben(double betrag)
	{
		Konto -= betrag;
	}
	double GetKonto() {
		return Konto;
	}
};

class CSparer : public CPerson
{
private:
	double Kredit;
public:
	CSparer(string name) : CPerson(name), Kredit(0.0) {}
	/*void abheben(double betrag)
	{
		CPerson::abheben(betrag); cout << "verfuegbarer Betrag:";
		cout << GetKonto() << endl;
	}
	*/
	void SetKredit(double hoehe) {
		Kredit = hoehe;
	} double GetKredit() {
		return Kredit;
	}
};


int main(void) {

	CPerson cp("CPerson");
	CSparer sp("CSparer");
	cout << "CPerson :" << cp.GetKonto() << endl;
	cout << "CSparer :" << sp.GetKonto ()<< endl;

	cp.abheben(10);
	sp.abheben(100);
	cout << "CPerson :" << cp.GetKonto() << endl;

	cout << "CSparer:" << sp.GetKonto() << endl;



	cin.peek();
	return 0;
}