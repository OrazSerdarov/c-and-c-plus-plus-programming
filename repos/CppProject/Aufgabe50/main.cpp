#include "Punkt.h"

class Polygon {
public:
	 Polygon(Punkt* ecken);
	 virtual double flaeche();
protected:
	Punkt* ecken;
};
class Dreieck : public Polygon {
public:
	Dreieck(Punkt* ecken);
	double flaeche();
protected:
	double grundseite;
	double hoehe;
}; 

/*
a) Ein Zeiger vom Typ Dreieck* kann auch auf ein Objekt der Klasse Polygon zeigen.



b) Das Attribut grundseite ist Objekten der Klasse Polygon zug?nglich.
	
	nein , Polygon hat ueberhaupt keine Informationen ueber den Unterklassen 
	Nur die Zeiger vom Typ Polygon hat Informationen ueber die Methode flaeche()
		


c) Jedes Objekt der Klasse Dreieck hat ein Attribut ecken.
		
		ja, da ecke als protected markiert ist ,kann jeder Objekt der Klasse Dreieck drauf zugreifen

d) Die Methode Polygon.flaeche() ist rein virtuell.
	nein 
	rein virtuell waere :
	virtual double flaeche() =0;

e) Die Methode Dreieck.flaeche() ist automatisch auch virtuell.

	nein. Wenn irgendeine Klasse von der Dreieck abgeleitet wird ist die Methode Deieck.flaeche fuer sie dann nicht mehr virtuel 
	


*/