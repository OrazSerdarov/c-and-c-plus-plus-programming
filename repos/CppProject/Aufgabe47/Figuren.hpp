#ifndef  _FIGUREN_HPP__
#define _FIGUREN_HPP__



#define PI  3.14159265359 ;

#include <stdio.h>
#include <iostream>

using namespace std;
class Figur
{
public:
	virtual double flaeche() const = 0;
	virtual double umfang() const = 0;
};


class Kreis :public Figur {
	double r;
public:
	Kreis(double d = 5) :r(d) {}
	double flaeche() { return r*r*PI; }
	double umfang() { return 2 * r*PI; }
};
class Rechteck :public Figur {
	double breite;
	double laenge;
public:
	Rechteck(double b, double l) :breite(b), laenge(l) {}
	double flaeche() { return breite*laenge; }
	double umfang() { return 2 * (breite + laenge); }

};
class Dreieck : public Figur {
	double a, b, c;

public:
	Dreieck(double _a, double _b, double _c) :
		a(_a), b(_b), c(_c) {}

	double flaeche() const {
		double s = umfang() / 2.0;
		return sqrt(s * (s - a) * (s - b) * (s - c));
	}

	double umfang() const {
		return (a + b + c);
	}
};


void figurDaten(Figur* f)
{
	
	cout << "Flaeche:" << f->flaeche() << endl;
	cout << "Umfang: " << f->umfang() << endl;
}



#endif // ! _FIGUREN_HPP__
