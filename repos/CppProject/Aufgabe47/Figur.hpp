#ifndef  _FIGUR_HPP__
#define  _FIGUR_HPP__


class Figur
{
public:
	virtual double flaeche() const = 0;
	virtual double umfang() const = 0;
};

#endif // ! _FIGUR_HPP__

