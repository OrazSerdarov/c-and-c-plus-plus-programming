#ifndef _KUNDE_H__
#define _KUNDE_H__

#include <string>
#include <iostream>
using namespace std;

class Kunde

{

	int m_nummer;
	string m_name;


public:
	static int zaehler;
	Kunde(int nummer = 0, const string& name = "");
	~Kunde();
	int getNummer() const;
	const string& getName() const;
	void ausgeben();
	static int gibAnzahl();

};

#endif