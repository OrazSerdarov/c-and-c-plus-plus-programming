#include "Kunde.h"
#include <stdio.h>
#include <iostream>



Kunde::Kunde(int nummer, const string& name ) :m_nummer(nummer),m_name(name){

	zaehler++;
	cout << "Konstruktor" << endl;
}

Kunde::~Kunde() { zaehler--; cout << "Dekonstruktor" << endl; }
int Kunde::getNummer() const {
	return m_nummer;
}
const string&  Kunde::getName() const {

	return m_name;
}
void Kunde::ausgeben() {
	cout << m_nummer << ": " << m_name << endl;
}
int Kunde::gibAnzahl () { return zaehler; }


int Kunde::zaehler = 0;


int main(void) {

	cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	Kunde k1(1, "Knut Wuchtig");
	Kunde k2(2, "Hasso Zorn");

	cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	{
		Kunde k3(3, "Sven Rueger");
		cout << "Objekte: " << Kunde::gibAnzahl() << endl;
	}

	cout << "Objekte: " << Kunde::gibAnzahl() << endl;


	cin.peek();
	return 0;
}