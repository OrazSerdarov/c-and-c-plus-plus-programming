#include <iostream>

class B{

public:
	B() { std::cout << "In B" << std::endl; };
	B(float _x) { d = _x; }
	float d;

};

class A : public B{

public:
	A(int _k):B() { i = _k; std::cout << "In A" << std::endl; }					// es faellt ein Standartkonstruktor von B und deshalb kann es kein Objekt der 	
	int i;									// Klasse A erzeugt werden 			
											// Es muss immer erst das Objekt der Basisklasse erzeugt werden koennen
											// befor man ein objekt der klasse A erzeugt wird
};											//also A(int _k):B(){}

int main()
{

	A a(3);

	std::cin.peek();
	return 0;

}