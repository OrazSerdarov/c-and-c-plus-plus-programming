
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "random.h"

using namespace std;
using namespace MyRandom;

int main()
{
	cout << "*** Zufallszahlenerzeugung *** " << endl;

	for (int i = 1; i <= 10; i++)
	{
		cout << setw(15) << MyRandom::rand();
		if (i % 5 == 0)
			cout << endl;
	}
	cout << endl;

	for (int i = 1; i <= 10; i++)
	{
		cout << setw(15) << MyRandom::randDouble();
		if (i % 5 == 0)
			cout << endl;
	}
	cout << endl;

	cin.peek();
	return 0;
}
