#include <stdio.h>
#include <ctime>
#include "random.h"

namespace MyRandom {
	static long m = 32768;
	static long b = 9757;
	static long c = 6925;


	int rand() {

		static int n = (int)(time(0) % m);  // der Keim
		n = (int)((n * b + c) % m);
		return n;
		

	}
	double randDouble() {

		return static_cast<double>(rand())/ m;

	}
}