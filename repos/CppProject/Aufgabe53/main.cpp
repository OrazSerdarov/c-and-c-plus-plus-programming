#include <iostream>

using namespace std;
template<typename T>
T f(T* t, int i) { cout << "In T f(T* t , int n)" << endl; return i; }
template<typename T> 
T f(T s, T t) { cout << "In T f(T s , T t)" << endl; return s; }

char f(char* s, int i) { cout << "In char f(char* s , int i)" << endl; return i; }

double f(double x, double y) { cout << "In double f(double x , double y)" << endl; return x+y; }


int main(void) {
	int i = 10;
	unsigned int ui = 1;
	char cFeld[20];	
	int iFeld[20];

	f(cFeld, 20);								//char  f(char * s, int i)   ->20
	f(iFeld, 20);								//T    f(T* t ,int i)		->20
	f(iFeld[0], i);								//T    f(T s, T t)			->0
	f(i, ui);									//  double  f(double x, double y)
	f(iFeld, ui);								//  T f(T* t , int n)
	f(&i, i);									//  T  f(T* t , int i)



	cin.peek();
	return 0;


}


