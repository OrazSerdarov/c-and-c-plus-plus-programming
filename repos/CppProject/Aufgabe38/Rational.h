#ifndef _RATIONAL_H__
#define _RATIONAL_H__

class Rational{

	   private:
	     long zaehler, nenner;
	     long ggT(long a, long b);

	   public:
   Rational();
   Rational(long z, long n);
   Rational(long gz); 
 
 Rational(const Rational& r); 
 void add(const Rational& a, const Rational& b);
 void sub(const Rational& a, const Rational& b);
 void mult(const Rational& a, const Rational& b);
void div(const Rational& a, const Rational& b);
void set(long z, long n);
void kehrwert();
void kuerzen();
void ausgabe();


friend ostream& operator<<(iostream& out, Rational& r);

};




#endif 