#include <iostream>


using namespace std;

class BitVec {

private:

	unsigned char* vec;    // Array mit 8*bytes Bits
	unsigned short bytes;  // Groesse des Arrays

public:

	BitVec(unsigned short dim);
	BitVec(const BitVec& v);
	~BitVec();
	bool operator[] (unsigned short idx);   // Wert eines einzelnen Bits
	void set(unsigned short index);   // Setzen auf 1
	void reset(unsigned short index); // Setzen auf 0
	void print();                     // Ausgabe aller Bits als 0/1

};

void BitVec::print() {

	for (unsigned i = 0; i < bytes ; i++)
		for (int j = 7; j >= 0; j--)
			cout << vec[i] &&(1 <<j);

}

void BitVec::set(unsigned short index) {

	unsigned short temp = index / 8;
	
	*(vec + temp)  += vec[temp] && 1 << index;
}

bool BitVec::operator[](unsigned short idx) {

	unsigned int temp = idx / 8;
	return	*(vec+temp) & 1 << idx;

}

BitVec::BitVec(const BitVec& v):bytes(v.bytes){

	if (!v.bytes)
		return;

	vec = new unsigned char[bytes];

	for (int i = 0; i < bytes; i++)
		vec[i] = v.vec[i];

}
BitVec::~BitVec() {

	if (!vec)
		return;


	delete vec;
}


BitVec::BitVec(unsigned short dim) {

	if (dim % 8 != 0)
		return;

	bytes = dim / 8;
	vec = new unsigned char[bytes];
	

	for (unsigned i = 0; i < bytes; i++)
		vec[i] = 0;

}


int main()
{


	BitVec s(16);  BitVec t(16);  BitVec u(16);
	s.set(3); s.set(10); s.set(12);
	t.set(8); t.set(10); t.set(11);
	cout << "s =      "; s.print();
	cout << endl;

	cout << "t =      "; t.print();
	cout << endl;

	/*
	u = s | t; cout << "s | t =  "; u.print();
	u = s & t; cout << "s & t =  "; u.print();
	u = ~s;    cout << "~s =     "; u.print();
	u &= t;    cout << "~s & t = "; u.print();
	*/
	system("pause");
	return 0;

}