#include "Employee.h"
#include <iostream>




Employee::Employee(const Employee &e) :salary(e.salary + 1) {

	std::cout << "Kopierkonstruktor" << std::endl;


}


Employee& Employee::operator=(Employee e) {


	using namespace std;

	if (this == &e)
		return *this;

	cout << "Zuweisungsoperator" << endl;
	swap(e.salary, salary);
	swap(e.name, name);

	return *this;
}

Employee::~Employee() {

	if (!name)
		delete[]name;

}

int main(void) {

	Employee e1(1000 );
	Employee e2 = e1;
	Employee e3;
	e3 = e1;
	using namespace std;

	cout << "Employee1 :" << e1.getName() << " \t" + e1.getSalary() << endl;
	cout << "Employee1 :" << e2.getName() << " \t" + e2.getSalary() << endl;

	std::cin.peek();
	return 0;
}