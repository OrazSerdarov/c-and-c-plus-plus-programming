#ifndef  __EMPLOYEE_H__	
#define __EMPLOYEE_H__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

class Employee {

	int salary;
	char* name;

public:		
	Employee(int newSalary) :salary(newSalary) {
	}
	Employee() {
		salary = 0;
	}
	
	Employee(const Employee &);
	~Employee();

	Employee& operator=(Employee e);

	
	int getSalary() { return salary; }
	const char* getName() { return name; }

};			


#endif 