#include <string>

using namespace std;

class Person {
private:
	string name;

public:
	Person(const string& nm);
	const string& getName() const;

};