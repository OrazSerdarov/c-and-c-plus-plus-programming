#ifndef  _PERSON_H__
#define	_PERSON_H__

#include <string>

using namespace std;

class Person {

private:
	string name;

public:
	Person(const string& nm) :name(nm) {}
	const string& getName() const { return name; }

};


#endif // !
