// aufgabe25.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

struct Point {
	double x;
	double y;
}poi;


void printPoint(struct Point *p) {

	printf("X = %.1lf  Y = %.1lf   \n", (*p).x, (*p).y);
}

int main()
{
	struct Point p1 ={ -1.1, 3.5 }, p2 ={ 2.4, -0.7 };
	

	printf("Erster Punkt P1:"); printPoint(&p1);
	printf("Zweiter Punkt P2:"); printPoint(&p2);



	getchar();
    return 0;
}

