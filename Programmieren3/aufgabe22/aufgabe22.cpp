// aufgabe22.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char **pp, *z;
	if (argc >1)
	{
		pp = argv + 1;
		z = *pp + strlen(*pp);

		while (*pp - --z)
			printf("%c", --*z);

		printf("%c\n", *z);
	}

	//COBURG
	return 0;
}
