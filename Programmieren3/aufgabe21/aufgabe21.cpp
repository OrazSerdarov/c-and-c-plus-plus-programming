// aufgabe21.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int substr(const char* str1, const char* str2);
int strlaenge(const char* str);

int main() {


	char quelle[40] = "Das ist unser Text!";
	char subs[10] = "ist";
	char subs2[10] = "uns";

	printf("\"%s\" kommt in \"%s\" ab %i vor.\n", subs, quelle, substr(quelle, subs));

	printf("\"%s\" kommt in \"%s\" ab %i vor.\n", subs2, quelle, substr(quelle, subs2));

	getchar();
	return 0;
}

int substr(const char* str1, const char* str2) {

	int laenge_1 = strlaenge(str1);
	int laenge_2 = strlaenge(str2);



	for (int i = 0; i < laenge_1 - laenge_2; i++) {

		for (int j = 0, g = i; j < laenge_2; j++)
			if (!(str1[g] == str2[j]))			
				break;
			else {
				g++;
				if (j + 1 == laenge_2)
					return i;
			}

	}



	return -1;
}


int strlaenge(const char* str) {

	int i = 0;

	while (*str != 0) {
		i++;
		str++;
	}
	return i;


}



