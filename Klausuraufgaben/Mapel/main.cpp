#include <iostream>
#include <stdio.h>


using namespace std;
typedef struct MapElement {

	int key;
	char * value;
	struct MapElement *next;


}mapel;

mapel * head = NULL;

int insert(int k, char * v) {

	mapel *temp = (mapel*)malloc(sizeof(mapel));
	temp->key = k;
	temp->value = (char*)calloc(strlen(v), sizeof(char));

	strcpy_s(temp->value, 6, v);

	temp->next = NULL;
	if (head == NULL) {

		head = temp;
		return 1;
	}
	else{

		if (head->key > k) {
			temp->next = head;
			head = temp;
			return 1;
		}	
		else {

			mapel * prev =NULL,*cur = head;

			while (cur != NULL && cur->key < k) {
				prev = cur;
				cur = cur->next;
			}

			if (!cur){
				prev->next = temp;
			}
			else {

				temp->next = cur;
				prev->next = temp;
}
			return 1;


		}
	}

}

void printAll() {

	mapel * temp = head;
	int i = 0;

	while (temp) {
		cout << "Element " << i++ << ": " << temp->key << "  " << temp->value << endl;
		temp = temp->next;
	}



}

void free_list() {

	mapel   *toDelete = NULL;

	while (head!=NULL) {
		toDelete = head;
		head = head->next;


		cout << "-----------------" << endl;
		printAll();

	}


}

int main(void) {

	insert(10, "Peter");
	insert(5, "Micho");
	insert(100, "Hanne");
	insert(50, "Jim");
	insert(100, "Miki");
	insert(1, "Mini");
	insert(25, "MItte");

	printAll();

	free_list();


	cin.peek();
	return 0;

}