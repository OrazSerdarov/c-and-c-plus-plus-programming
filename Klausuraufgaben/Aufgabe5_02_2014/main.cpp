#include <iostream>
#include <string>
#include <list>
#include <algorithm>


using namespace std;


class Image {
private:
	int dimx, dimy;
	int** img;
	string name;

public:
	Image():dimx(0),dimy(0),img(nullptr){}
	Image(string n ,int x, int y);
	Image& operator=(const Image&);
	void ausgeben();
	bool testTags(const string& s);

};

bool Image::testTags(const string& s) {

	
	if (name == s)
		return true;

	return false;

}

void Image::ausgeben() {

	cout << "Image with x " << dimx << "and y:" << dimy <<endl ;
	for(int i =0 ;i<dimx ;i++){
		for (int j = 0; j < dimy; j++)
			cout<<img[i][j]<<" ";

		cout << endl;
	}

	cout << "------------------" << endl;
}

Image& Image::operator=(const Image& image){


	if(!img){
		for (int i = 0; i < dimx; i++)
			delete[] (img[i]);

		delete[]img;

	}
	dimx = image.dimx;
	dimy = image.dimy;

	img = new int*[dimx];

	for (int i = 0; i <dimx; i++)
		img[i] = new int[dimy];

	for (int i = 0; i < dimx; i++)
		for (int j = 0; j < dimy; j++)
			img[i][j] = image.img[i][j];

	return *this;

}
Image::Image(string n ,int x, int y):name(n),dimx(x),dimy(y){

	img = new int*[x];

	for (int i = 0; i < x; i++)
		img[i] = new int[y];

	for (int i = 0; i < dimx; i++)
		for (int j = 0; j < dimy; j++)
			img[i][j] = i;
}

class Funktor {

	string  n;
	 int& anz;

public:
	Funktor(const string n , int & i ) :n(n),anz(i) {}

	//double operator()(double arg) const {
	void operator()(Image  i) const {
		
		if (i.testTags(n))
			anz++;
		
	}


};




int search(list<Image> imgList, const string & s) {


	int n = 0;
	//for_each(imgList.begin(), imgList.end(), Funktor(s ,n));

	for_each(imgList.begin(), imgList.end(), [&](Image m)->void {

		if (m.testTags(s))
			n++;

	});




	return n;

}


int main(void) {


	list<Image> previews(5);
	previews.push_back(Image("MikiMaus", 3, 3));
	previews.push_back(Image("MikiMaus", 2, 1));
	previews.push_front(Image("Xuy", 2, 1));
	previews.push_front(Image("Slonik", 2, 2));
	previews.push_front(Image("MikiMaus", 2, 1));


	cout << search(previews, "MikiMaus") << endl;




	cin.peek();
	return 0;
}

