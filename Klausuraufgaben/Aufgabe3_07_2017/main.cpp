#include <stdio.h>
#include <stdlib.h>

typedef struct MapElement
{
	int key;
	char *value;
	struct MapElement *next;
}mapel;

mapel *head = NULL;

void printAll(mapel * h) {

	while (h != NULL){
		printf("Key:%d \t Value:%s \n", h->key, h->value);
		h = h->next;
	}

}

int insert(int k, char *v ){

	mapel * neu = (mapel*)malloc(sizeof(mapel));
	
	if (!neu)
		return 0;

	neu->key = k;
	neu->value = v;
	neu->next = NULL;
	
	if (head == NULL) {
		head = neu;
		printf("Aller erstes Element %d %s \n", k, v);
	
		return 1;
	}

	else if (head->key > k) {
		neu->next = head;
		head = neu;
		printf("Zweites Element %d %s \n", k, v);
		
		return 1;
	}
	else {

		mapel *cur = head;
	


		printf("Ab zwei Elemente %d %s \n", k, v);
		while (cur->next != NULL) {
			if (cur->next->key > k) {
				neu->next = cur->next;
				cur->next = neu;
				return 1;
			}
			else
				cur = cur->next;
		}
		
		if(neu->next==NULL)
			cur->next = neu;

		return 1;

	}


	return 0;
}

//does not working!
void free_list(mapel *k) {

	mapel  *toDelete;

	while(k) {
		toDelete = k;
		k = k->next;
		free(toDelete->value);
	}

	head = NULL;


}

int main(void) {

	insert(10 ,"Michi");
	insert(5, "Jonas");
	insert(100, "Peter");	
	insert(30, "Frank");
	insert(50, "Johanna");

	printAll(head);

	printf("Delete now \n");
	free_list(head);
	printAll(head);
	printf("insert again \n");
	insert(500, "Tom");
	insert(90, "Jassi");
	insert(700, "Miki");
	printAll(head);

	system("pause");
	return 0;
}