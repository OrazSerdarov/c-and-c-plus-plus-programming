#include <stdio.h>
#include <stdlib.h>


struct Node {
	int val;
	struct Node *leftChild;
	struct Node *rightChild;

};




typedef  int (*Visitor)(struct Node*);

struct Node *head;

Node* insert(struct Node * aNode ,int i) {


	if (aNode == NULL) {
		struct Node *newNode = (Node *)malloc(sizeof(struct Node));
		newNode->val = i;
		newNode->leftChild = NULL;
		newNode->rightChild = NULL;
		return newNode;
	}
	else if(aNode->val<i){

		if (aNode->rightChild == NULL)
			aNode->rightChild = insert(aNode->rightChild, i);
		else
			insert(aNode->rightChild, i);

	}
	else{

		
		if (aNode->leftChild == NULL)
			aNode->leftChild = insert(aNode->leftChild, i);
		else
		insert(aNode->leftChild, i);

	}


}

int sum(struct Node* node) {

	int s = 0;


	if (node == NULL)
		return 0;
	else {

		if (node->leftChild != NULL)
			s += sum(node->leftChild);
		s += node->val;

		if (node->rightChild != NULL)
			s += sum(node->rightChild);
		
	

			
	}


	return s;

}

void iterate(struct Node* aNode, Visitor v) {



	if (aNode == NULL)
		return;
	else {

		if (aNode->leftChild != NULL)
				iterate(aNode->leftChild ,v);
		
		
		

		if (aNode->rightChild != NULL)
			iterate(aNode->rightChild , v);






}


int main(void){


	
	Visitor = sum;
	
	head = insert(head, 10);
	insert(head, 0);
	insert(head, 15);
	insert(head, -10);
	insert(head, 65);
	insert(head, -5);
	insert(head, 50);

	 insert(head, 20);


	 printf("%d \n", sum(head));
	system("pause");
	return 0;
}