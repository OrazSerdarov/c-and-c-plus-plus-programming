#include <iostream>

#define macro(a)  ( ~(a) &1)

using namespace std;



void  pr(const int i) {

	printf("%c", i);
}
int main(void) {

	printf("%d \n", macro(7));

	printf("%d \n", macro(3));


	printf("%d \n", macro(8));

	printf("%d \n", macro(6));
	
	/*

	printf("short int: %d\n", sizeof(short int));			//2
	printf("int: %d\n", sizeof(int));						//4
	printf("long int: %d\n", sizeof(long int));				//8			4
	printf("long long: %d\n", sizeof(long long));			//8
	printf("float: %d\n", sizeof(float));					//4
	printf("double: %d\n", sizeof(double));					//8
	printf("long double: %d\n", sizeof(long double));		//8
	*/
	cin.peek();
	return 0;
}