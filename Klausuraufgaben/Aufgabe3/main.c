#include<stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct element {
	int val;
	struct element* next;
	
}node;


int insert(node ** head, int n) {

	node *newElement = (node*)malloc(sizeof(node));
	newElement->val = n;
	newElement->next = NULL;


	if (*head == NULL){
		*head = newElement;
		return 1;
	
	}
	else if ((*head)->val > n) {

		newElement->next = *head;
		*head = newElement;

		return 1;

	}
	else {

		node * prev = NULL, *cur = *head;

		while (cur != NULL && cur->val < n) {
			prev = cur;
			cur = cur->next;
		}

		if (cur == NULL) 
			prev->next = newElement;
		else {

			newElement->next = cur;
			prev->next = newElement;
			

		}
		return 1;



	}


	free(newElement);

	return 0;
}

void printAll(node ** head) {

	node *temp = *head;

	if (!temp) {
		printf("No Elements \n");
		return;
	}


	while (temp) {
		printf("Value :%d \n", temp->val);
		temp = temp->next;
	}
	

}

void delete_listRekrusiv(node **head, node *e)
{

	node * temp = *head;

	if(temp == NULL)
		return;


	if (temp->val >= e->val) {

		*head = (*head)->next;
		free(temp);
		delete_listRekrusiv(head, e);
	}



	if (temp->val < e->val)
		return;




}
	
void delete_list(node **head ,node * e) {

	if (*head == NULL)
		return;


	node *cur = *head , *prev=*head;

	while (cur != NULL && cur->val != e->val) {

		prev = cur;
		cur = cur->next;



	}
	if (cur->val == e->val) {

		if (cur->val == (*head)->val)
			*head = NULL;

		prev->next = NULL;
		while (cur) {
			prev = cur;
			cur = cur->next;

			free(prev);
			prev = NULL;
		}


	}
	return;
}

int main(void) {

	float f = 10.2f;
	const fConst = 9.8;

	const float* constFloat = &f;
	float* const  ConstPtr = &f;
	const float* const constConst = &f;

		

	

	printf("%f.* \n", f);


	/*
	node aNode;
	aNode.next = NULL;
	aNode.val = 5;

	node *aNodePointer = &aNode;
	node * n =NULL;
	node ** head  = &n;
	insert(head, 10);
	insert(head, 5);
	insert(head, 7);
	insert(head, 20);
	insert(head, 6);

	printAll(head);

	printf("Nach dem Loeschen von 7 \n -----------------\n");

	delete_listRekrusiv(head, aNodePointer);
	printAll(head);
	*/
	system("pause");
	return 0;
}