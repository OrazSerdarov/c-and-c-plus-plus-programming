#include <stdio.h>
#include "header.h"
#include <stdlib.h>
#include <iostream>

void init(int v) {

	std::cout << "in init" << std::endl;

	root = (knoten*)malloc(sizeof(struct node));

	if (root)
		root->value = v;
}

knoten * addLeft(knoten* parent, int value) {


	std::cout << "in addLeft" << std::endl;

	knoten  *neuKnoten = (knoten*)malloc(sizeof(struct node));
	neuKnoten->value = value;
	parent->lefChild = neuKnoten;

	return neuKnoten;

}

knoten * addRight(knoten* parent, int value) {

	std::cout << "in addRight" << std::endl;

	knoten  *neuKnoten = (knoten*)malloc(sizeof(struct node));
	neuKnoten->value = value;
	parent->rightChild = neuKnoten;

	return neuKnoten;

}
unsigned int getHeight(knoten * root) {

	if (!root)
		return 0;

	int r = 1;
	int l = 1;

	l += getHeight(root->lefChild);
	r += getHeight(root->rightChild);


	return l > r ? l : r;

}