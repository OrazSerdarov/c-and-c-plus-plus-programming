#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;


template <int n>
class AClass {
public:
	enum{ wert=  AClass<n-1>::wert + AClass<n-2>::wert };

};

template<>class AClass<1> {

public:
	enum{wert=1};
};


template<>class AClass<0>{

public:
	enum{wert=1};
};


int main(void) {

	const  int x = AClass<6>::wert;
	cout << x << endl;
	cin.peek();

	return 0;

}