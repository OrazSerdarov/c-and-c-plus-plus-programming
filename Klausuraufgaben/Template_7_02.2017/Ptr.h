#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Ptr {
	T* value;

public:
	Ptr();
	Ptr(const T& v);						 
	Ptr(const Ptr<T> &p);
	virtual ~Ptr() { cout << "Ptr::~Ptr\n"; delete value; }
	Ptr<T>& operator=(const Ptr<T> &p);
	const T& getValue()const { return *value; }
	virtual string name() { return "Ptr"; }
};

template<typename T> Ptr<T>::Ptr(const T& v) {

	value = new T;

	if (!value)
		return;
	*value = v;
	cout << "adresse von T" << value << "\n Adresse von uebergebenem Wert    " << &v << endl;
}
template<typename T> Ptr<T>::Ptr(const Ptr<T>& p) {

	value = new T;
	if (!value)
		return;
	*value = *(p.value);
	cout << "Addresse von dem neuen Object:   " << value << "\n Addresse von dem uebergebenem Object: " << p.value << endl;
}
template<typename T> Ptr<T>& Ptr<T>::operator=(const Ptr<T> &p) {

	*value = *(p.value);

	cout << "Hier ist Zuweisungsoperator mit   " << value << " und value vom uebegebenem Object : " << p.value << endl;

	return *this;
}

