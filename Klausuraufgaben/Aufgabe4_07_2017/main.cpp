#include <string>
#include <iostream>

using namespace std;

class CQueue {

	string * m_asQueue;
	int m_iMax;
	int m_iCount;
	int m_iFirst;
	int m_iLast;

public:
	CQueue():m_asQueue(0),m_iMax(0),m_iCount(0),m_iFirst(0),m_iLast(0){}
	CQueue(int iMax): m_iMax(iMax), m_iCount(0), m_iFirst(0), m_iLast(0) {
		
		m_asQueue = new string[iMax];

	}
	CQueue(const CQueue &q):m_iMax(q.m_iMax),m_iCount(q.m_iCount),m_iFirst(q.m_iFirst),m_iLast(q.m_iLast) {

		m_asQueue = new string[m_iMax];
		for (int i = 0, j =m_iFirst; i < m_iCount; i++ ,j++){
			
			if (j >= m_iMax)
				j = 0;
			
			*(m_asQueue+j)  = *(q.m_asQueue+j);

			

		}
	}
	bool isEmpty() { return m_iCount == 0; }
	bool isFull() { return m_iMax == m_iCount; }
	const string& get() {
		return m_asQueue[m_iFirst];
	}
	bool put(const string & name) {

		if (m_iMax == m_iCount ){
			cout << "Queue ist schon voll!!!" << endl;
			return false;
		}
		

		m_asQueue[m_iLast]=name;
		m_iCount++;
		m_iLast++;
		m_iLast = m_iLast%m_iMax;

		if (m_iCount == 0)
			m_iFirst = m_iLast;

		return true;
	}
	string pop() {
		
		if (!m_iCount)
			return "";
		
		string  temp( m_asQueue[m_iFirst]);
		m_asQueue[m_iFirst] = "";

		m_iFirst++;
		m_iFirst = m_iFirst%m_iMax;
		m_iCount--;

		return temp;


	}
	void contents() {

		for (int i = 0, j = m_iFirst; i < m_iCount; i++, j++) {
			cout << m_asQueue[j] << endl;

			if (j + i >= m_iMax)
				j = 0;
		}

	}
};


int main(void) {

	double *(*b)[2];
	double **bb[2];


	int  ii[2] = { 10,20 };

	int *c = ii;


	int i = 5;
	int *ptr = &i;
	int **ptr2 = &ptr;

	cout << "Die i hat den Wert" << i << " \t Mit der Adresse :" <<&i<<endl  ;
	cout << "Ptr* Wert" << ptr << " \t Adresse" << &ptr << endl;
	cout << "ptr** wert" << ptr2 << "\t Adresse" << &ptr2 << endl;

	/*
	CQueue aQueue(3);

	aQueue.put("Friste String");
	aQueue.put("Zweite String");
	aQueue.put("Dritte String");
	aQueue.put("Vierte String");

	aQueue.contents();

	cout << "Jetzt wird gepopt \t" << aQueue.pop() << endl;

	aQueue.put("Vierte String");
	aQueue.contents();

	cout << "Kopierkonstruktor" << endl;

	CQueue another(aQueue);

	cout << "Erste Queue" << endl;
	aQueue.contents();
	cout << "Zweite Queue" << endl;
	another.contents();

	aQueue.pop();

	cout << "Erste Queue" << endl;
	aQueue.contents();
	cout << "Zweite Queue" << endl;
	another.contents();


	aQueue.put("Fuenfte String");

	cout << "Alles loeschen" << endl;

	cout << "1" << aQueue.pop()<< endl;
	cout << "2" << aQueue.pop()<<endl;
	cout << "3" << aQueue.pop()<<endl;

	cout << "Nix mehr da"<< endl;
	aQueue.contents();

	aQueue.put("Ein neues");
	aQueue.contents();
*/

	
	
	cin.peek();
	return 0;


}